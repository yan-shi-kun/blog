package xyz.xiaomy.blog.config;

import org.springframework.amqp.core.*;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import xyz.xiaomy.blog.constant.MQPrefixConst;

/**
 * Rabbitmq配置类
 *
 * @author yanshikun
 * @date 2021/07/29
 */
@Configuration
public class RabbitMQConfig {

    @Bean
    public Queue articleQueue() {
        return QueueBuilder
                .durable(MQPrefixConst.MAXWELL_QUEUE)
                .build();
    }

    @Bean
    public FanoutExchange maxWellExchange() {
        return new FanoutExchange(MQPrefixConst.MAXWELL_EXCHANGE, true, false);
    }

    @Bean
    public Binding bindingArticleDirect(Queue articleQueue,FanoutExchange maxWellExchange) {
        return BindingBuilder
                .bind(articleQueue)
                .to(maxWellExchange);
    }

    @Bean
    public Queue emailQueue() {
        return QueueBuilder
                .durable(MQPrefixConst.EMAIL_QUEUE)
                .build();
    }

    @Bean
    public FanoutExchange emailExchange() {
        return new FanoutExchange(MQPrefixConst.EMAIL_EXCHANGE, true, false);
    }

    @Bean
    public Binding bindingEmailDirect(Queue emailQueue,FanoutExchange emailExchange) {
        return BindingBuilder.bind(emailQueue).to(emailExchange);
    }

}
