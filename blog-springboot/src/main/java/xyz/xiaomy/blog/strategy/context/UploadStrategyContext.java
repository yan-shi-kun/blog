package xyz.xiaomy.blog.strategy.context;

import xyz.xiaomy.blog.strategy.UploadStrategy;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;
import xyz.xiaomy.blog.enums.UploadModeEnum;

import java.util.Map;


/**
 * 上传策略上下文
 *
 * @author yanshikun
 * @date 2021/07/28
 */
@Service
public class UploadStrategyContext {
    /**
     * 上传模式
     */
    @Value("${upload.mode}")
    private String uploadMode;

    @Autowired
    private Map<String, UploadStrategy> uploadStrategyMap;

    /**
     * 上传文件
     *
     * @param file 文件
     * @param path 路径
     * @return {@link String} 文件地址
     */
    public String executeUploadStrategy(MultipartFile file, String path) {
        return uploadStrategyMap.get(UploadModeEnum.getStrategy(uploadMode)).uploadFile(file, path);
    }

    public String getUploadPath(){
        return uploadStrategyMap.get(UploadModeEnum.getStrategy(uploadMode)).getFileAccessPath();
    }
    public String getUploadUrl(){
        return uploadStrategyMap.get(UploadModeEnum.getStrategy(uploadMode)).getFileAccessUrl("");
    }


}
