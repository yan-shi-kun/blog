package xyz.xiaomy.blog.service.impl;


import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.IdWorker;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.xiaomy.blog.dao.PhotoDao;
import xyz.xiaomy.blog.dto.PhotoBackDTO;
import xyz.xiaomy.blog.dto.PhotoDTO;
import xyz.xiaomy.blog.entity.Photo;
import xyz.xiaomy.blog.entity.PhotoAlbum;
import xyz.xiaomy.blog.enums.FilePathEnum;
import xyz.xiaomy.blog.exception.BizException;
import xyz.xiaomy.blog.service.PhotoAlbumService;
import xyz.xiaomy.blog.service.PhotoService;
import xyz.xiaomy.blog.strategy.context.UploadStrategyContext;
import xyz.xiaomy.blog.util.BeanCopyUtils;
import xyz.xiaomy.blog.util.PageUtils;
import xyz.xiaomy.blog.vo.*;
import net.coobird.thumbnailator.Thumbnails;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import xyz.xiaomy.blog.constant.CommonConst;
import xyz.xiaomy.blog.vo.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import static xyz.xiaomy.blog.enums.PhotoAlbumStatusEnum.PUBLIC;

/**
 * 照片服务
 *
 * @author yanshikun
 * @date 2021/08/04
 */
@Service
public class PhotoServiceImpl extends ServiceImpl<PhotoDao, Photo> implements PhotoService {
    @Autowired
    private PhotoDao photoDao;
    @Autowired
    private PhotoAlbumService photoAlbumService;
    @Autowired
    private UploadStrategyContext uploadStrategyContext;

    @Override
    public PageResult<PhotoBackDTO> listPhotos(ConditionVO condition) {
        // 查询照片列表
        Page<Photo> page = new Page<>(PageUtils.getCurrent(), PageUtils.getSize());
        Page<Photo> photoPage = photoDao.selectPage(page, new LambdaQueryWrapper<Photo>()
                .eq(Objects.nonNull(condition.getAlbumId()), Photo::getAlbumId, condition.getAlbumId())
                .eq(Photo::getIsDelete, condition.getIsDelete())
                .orderByDesc(Photo::getId)
                .orderByDesc(Photo::getUpdateTime));
        List<PhotoBackDTO> photoList = BeanCopyUtils.copyList(photoPage.getRecords(), PhotoBackDTO.class);
        return new PageResult<>(photoList, (int) photoPage.getTotal());
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updatePhoto(PhotoInfoVO photoInfoVO) {
        Photo photo = BeanCopyUtils.copyObject(photoInfoVO, Photo.class);
        photoDao.updateById(photo);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void savePhotos(PhotoVO photoVO) {
        List<Photo> photoList = new ArrayList<>();
        String uploadPath = uploadStrategyContext.getUploadPath();
        String uploadUrl = uploadStrategyContext.getUploadUrl();
        for (String url : photoVO.getPhotoUrlList()) {
            //原图
            String fileName = url.substring(url.lastIndexOf("/") + 1);
            String filePath = FilePathEnum.ARTICLE.getPath()+fileName;
            //压缩图
            String compressFileName = "compress_"+fileName;
            String photoCompress=uploadUrl+ FilePathEnum.PHOTO.getPath()+ compressFileName;
            String outPutCompress=uploadPath+ FilePathEnum.PHOTO.getPath()+ compressFileName;
            new File(uploadPath+ FilePathEnum.PHOTO.getPath()).mkdirs();
            try (FileOutputStream fileOutputStream = new FileOutputStream(outPutCompress)){
                Thumbnails.of(new File(uploadPath+filePath))
                        .scale(1f) //图片大小（长宽）压缩比例 从0-1，1表示原图
                        .outputQuality(0.5f) //图片质量压缩比例 从0-1，越接近1质量越好
                        .toOutputStream(fileOutputStream);
            } catch (IOException e) {
                e.printStackTrace();
            }
            Photo photo = Photo.builder()
                    .albumId(photoVO.getAlbumId())
                    .photoName(IdWorker.getIdStr())
                    .photoSrc(url)
                    .photoCompress(photoCompress)
                    .build();
            photoList.add(photo);
        }
        this.saveBatch(photoList);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updatePhotosAlbum(PhotoVO photoVO) {
        List<Photo> photoList = photoVO.getPhotoIdList().stream().map(item -> Photo.builder()
                        .id(item)
                        .albumId(photoVO.getAlbumId())
                        .build())
                .collect(Collectors.toList());
        this.updateBatchById(photoList);
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void updatePhotoDelete(DeleteVO deleteVO) {
        // 更新照片状态
        List<Photo> photoList = deleteVO.getIdList().stream().map(item -> Photo.builder()
                        .id(item)
                        .isDelete(deleteVO.getIsDelete())
                        .build())
                .collect(Collectors.toList());
        this.updateBatchById(photoList);
        // 若恢复照片所在的相册已删除，恢复相册
        if (deleteVO.getIsDelete().equals(CommonConst.FALSE)) {
            List<PhotoAlbum> photoAlbumList = photoDao.selectList(new LambdaQueryWrapper<Photo>()
                            .select(Photo::getAlbumId)
                            .in(Photo::getId, deleteVO.getIdList())
                            .groupBy(Photo::getAlbumId))
                    .stream()
                    .map(item -> PhotoAlbum.builder()
                            .id(item.getAlbumId())
                            .isDelete(CommonConst.FALSE)
                            .build())
                    .collect(Collectors.toList());
            photoAlbumService.updateBatchById(photoAlbumList);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    @Override
    public void deletePhotos(List<Integer> photoIdList) {
        photoDao.deleteBatchIds(photoIdList);
    }

    @Override
    public PhotoDTO listPhotosByAlbumId(Integer albumId) {
        // 查询相册信息
        PhotoAlbum photoAlbum = photoAlbumService.getOne(new LambdaQueryWrapper<PhotoAlbum>()
                .eq(PhotoAlbum::getId, albumId)
                .eq(PhotoAlbum::getIsDelete, CommonConst.FALSE)
                .eq(PhotoAlbum::getStatus, PUBLIC.getStatus()));
        if (Objects.isNull(photoAlbum)) {
            throw new BizException("相册不存在");
        }
        // 查询照片列表
        Page<Photo> page = new Page<>(PageUtils.getCurrent(), PageUtils.getSize());
        List<Photo> photoList = new ArrayList<>(photoDao.selectPage(page, new LambdaQueryWrapper<Photo>()
                                .select(Photo::getPhotoCompress, Photo::getId)
                                .eq(Photo::getAlbumId, albumId)
                                .eq(Photo::getIsDelete, CommonConst.FALSE)
                                .orderByDesc(Photo::getId))
                                .getRecords());
        List<String> photoCompressList = photoList.stream().map(Photo::getPhotoCompress).collect(Collectors.toList());
        List<Integer> IdList = photoList.stream().map(Photo::getId).collect(Collectors.toList());
        return PhotoDTO.builder()
                .photoAlbumCover(photoAlbum.getAlbumCover())
                .photoAlbumName(photoAlbum.getAlbumName())
                .photoList(photoCompressList)
                .photoIdList(IdList)
                .build();
    }

    @Override
    public InputStream downByAlbumId(Integer photoId) throws FileNotFoundException {

        Photo photo = photoDao.selectOne(new LambdaQueryWrapper<Photo>()
                .select(Photo::getPhotoSrc)
                .eq(Photo::getId, photoId)
                .eq(Photo::getIsDelete, CommonConst.FALSE));
        if(photo==null){
            throw new BizException("下载资源不存在!");
        }
        String photoSrc = photo.getPhotoSrc();

        int sub = photoSrc.lastIndexOf("/");;
        String substring = photoSrc.substring(sub+1);
        String uploadPath = uploadStrategyContext.getUploadPath();
        return new FileInputStream(uploadPath+FilePathEnum.ARTICLE.getPath()+substring);
    }

}




