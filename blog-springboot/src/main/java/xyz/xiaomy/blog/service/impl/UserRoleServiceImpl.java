package xyz.xiaomy.blog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.xiaomy.blog.dao.UserRoleDao;
import xyz.xiaomy.blog.entity.UserRole;
import xyz.xiaomy.blog.service.UserRoleService;
import org.springframework.stereotype.Service;


/**
 * 用户角色服务
 *
 * @author yanshikun
 * @date 2021/08/10
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleDao, UserRole> implements UserRoleService {


}
