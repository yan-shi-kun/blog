package xyz.xiaomy.blog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.xiaomy.blog.dao.ChatRecordDao;
import xyz.xiaomy.blog.entity.ChatRecord;
import xyz.xiaomy.blog.service.ChatRecordService;
import org.springframework.stereotype.Service;

/**
 * 聊天记录服务
 *
 * @author yanshikun
 * @date 2021/07/28
 */
@Service
public class ChatRecordServiceImpl extends ServiceImpl<ChatRecordDao, ChatRecord> implements ChatRecordService {



}
