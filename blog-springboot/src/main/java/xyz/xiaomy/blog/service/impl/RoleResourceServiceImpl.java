package xyz.xiaomy.blog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.xiaomy.blog.dao.RoleResourceDao;
import xyz.xiaomy.blog.entity.RoleResource;
import xyz.xiaomy.blog.service.RoleResourceService;
import org.springframework.stereotype.Service;

/**
 * 角色资源服务
 *
 * @author yanshikun
 * @date 2021/07/28
 */
@Service
public class RoleResourceServiceImpl extends ServiceImpl<RoleResourceDao, RoleResource> implements RoleResourceService {


}
