package xyz.xiaomy.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.xiaomy.blog.dto.OperationLogDTO;
import xyz.xiaomy.blog.vo.PageResult;
import xyz.xiaomy.blog.entity.OperationLog;
import xyz.xiaomy.blog.vo.ConditionVO;

/**
 * 操作日志服务
 *
 * @author yanshikun
 * @date 2021/07/29
 */
public interface OperationLogService extends IService<OperationLog> {

    /**
     * 查询日志列表
     *
     * @param conditionVO 条件
     * @return 日志列表
     */
    PageResult<OperationLogDTO> listOperationLogs(ConditionVO conditionVO);

}
