package xyz.xiaomy.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.xiaomy.blog.entity.RoleResource;

/**
 * 角色资源服务
 *
 * @author yanshikun
 * @date 2021/07/29
 */
public interface RoleResourceService extends IService<RoleResource> {


}
