package xyz.xiaomy.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.xiaomy.blog.entity.RoleMenu;

/**
 * 角色菜单服务
 *
 * @author yanshikun
 * @date 2021/07/29
 */
public interface RoleMenuService extends IService<RoleMenu> {


}
