package xyz.xiaomy.blog.service;

import xyz.xiaomy.blog.entity.WebsiteConfig;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * 网站配置服务
 *
 * @author yanshikun
 * @date 2021/08/09
 */
public interface WebsiteConfigService extends IService<WebsiteConfig> {

}
