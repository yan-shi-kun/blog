package xyz.xiaomy.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.xiaomy.blog.entity.ChatRecord;

/**
 * 聊天记录服务
 *
 * @author yanshikun
 * @date 2021/07/28
 */
public interface ChatRecordService extends IService<ChatRecord> {


}
