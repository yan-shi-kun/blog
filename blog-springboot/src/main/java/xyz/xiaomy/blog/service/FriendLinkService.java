package xyz.xiaomy.blog.service;

import xyz.xiaomy.blog.dto.FriendLinkBackDTO;
import xyz.xiaomy.blog.dto.FriendLinkDTO;
import xyz.xiaomy.blog.vo.ConditionVO;
import xyz.xiaomy.blog.vo.PageResult;
import xyz.xiaomy.blog.entity.FriendLink;
import com.baomidou.mybatisplus.extension.service.IService;
import xyz.xiaomy.blog.vo.FriendLinkVO;

import java.util.List;

/**
 * 友链服务
 *
 * @author yanshikun
 * @date 2021/07/29
 */
public interface FriendLinkService extends IService<FriendLink> {

    /**
     * 查看友链列表
     *
     * @return 友链列表
     */
    List<FriendLinkDTO> listFriendLinks();

    /**
     * 查看后台友链列表
     *
     * @param condition 条件
     * @return 友链列表
     */
    PageResult<FriendLinkBackDTO> listFriendLinkDTO(ConditionVO condition);

    /**
     * 保存或更新友链
     *
     * @param friendLinkVO 友链
     */
    void saveOrUpdateFriendLink(FriendLinkVO friendLinkVO);

}
