package xyz.xiaomy.blog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.xiaomy.blog.dao.WebsiteConfigDao;
import xyz.xiaomy.blog.entity.WebsiteConfig;
import xyz.xiaomy.blog.service.WebsiteConfigService;
import org.springframework.stereotype.Service;

/**
 * 网站配置服务
 *
 * @author yanshikun
 * @date 2021/08/09
 */
@Service
public class WebsiteConfigServiceImpl extends ServiceImpl<WebsiteConfigDao, WebsiteConfig> implements WebsiteConfigService{

}




