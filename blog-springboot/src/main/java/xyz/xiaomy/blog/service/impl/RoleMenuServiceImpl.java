package xyz.xiaomy.blog.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import xyz.xiaomy.blog.dao.RoleMenuDao;
import xyz.xiaomy.blog.entity.RoleMenu;
import xyz.xiaomy.blog.service.RoleMenuService;
import org.springframework.stereotype.Service;

/**
 * 角色菜单服务
 *
 * @author yanshikun
 * @date 2021/07/28
 */
@Service
public class RoleMenuServiceImpl extends ServiceImpl<RoleMenuDao, RoleMenu> implements RoleMenuService {


}
