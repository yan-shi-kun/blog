package xyz.xiaomy.blog.service;

import com.baomidou.mybatisplus.extension.service.IService;
import xyz.xiaomy.blog.entity.UserRole;

/**
 * 用户角色服务
 *
 * @author yanshikun
 * @date 2021/07/29
 */
public interface UserRoleService extends IService<UserRole> {

}
