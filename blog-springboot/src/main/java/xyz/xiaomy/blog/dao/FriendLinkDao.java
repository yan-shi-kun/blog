package xyz.xiaomy.blog.dao;

import xyz.xiaomy.blog.entity.FriendLink;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;


/**
 * 友情链接
 *
 * @author yanshikun
 * @date 2021/08/10
 */
@Repository
public interface FriendLinkDao extends BaseMapper<FriendLink> {

}
