package xyz.xiaomy.blog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.xiaomy.blog.entity.RoleResource;
import org.springframework.stereotype.Repository;


/**
 * 角色资源
 *
 * @author yanshikun
 * @date 2021/08/10
 */
@Repository
public interface RoleResourceDao extends BaseMapper<RoleResource> {
}
