package xyz.xiaomy.blog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.xiaomy.blog.entity.RoleMenu;
import org.springframework.stereotype.Repository;

/**
 * 角色菜单
 *
 * @author yanshikun
 * @date 2021/08/10
 */
@Repository
public interface RoleMenuDao extends BaseMapper<RoleMenu> {

}
