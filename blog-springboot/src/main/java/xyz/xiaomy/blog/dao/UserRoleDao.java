package xyz.xiaomy.blog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.xiaomy.blog.entity.UserRole;
import org.springframework.stereotype.Repository;


/**
 * 用户角色
 *
 * @author yanshikun
 * @date 2021/08/10
 */
@Repository
public interface UserRoleDao extends BaseMapper<UserRole> {

}
