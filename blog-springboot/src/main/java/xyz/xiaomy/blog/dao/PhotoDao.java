package xyz.xiaomy.blog.dao;

import xyz.xiaomy.blog.entity.Photo;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.springframework.stereotype.Repository;


/**
 * 照片映射器
 *
 * @author yanshikun
 * @date 2021/08/04
 */
@Repository
public interface PhotoDao extends BaseMapper<Photo> {



}




