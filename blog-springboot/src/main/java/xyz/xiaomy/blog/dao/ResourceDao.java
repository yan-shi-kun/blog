package xyz.xiaomy.blog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.xiaomy.blog.entity.Resource;
import org.springframework.stereotype.Repository;

/**
 * 资源
 *
 * @author yanshikun
 * @date 2021/08/10
 */
@Repository
public interface ResourceDao extends BaseMapper<Resource> {



}
