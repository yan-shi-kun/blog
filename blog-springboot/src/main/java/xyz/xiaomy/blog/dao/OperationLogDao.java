package xyz.xiaomy.blog.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import xyz.xiaomy.blog.entity.OperationLog;
import org.springframework.stereotype.Repository;


/**
 * 操作日志
 *
 * @author yanshikun
 * @date 2021/08/10
 */
@Repository
public interface OperationLogDao extends BaseMapper<OperationLog> {
}
