export default {
  "[微笑]": "http://xiaomy.xyz/static/emoji/smile.jpg",

  "[笑]": "http://xiaomy.xyz/static/emoji/dx.jpg",

  "[呲牙]": "http://xiaomy.xyz/static/emoji/cy.jpg",

  "[OK]": "http://xiaomy.xyz/static/emoji/ok.jpg",

  "[星星眼]": "http://xiaomy.xyz/static/emoji/xxy.jpg",

  "[哦呼]": "http://xiaomy.xyz/static/emoji/oh.jpg",

  "[嫌弃]": "http://xiaomy.xyz/static/emoji/xq.jpg",

  "[喜欢]": "http://xiaomy.xyz/static/emoji/xh.jpg",

  "[酸了]": "http://xiaomy.xyz/static/emoji/sl.jpg",

  "[大哭]": "http://xiaomy.xyz/static/emoji/dq.jpg?",

  "[害羞]": "http://xiaomy.xyz/static/emoji/hx.jpg",

  "[无语]": "http://xiaomy.xyz/static/emoji/wy.jpg",

  "[疑惑]": "http://xiaomy.xyz/static/emoji/yh.jpg",

  "[调皮]": "http://xiaomy.xyz/static/emoji/tiaopi.jpg",

  "[笑哭]": "http://xiaomy.xyz/static/emoji/xiaoku.jpg",

  "[奸笑]": "http://xiaomy.xyz/static/emoji/jianxiao.jpg",

  "[偷笑]": "http://xiaomy.xyz/static/emoji/touxiao.jpg",

  "[大笑]": "http://xiaomy.xyz/static/emoji/daxiao.jpg",

  "[阴险]": "http://xiaomy.xyz/static/emoji/yinxian.jpg",

  "[捂脸]": "http://xiaomy.xyz/static/emoji/wulian.jpg",

  "[呆]": "http://xiaomy.xyz/static/emoji/dai.jpg",

  "[抠鼻]": "http://xiaomy.xyz/static/emoji/koubi.jpg",

  "[惊喜]": "http://xiaomy.xyz/static/emoji/jingxi.jpg",

  "[惊讶]": "http://xiaomy.xyz/static/emoji/jingya.jpg",

  "[捂脸哭]": "http://xiaomy.xyz/static/emoji/wulianku.jpg",

  "[妙啊]": "http://xiaomy.xyz/static/emoji/miaoa.jpg",

  "[狗头]": "http://xiaomy.xyz/static/emoji/goutou.jpg",

  "[滑稽]": "http://xiaomy.xyz/static/emoji/huaji.jpg",

  "[吃瓜]": "http://xiaomy.xyz/static/emoji/chigua.jpg",

  "[打call]": "http://xiaomy.xyz/static/emoji/dacall.jpg",

  "[点赞]": "http://xiaomy.xyz/static/emoji/dianzan.jpg",

  "[鼓掌]": "http://xiaomy.xyz/static/emoji/guzhang.jpg",

  "[尴尬]": "http://xiaomy.xyz/static/emoji/ganga.jpg",

  "[冷]": "http://xiaomy.xyz/static/emoji/leng.jpg",

  "[灵魂出窍]": "http://xiaomy.xyz/static/emoji/linghunchuqiao.jpg",

  "[委屈]": "http://xiaomy.xyz/static/emoji/weiqu.jpg",

  "[傲娇]": "http://xiaomy.xyz/static/emoji/aojiao.jpg",

  "[疼]": "http://xiaomy.xyz/static/emoji/teng.jpg",

  "[吓]": "http://xiaomy.xyz/static/emoji/xia.jpg?",

  "[生病]": "http://xiaomy.xyz/static/emoji/shengbing.jpg",

  "[吐]": "http://xiaomy.xyz/static/emoji/tu.jpg",

  "[嘘声]": "http://xiaomy.xyz/static/emoji/xusheng.jpg",

  "[捂眼]": "http://xiaomy.xyz/static/emoji/wuyan.jpg",

  "[思考]": "http://xiaomy.xyz/static/emoji/sikao.jpg",

  "[再见]": "http://xiaomy.xyz/static/emoji/zaijian.jpg",

  "[翻白眼]": "http://xiaomy.xyz/static/emoji/fanbaiyan.jpg",

  "[哈欠]": "http://xiaomy.xyz/static/emoji/haqian.jpg",

  "[奋斗]": "http://xiaomy.xyz/static/emoji/fengdou.jpg",

  "[墨镜]": "http://xiaomy.xyz/static/emoji/mojing.jpg",

  "[撇嘴]": "http://xiaomy.xyz/static/emoji/piezui.jpg",

  "[难过]": "http://xiaomy.xyz/static/emoji/nanguo.jpg",

  "[抓狂]": "http://xiaomy.xyz/static/emoji/zhuakuang.jpg",

  "[生气]": "http://xiaomy.xyz/static/emoji/shengqi.jpg",

  "[爱心]": "http://xiaomy.xyz/static/emoji/aixin.jpg",

  "[胜利]": "http://xiaomy.xyz/static/emoji/shengli.jpg",

  "[保佑]": "http://xiaomy.xyz/static/emoji/baoyou.jpg?",

  "[支持]": "http://xiaomy.xyz/static/emoji/zhichi.jpg"
};
