/*
 Navicat Premium Data Transfer

 Source Server         : 8.131.54.14_blog
 Source Server Type    : MySQL
 Source Server Version : 80026
 Source Host           : 8.131.54.14:3306
 Source Schema         : blog

 Target Server Type    : MySQL
 Target Server Version : 80026
 File Encoding         : 65001

 Date: 25/09/2021 13:11:11
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for tb_article
-- ----------------------------
DROP TABLE IF EXISTS `tb_article`;
CREATE TABLE `tb_article`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NOT NULL COMMENT '作者',
  `category_id` int NULL DEFAULT NULL COMMENT '文章分类',
  `article_cover` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '文章缩略图',
  `article_title` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标题',
  `article_content` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '内容',
  `type` tinyint(1) NOT NULL DEFAULT 0 COMMENT '文章类型 1原创 2转载 3翻译',
  `original_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '原文链接',
  `is_top` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否置顶 0否 1是',
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除  0否 1是',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态值 1公开 2私密 3评论可见',
  `sortno` int NULL DEFAULT 0 COMMENT '自定义排序',
  `create_time` datetime NOT NULL COMMENT '发表时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  FULLTEXT INDEX `ft_search`(`article_content`)
) ENGINE = InnoDB AUTO_INCREMENT = 49 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_article
-- ----------------------------
INSERT INTO `tb_article` VALUES (47, 1, 184, 'https://www.static.talkxj.com/articles/bd74062266c1fb04f3084968231c0580.jpg', '测试文章', '## 目录\n\n恭喜你已成功运行博客，开启你的文章之旅吧', 1, '', 0, 1, 1, '2021-08-12 15:50:57', '2021-09-21 00:13:03');
INSERT INTO `tb_article` VALUES (48, 1, 185, 'http://8.131.54.14:83/articles/a822004a23789295bf1cc5255046b656.jpg', 'java反射机制', '# java反射机制\n## 概念\n>Java 反射机制是 Java 语言的一个重要特性。在学习 Java 反射机制前，大家应该先了解两个概念，编译期和运行期。\n\n>**编译期**:是指把源码交给编译器编译成计算机可以执行的文件的过程。在 Java 中也就是把 Java 代码编成 class 文件的过程。编译期只是做了一些翻译功能，并没有把代码放在内存中运行起来，而只是把代码当成文本进行操作，比如检查错误。\n\n>**运行期**:是把编译后的文件交给计算机执行，直到程序运行结束。所谓运行期就把在磁盘中的代码放到内存中执行起来。\n\n>**百度百科:**\nJava的反射（*reflection*）机制是指在程序的运行状态中，可以构造任意一个类的对象，可以了解任意一个对象所属的类，可以了解任意一个类的成员变量和方法，可以调用任意一个对象的属性和方法。这种动态获取程序信息以及动态调用对象的功能称为Java语言的反射机制。反射被视为==动态语言==的关键。\n\n>**个人理解：**\nJava反射机制可以视为一个输入流，将一个自定义的类，视为一个普通文件，我们可以得到该文件的所有信息，但又比输入流更加的高级，我们又可以执行该类中的功能点，简单来说，反射机制指的是程序在运行时能够获取自身的信息。在 Java 中，只要给定**类的全限定名**，就可以通过反射机制来获得类的所有信息\n\n**什么是静态语言什么是动态语言？**\n>静态类型语言：变量定义时有类型声明的语言。\n> 1. 变量的类型在编译的时候确定\n> 2. 变量的类型在运行时不能修改\n\n>动态类型语言：变量定义时无类型声明的语言。\n> 1. 变量的类型在运行的时候确定\n>  2. 变量的类型在运行可以修改\n## 如何实现java的语言动态化？\n\n 1. 通过java的反射机制\n 2. 多态，Java又可以通过接口等方式，在运行时注入相关的类的实现，所以这个又是其动态性的提现\n \n ## 如何使用java的反射机制\n **熟悉获取 Java 反射使用到类**\n \n 3. Class类\n 4. Constructor 类\n 5. Method 类\n 6. Field 类\nConstructor，Method，Field 都是通过Class类得到的，在java.lang.reflect 包下\n\n ### 掌握Class类的常用方法\n >  如何得到Class类对象？\n 	1.众所周知，所有 Java 类均继承了 Object 类，在 Object 类中定义了一个 getClass() 方法，该方法返回同一个类型为 Class 的对象。\n 	2.或者使用 Class类的静态方法forName(“全限定类名”)\n 	\n|类型  | 访问方法  | 返回值类型 | 说明\n|--|--|--|--|\n| 包路径 | getPackage() | Package 对象 | 获取该类的存放路径\n| 类名称| getName() | String 对象 | 获取该类的名称\n| 继承类| getSuperclass() | Class 对象 | 获取该类继承的类\n|实现接口	|getlnterfaces()|	Class 型数组|	获取该类实现的所有接口\n|构造方法|	getConstructor()	|Constructor 对象实例|	获取权限为 public 的构造方法，包括，所继承的父类的构造\n|| getConstructors()	|Constructor 型数组|	获取所有权限为 public 的构造方法\n|| getDeclaredContruectors()|Constructor 对象|获取当前对象的所有构造方法\n|方法|getMethods()|Method 型数组|获取权限为 public 的方法，包括父类\n||getDeclaredMethods()|Method 对象数组|获取当前对象的所有方法\n||getDeclaredMethod(String name, Class<?>... parameterTypes)|Method对象|获取本类指定的方法\n成员变量|getFields()|Field 型数组|获取所有权限为 public 的成员变量,包含父类\n||getDeclareFileds()|Field 型数组|获取当前对象的所有成员变量\n>Student类\n```java\npackage cn.entity;\n\npublic class Student {\n    private String name;\n    private Integer age;\n    private Double score;\n    public Student(){\n        System.out.println(\"执行了Student的无参构造方法---------------->\");\n    }\n\n    public Student(String name) {\n        System.out.println(\"执行了Student的有参构造方法---------------->\");\n        this.name=name;\n    }\n\n    public String getName() {\n        return name;\n    }\n\n    public void setName(String name) {\n        this.name = name;\n    }\n\n    public Integer getAge() {\n        return age;\n    }\n\n    public void setAge(Integer age) {\n        this.age = age;\n        System.out.println(\"setAge-------------------->\");\n    }\n\n    public Double getScore() {\n        return score;\n    }\n\n    public void setScore(Double score) {\n        this.score = score;\n    }\n\n    @Override\n    public String toString() {\n        return \"Student{\" +\n                \"name=\'\" + name + \'\\\'\' +\n                \", age=\" + age +\n                \", score=\" + score +\n                \'}\';\n    }\n}\n```\n\n> 示例代码：\n```java\nClass<?> cls = Class.forName(\"cn.entity.Student\");\n//得到Student类的构造器，此时并未创建实例\nConstructor  constructor= cls.getDeclaredConstructor();\n//得到Student类的所有方法，不包括父类\nMethod[] methods = cls.getDeclaredMethods()\n//得到Student类的指定方法名的方法\nMethod getName = cls.getMethod(\"getName\");\n//得到Student类的指定属性名的属性\nField age = cls.getDeclaredField(\"age\");\n```\n### 掌握 Constructor 类的常用方法\n|方法名称	| 说明 |\n|--|--|\n|isVarArgs()|	查看该构造方法是否允许带可变数量的参数，如果允许，返回 true，否则返回false|\n|getParameterTypes() |	按照声明顺序以 Class 数组的形式获取该构造方法各个参数的类型|\n|getExceptionTypes() |	以 Class 数组的形式获取该构造方法可能抛出的异常类型|\n|newInstance(Object … initargs) |	通过该构造方法利用指定参数创建一个该类型的对象，如果未设置参数则表示采用默认无参的构造方法|\n|setAccessiable(boolean flag) |	如果该构造方法的权限为 private，默认为不允许通过反射利用 \nnewlnstance()|方法创建对象。如果先执行该方法，并将入口参数设置为 true，则允许创建对象\n|getModifiers()	| 获得可以解析出该构造方法所采用修饰符的整数\n\n>示例代码：\n\n```java\n //此方法忽略构造方法的访问修饰符，不推荐使用，会破坏类的封装性，\nconstructor.setAccessible(true);\n//此步是真正的创建一个实例，执行了类的无惨构造，当无惨构造为private时无法使用，需执行setAccessible(true);才能创建实例\nObject obj = constructor.newInstance();\n```\n### Method类的常用方法\n|静态方法名称	|说明|\n|--|--|\ngetName()	|获取该方法的名称\ngetParameterTypes()|	按照声明顺序以 Class 数组的形式返回该方法各个参数的类型\ngetReturnType()|	以 Class 对象的形式获得该方法的返回值类型\ngetExceptionTypes()	|以 Class 数组的形式获得该方法可能抛出的异常类型\ninvoke(Object obj,Object...args)|	利用 args 参数执行指定对象 obj 中的该方法，返回值为 Object 类型\nisVarArgs()|	查看该方法是否允许带有可变数量的参数，如果允许返回 true，否则返回 false\ngetModifiers()|	获得可以解析出该方法所采用修饰符的整数\n\n>示例代码：\n```java\nMethod[] methods = cls.getDeclaredMethods();\n//遍历所有本类方法\nfor (Method method : methods) {\n	System.out.println(method);\n}\n//使用方法名，和指定参数类型，得到需要执行的方法，此时并未指定，只是得到该方法。参数类型，可根据对应的属性类型获取\nMethod setName = cls.getDeclaredMethod(\"setName\", String.class);\n/*此时才是真正的执行方法，\"obj\"为使用那一个对象，\n来调用这个方法，“张三”为参数的值，\n使用非反射的方式调用就是这种:((Student)obj).setName(\"张三\")*/\nsetName.invoke(obj,\"张三\");\n```\n### Field类的常用方法\n|方法名称	|说明|\n|--|--|\ngetName()	|获得该成员变量的名称\ngetType()|	获取表示该成员变量的 Class 对象\nget(Object obj)|	获得指定对象 obj 中成员变量的值，返回值为 Object 类型\nset(Object obj, Object value)|	将指定对象 obj 中成员变量的值设置为 value\ngetlnt(0bject obj)|	获得指定对象 obj 中成员类型为 int 的成员变量的值\nsetlnt(0bject obj, int i)|	将指定对象 obj 中成员变量的值设置为 i\nsetFloat(Object obj, float f)|	将指定对象 obj 中成员变量的值设置为 f\ngetBoolean(Object obj)|	获得指定对象 obj 中成员类型为 boolean 的成员变量的值\nsetBoolean(Object obj, boolean b)|	将指定对象 obj 中成员变量的值设置为 b\ngetFloat(Object obj)|	获得指定对象 obj 中成员类型为 float 的成员变量的值\nsetAccessible(boolean flag)|	此方法可以设置是否忽略权限直接访问 private 等私有权限的成员变量\ngetModifiers()|	获得可以解析出该方法所采用修饰符的整数\n>示例代码：\n```java\n//得到本类所有属性并遍历输出\nMethod[] methods = cls.getDeclaredMethods();\nfor (Method method : methods) {\n	System.out.println(method);\n}\n//得到指定名称的属性\nField age = cls.getDeclaredField(\"age\");\n //此方法属性的访问修饰符，不推荐使用，会破坏类的封装性，\nage.setAccessible(true);\n//为obj的age属性赋值，常规写法，为：((Student)obj).age=18;age为私有的，无法调用\nage.set(obj,18);\n```\n### 那么反射到底有什么用？\n反射最主要还是运用在框架的设计中，了解了反射才更好的了解一些框架功能实现的原理。\n### 我们所学的springmvc中的某些功能使用到的反射：\n\n 1. 页面向controller中传值，的参数自动映射\n 	 他的下一个博客\n	 [ 使用反射自定义一个简易的参数自动映射](https://blog.csdn.net/qq_44621891/article/details/108910305).\n 2.  DispatchServlet(前端控制器)\n    他的下一个博客\n	[使用反射自定义一个简易DispatchServlet](https://blog.csdn.net/qq_44621891/article/details/108913692)\n	\n## Java 反射机制的优缺点\n**优点**： \n\n - 能够运行时动态获取类的实例，大大提高系统的灵活性和扩展性。\n - 与 Java 动态编译相结合，可以实现无比强大的功能。\n- 对于 Java 这种先编译再运行的语言，能够让我们很方便的创建灵活的代码，这些代码可以在运行时装配，无需在组件之间进行源代码的链接，更加容易实现面向对象。\n\n**缺点**：\n- 一个主要的缺点是对性能有影响，因此，如果不需要动态地创建一个对象，那么就不需要用反射；\n- 反射调用方法时可以忽略权限检查，获取这个类的私有方法和属性，因此可能会破坏类的封装性而导致安全问题。\n## 总结：\n使用java的反射机制，一般需要遵循三步：\n\n- 获得你想操作类的Class对象，使用全限定类名，“包名+类名””\n- 使用Class对象，得到构造，方法，属性，\n-	再使用具体的构造，方法，属性，来操作对应的方法，', 2, 'https://blog.csdn.net/qq_44621891/article/details/108902719', 0, 0, 1, '2021-09-21 00:05:56', '2021-09-21 00:11:59');

-- ----------------------------
-- Table structure for tb_article_tag
-- ----------------------------
DROP TABLE IF EXISTS `tb_article_tag`;
CREATE TABLE `tb_article_tag`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `article_id` int NOT NULL COMMENT '文章id',
  `tag_id` int NOT NULL COMMENT '标签id',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_article_tag_1`(`article_id`) USING BTREE,
  INDEX `fk_article_tag_2`(`tag_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 498 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_article_tag
-- ----------------------------
INSERT INTO `tb_article_tag` VALUES (494, 47, 27);
INSERT INTO `tb_article_tag` VALUES (497, 48, 29);

-- ----------------------------
-- Table structure for tb_category
-- ----------------------------
DROP TABLE IF EXISTS `tb_category`;
CREATE TABLE `tb_category`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `category_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '分类名',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 186 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_category
-- ----------------------------
INSERT INTO `tb_category` VALUES (184, '测试分类', '2021-08-12 15:50:57', NULL);
INSERT INTO `tb_category` VALUES (185, 'java专栏', '2021-09-20 23:47:22', NULL);

-- ----------------------------
-- Table structure for tb_chat_record
-- ----------------------------
DROP TABLE IF EXISTS `tb_chat_record`;
CREATE TABLE `tb_chat_record`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '头像',
  `content` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '聊天内容',
  `ip_address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'ip地址',
  `ip_source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT 'ip来源',
  `type` tinyint NOT NULL COMMENT '类型',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1890 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_chat_record
-- ----------------------------

-- ----------------------------
-- Table structure for tb_comment
-- ----------------------------
DROP TABLE IF EXISTS `tb_comment`;
CREATE TABLE `tb_comment`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `user_id` int NOT NULL COMMENT '评论用户Id',
  `article_id` int NULL DEFAULT NULL COMMENT '评论文章id',
  `comment_content` text CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '评论内容',
  `reply_user_id` int NULL DEFAULT NULL COMMENT '回复用户id',
  `parent_id` int NULL DEFAULT NULL COMMENT '父评论id',
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除  0否 1是',
  `is_review` tinyint(1) NOT NULL DEFAULT 1 COMMENT '是否审核',
  `create_time` datetime NOT NULL COMMENT '评论时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_comment_user`(`user_id`) USING BTREE,
  INDEX `fk_comment_article`(`article_id`) USING BTREE,
  INDEX `fk_comment_parent`(`parent_id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 426 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_comment
-- ----------------------------
INSERT INTO `tb_comment` VALUES (425, 1, 47, '评论', NULL, NULL, 0, 1, '2021-08-12 15:53:27', NULL);

-- ----------------------------
-- Table structure for tb_friend_link
-- ----------------------------
DROP TABLE IF EXISTS `tb_friend_link`;
CREATE TABLE `tb_friend_link`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `link_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '链接名',
  `link_avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '链接头像',
  `link_address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '链接地址',
  `link_intro` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '链接介绍',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE,
  INDEX `fk_friend_link_user`(`link_name`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_friend_link
-- ----------------------------
INSERT INTO `tb_friend_link` VALUES (20, '风丶宇的个人博客', 'https://www.static.talkxj.com/photos/b553f564f81a80dc338695acb1b475d2.jpg', 'https://www.talkxj.com/', '成事在人 谋事在天', '2021-08-12 15:56:48', NULL);
INSERT INTO `tb_friend_link` VALUES (21, '高清壁纸', 'http://browser9.qhimg.com/bdm/960_593_0/t01028e5f2ec69e423d.jpg', 'http://www.2800.so/pic/', '高清壁纸', '2021-09-20 23:55:28', '2021-09-20 23:56:15');

-- ----------------------------
-- Table structure for tb_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_menu`;
CREATE TABLE `tb_menu`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单名',
  `path` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单路径',
  `component` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '组件',
  `icon` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '菜单icon',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `order_num` tinyint(1) NOT NULL COMMENT '排序',
  `parent_id` int NULL DEFAULT NULL COMMENT '父id',
  `is_hidden` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否隐藏  0否1是',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 215 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_menu
-- ----------------------------
INSERT INTO `tb_menu` VALUES (1, '首页', '/', '/home/Home.vue', 'el-icon-myshouye', '2021-01-26 17:06:51', '2021-01-26 17:06:53', 1, NULL, 0);
INSERT INTO `tb_menu` VALUES (2, '文章管理', '/article-submenu', 'Layout', 'el-icon-mywenzhang-copy', '2021-01-25 20:43:07', '2021-01-25 20:43:09', 2, NULL, 0);
INSERT INTO `tb_menu` VALUES (3, '消息管理', '/message-submenu', 'Layout', 'el-icon-myxiaoxi', '2021-01-25 20:44:17', '2021-01-25 20:44:20', 3, NULL, 0);
INSERT INTO `tb_menu` VALUES (4, '系统管理', '/system-submenu', 'Layout', 'el-icon-myshezhi', '2021-01-25 20:45:57', '2021-01-25 20:45:59', 5, NULL, 0);
INSERT INTO `tb_menu` VALUES (5, '个人中心', '/setting', '/setting/Setting.vue', 'el-icon-myuser', '2021-01-26 17:22:38', '2021-01-26 17:22:41', 7, NULL, 0);
INSERT INTO `tb_menu` VALUES (6, '发布文章', '/articles', '/article/Article.vue', 'el-icon-myfabiaowenzhang', '2021-01-26 14:30:48', '2021-01-26 14:30:51', 1, 2, 0);
INSERT INTO `tb_menu` VALUES (7, '修改文章', '/articles/*', '/article/Article.vue', 'el-icon-myfabiaowenzhang', '2021-01-26 14:31:32', '2021-01-26 14:31:34', 2, 2, 1);
INSERT INTO `tb_menu` VALUES (8, '文章列表', '/article-list', '/article/ArticleList.vue', 'el-icon-mywenzhangliebiao', '2021-01-26 14:32:13', '2021-01-26 14:32:16', 3, 2, 0);
INSERT INTO `tb_menu` VALUES (9, '分类管理', '/categories', '/category/Category.vue', 'el-icon-myfenlei', '2021-01-26 14:33:42', '2021-01-26 14:33:43', 4, 2, 0);
INSERT INTO `tb_menu` VALUES (10, '标签管理', '/tags', '/tag/Tag.vue', 'el-icon-myicontag', '2021-01-26 14:34:33', '2021-01-26 14:34:36', 5, 2, 0);
INSERT INTO `tb_menu` VALUES (11, '评论管理', '/comments', '/comment/Comment.vue', 'el-icon-mypinglunzu', '2021-01-26 14:35:31', '2021-01-26 14:35:34', 1, 3, 0);
INSERT INTO `tb_menu` VALUES (12, '留言管理', '/messages', '/message/Message.vue', 'el-icon-myliuyan', '2021-01-26 14:36:09', '2021-01-26 14:36:13', 2, 3, 0);
INSERT INTO `tb_menu` VALUES (13, '用户列表', '/users', '/user/User.vue', 'el-icon-myyonghuliebiao', '2021-01-26 14:38:09', '2021-01-26 14:38:12', 1, 202, 0);
INSERT INTO `tb_menu` VALUES (14, '角色管理', '/roles', '/role/Role.vue', 'el-icon-myjiaoseliebiao', '2021-01-26 14:39:01', '2021-01-26 14:39:03', 2, 213, 0);
INSERT INTO `tb_menu` VALUES (15, '接口管理', '/resources', '/resource/Resource.vue', 'el-icon-myjiekouguanli', '2021-01-26 14:40:14', '2021-08-07 20:00:28', 2, 213, 0);
INSERT INTO `tb_menu` VALUES (16, '菜单管理', '/menus', '/menu/Menu.vue', 'el-icon-mycaidan', '2021-01-26 14:40:54', '2021-08-07 10:18:49', 2, 213, 0);
INSERT INTO `tb_menu` VALUES (17, '友链管理', '/links', '/friendLink/FriendLink.vue', 'el-icon-mydashujukeshihuaico-', '2021-01-26 14:41:35', '2021-01-26 14:41:37', 3, 4, 0);
INSERT INTO `tb_menu` VALUES (18, '关于我', '/about', '/about/About.vue', 'el-icon-myguanyuwo', '2021-01-26 14:42:05', '2021-01-26 14:42:10', 4, 4, 0);
INSERT INTO `tb_menu` VALUES (19, '日志管理', '/log-submenu', 'Layout', 'el-icon-myguanyuwo', '2021-01-31 21:33:56', '2021-01-31 21:33:59', 6, NULL, 0);
INSERT INTO `tb_menu` VALUES (20, '操作日志', '/operation/log', '/log/Operation.vue', 'el-icon-myguanyuwo', '2021-01-31 15:53:21', '2021-01-31 15:53:25', 1, 19, 0);
INSERT INTO `tb_menu` VALUES (201, '在线用户', '/online/users', '/user/Online.vue', 'el-icon-myyonghuliebiao', '2021-02-05 14:59:51', '2021-02-05 14:59:53', 7, 202, 0);
INSERT INTO `tb_menu` VALUES (202, '用户管理', '/users-submenu', 'Layout', 'el-icon-myyonghuliebiao', '2021-02-06 23:44:59', '2021-02-06 23:45:03', 4, NULL, 0);
INSERT INTO `tb_menu` VALUES (205, '相册管理', '/album-submenu', 'Layout', 'el-icon-myimage-fill', '2021-08-03 15:10:54', '2021-08-07 20:02:06', 5, NULL, 0);
INSERT INTO `tb_menu` VALUES (206, '相册列表', '/albums', '/album/Album.vue', 'el-icon-myzhaopian', '2021-08-03 20:29:19', '2021-08-04 11:45:47', 1, 205, 0);
INSERT INTO `tb_menu` VALUES (208, '照片管理', '/albums/:albumId', '/album/Photo.vue', 'el-icon-myzhaopian', '2021-08-03 21:37:47', '2021-08-05 10:24:08', 1, 205, 1);
INSERT INTO `tb_menu` VALUES (209, '页面管理', '/pages', '/page/Page.vue', 'el-icon-myyemianpeizhi', '2021-08-04 11:36:27', '2021-08-07 20:01:26', 2, 4, 0);
INSERT INTO `tb_menu` VALUES (210, '照片回收站', '/photos/delete', '/album/Delete.vue', 'el-icon-myhuishouzhan', '2021-08-05 13:55:19', NULL, 3, 205, 1);
INSERT INTO `tb_menu` VALUES (213, '权限管理', '/permission-submenu', 'Layout', 'el-icon-mydaohanglantubiao_quanxianguanli', '2021-08-07 19:56:55', '2021-08-07 19:59:40', 4, NULL, 0);
INSERT INTO `tb_menu` VALUES (214, '网站管理', '/website', '/website/Website.vue', 'el-icon-myxitong', '2021-08-07 20:06:41', NULL, 1, 4, 0);

-- ----------------------------
-- Table structure for tb_message
-- ----------------------------
DROP TABLE IF EXISTS `tb_message`;
CREATE TABLE `tb_message`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '昵称',
  `avatar` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '头像',
  `message_content` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '留言内容',
  `ip_address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户ip',
  `ip_source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户地址',
  `time` tinyint(1) NULL DEFAULT NULL COMMENT '弹幕速度',
  `is_review` tinyint NOT NULL DEFAULT 1 COMMENT '是否审核',
  `create_time` datetime NOT NULL COMMENT '发布时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 3460 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_message
-- ----------------------------
INSERT INTO `tb_message` VALUES (3458, '管理员', 'http://8.131.54.14:83/avatar/4610eddb63a74e4610004295d321d7e8.jpeg', '9月25日', '125.46.164.45', '河南省郑州市 联通', 9, 1, '2021-09-25 00:04:51', NULL);
INSERT INTO `tb_message` VALUES (3459, '游客', 'https://www.static.talkxj.com/photos/0bca52afdb2b9998132355d716390c9f.png', '1', '125.46.164.45', '河南省郑州市 联通', 9, 1, '2021-09-25 00:05:08', NULL);

-- ----------------------------
-- Table structure for tb_operation_log
-- ----------------------------
DROP TABLE IF EXISTS `tb_operation_log`;
CREATE TABLE `tb_operation_log`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `opt_module` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作模块',
  `opt_type` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作类型',
  `opt_url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作url',
  `opt_method` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作方法',
  `opt_desc` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作描述',
  `request_param` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '请求参数',
  `request_method` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '请求方式',
  `response_data` longtext CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '返回数据',
  `user_id` int NOT NULL COMMENT '用户id',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户昵称',
  `ip_address` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作ip',
  `ip_source` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '操作地址',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 733 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_operation_log
-- ----------------------------
INSERT INTO `tb_operation_log` VALUES (688, '角色模块', '新增或修改', '/admin/role', 'com.minzheng.blog.controller.RoleController.saveOrUpdateRole', '保存或更新角色', '[{\"id\":1,\"resourceIdList\":[165,192,193,194,195,166,183,184,246,247,167,199,200,201,168,185,186,187,188,189,190,191,254,169,208,209,170,234,235,236,237,171,213,214,215,216,217,224,172,240,241,244,245,267,269,270,173,239,242,276,174,205,206,207,175,218,219,220,221,222,223,176,202,203,204,230,238,177,229,232,233,243,178,196,197,198,257,258,179,225,226,227,228,231,180,210,211,212],\"roleLabel\":\"admin\",\"roleName\":\"管理员\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '127.0.0.1', '', '2021-08-24 11:25:33', NULL);
INSERT INTO `tb_operation_log` VALUES (689, '角色模块', '新增或修改', '/admin/role', 'com.minzheng.blog.controller.RoleController.saveOrUpdateRole', '保存或更新角色', '[{\"id\":3,\"resourceIdList\":[192,195,183,246,199,185,191,254,208,234,237,213,241,239,276,205,218,223,202,230,238,232,243,196,257,258,225,231,210],\"roleLabel\":\"test\",\"roleName\":\"测试\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '127.0.0.1', '', '2021-08-24 11:25:40', NULL);
INSERT INTO `tb_operation_log` VALUES (690, '页面模块', '新增或修改', '/admin/pages', 'com.minzheng.blog.controller.PageController.saveOrUpdatePage', '保存或更新页面', '[{\"id\":1,\"pageCover\":\"http://8.131.54.14:83/config/0a129ee6c2764fac6d5eef2394db9b7a.jpg\",\"pageLabel\":\"home\",\"pageName\":\"首页\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:12:03', NULL);
INSERT INTO `tb_operation_log` VALUES (691, '页面模块', '新增或修改', '/admin/pages', 'com.minzheng.blog.controller.PageController.saveOrUpdatePage', '保存或更新页面', '[{\"id\":1,\"pageCover\":\"http://8.131.54.14:83/config/25ad2ad8ef92ac45b659d6af873d93b3.jpg\",\"pageLabel\":\"home\",\"pageName\":\"首页\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:12:58', NULL);
INSERT INTO `tb_operation_log` VALUES (692, '页面模块', '新增或修改', '/admin/pages', 'com.minzheng.blog.controller.PageController.saveOrUpdatePage', '保存或更新页面', '[{\"id\":7,\"pageCover\":\"http://8.131.54.14:83/config/c7bbd3c43373872a08e0e577858e2c51.jpg\",\"pageLabel\":\"about\",\"pageName\":\"关于\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:13:38', NULL);
INSERT INTO `tb_operation_log` VALUES (693, '页面模块', '新增或修改', '/admin/pages', 'com.minzheng.blog.controller.PageController.saveOrUpdatePage', '保存或更新页面', '[{\"id\":6,\"pageCover\":\"http://8.131.54.14:83/config/0a129ee6c2764fac6d5eef2394db9b7a.jpg\",\"pageLabel\":\"link\",\"pageName\":\"友链\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:14:54', NULL);
INSERT INTO `tb_operation_log` VALUES (694, '页面模块', '新增或修改', '/admin/pages', 'com.minzheng.blog.controller.PageController.saveOrUpdatePage', '保存或更新页面', '[{\"id\":1,\"pageCover\":\"http://8.131.54.14:83/config/2a273104a938161f2151dbc46a20cd23.jpg\",\"pageLabel\":\"home\",\"pageName\":\"首页\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:15:17', NULL);
INSERT INTO `tb_operation_log` VALUES (695, '相册模块', '新增或修改', '/admin/photos/albums', 'com.minzheng.blog.controller.PhotoAlbumController.saveOrUpdatePhotoAlbum', '保存或更新相册', '[{\"albumCover\":\"http://8.131.54.14:83/config/92e78b7c6586c5c5620190964908c6c7.jpg\",\"albumDesc\":\"精美高清图片\",\"albumName\":\"网站页面\",\"status\":1}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:17:06', NULL);
INSERT INTO `tb_operation_log` VALUES (696, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":3,\"photoUrlList\":[\"http://8.131.54.14:83/articles/92e78b7c6586c5c5620190964908c6c7.jpg\",\"http://8.131.54.14:83/articles/c7bbd3c43373872a08e0e577858e2c51.jpg\",\"http://8.131.54.14:83/articles/ab11cc8ca3d2258dae7b40a802d20e83.jpg\",\"http://8.131.54.14:83/articles/9c7fc5035adba1571d27549d2d98e123.jpg\",\"http://8.131.54.14:83/articles/4adce412894f3a9c2c1a942065859037.jpg\",\"http://8.131.54.14:83/articles/ca0bf4cf744e32d694f7965450fd9d16.jpg\",\"http://8.131.54.14:83/articles/57f840c784d6e921b9be6324f2d038f1.jpg\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:21:20', NULL);
INSERT INTO `tb_operation_log` VALUES (697, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":3,\"photoUrlList\":[\"http://8.131.54.14:83/articles/00212d0ec9c7a3d7204bc81fd68a2ae8.jpg\",\"http://8.131.54.14:83/articles/2a273104a938161f2151dbc46a20cd23.jpg\",\"http://8.131.54.14:83/articles/580a71e15056ca5af9d854eef89956d8.jpg\",\"http://8.131.54.14:83/articles/0cb8cc1fe5b09776215d0be91c6b6f13.jpg\",\"http://8.131.54.14:83/articles/479f4af71fd0addfe4f9fb9fb054c2e6.jpg\",\"http://8.131.54.14:83/articles/382a1e91f66baef5be699be7b68cc7da.jpg\",\"http://8.131.54.14:83/articles/ecfe698a85901b9fc504bf94e52d5da9.jpg\",\"http://8.131.54.14:83/articles/26bfe22548973428ddf3532c15095af1.jpg\",\"http://8.131.54.14:83/articles/3bf823d4221a27d41bda273beb1f0e14.jpg\",\"http://8.131.54.14:83/articles/d0d1aa5e0f1c1b49fe4346ec6a93c0a1.jpg\",\"http://8.131.54.14:83/articles/1a69ad8431e57ab978d7cbf2dee6662f.jpg\",\"http://8.131.54.14:83/articles/3571985233db5f47ac0c60cc4a3cf2b4.jpg\",\"http://8.131.54.14:83/articles/a70032ec9baa881321abff48bca35de3.jpg\",\"http://8.131.54.14:83/articles/3a32e1783ad95aefcd7a4cd15d53aa48.jpg\",\"http://8.131.54.14:83/articles/2662bc80c6d1bc871d8276fc7852425b.jpg\",\"http://8.131.54.14:83/articles/cedc33b0045f5d4702b9b8ba3692978b.jpg\",\"http://8.131.54.14:83/articles/902661f2bbbcf62268637c0c92ead031.jpg\",\"http://8.131.54.14:83/articles/58ca41093f2ff9bd2fe5640113a35ff1.jpg\",\"http://8.131.54.14:83/articles/0c674d8258c7f4ece3dd7921eeb437ec.jpg\",\"http://8.131.54.14:83/articles/daba30642c8cefbf8f3b8bbbd3236ead.jpg\",\"http://8.131.54.14:83/articles/d3447e329c1ecbf753c96dedeee09ced.jpg\",\"http://8.131.54.14:83/articles/7f00a64f169e9294fbb29c8e010af73a.jpg\",\"http://8.131.54.14:83/articles/4406841ec66c688e8450f84cbd557b7b.jpg\",\"http://8.131.54.14:83/articles/9ad17bd824f692636d0f20f0722b005b.jpg\",\"http://8.131.54.14:83/articles/a91b381285b6302184a3cadc9147b14e.jpg\",\"http://8.131.54.14:83/articles/d0c32b20289b767094b752ba922dfc68.jpg\",\"http://8.131.54.14:83/articles/d057ec5e5d83c0fa52de422cbb3e46f4.jpg\",\"http://8.131.54.14:83/articles/25ad2ad8ef92ac45b659d6af873d93b3.jpg\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:29:36', NULL);
INSERT INTO `tb_operation_log` VALUES (698, '相册模块', '新增或修改', '/admin/photos/albums', 'com.minzheng.blog.controller.PhotoAlbumController.saveOrUpdatePhotoAlbum', '保存或更新相册', '[{\"albumCover\":\"http://8.131.54.14:83/config/92e78b7c6586c5c5620190964908c6c7.jpg\",\"albumDesc\":\"精美高清博客配图\",\"albumName\":\"网站页面\",\"id\":3,\"status\":1}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:31:04', NULL);
INSERT INTO `tb_operation_log` VALUES (699, '相册模块', '新增或修改', '/admin/photos/albums', 'com.minzheng.blog.controller.PhotoAlbumController.saveOrUpdatePhotoAlbum', '保存或更新相册', '[{\"albumCover\":\"http://8.131.54.14:83/config/92e78b7c6586c5c5620190964908c6c7.jpg\",\"albumDesc\":\"精美高清博客配图\",\"albumName\":\"博客配图\",\"id\":3,\"status\":1}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:31:26', NULL);
INSERT INTO `tb_operation_log` VALUES (700, '相册模块', '新增或修改', '/admin/photos/albums', 'com.minzheng.blog.controller.PhotoAlbumController.saveOrUpdatePhotoAlbum', '保存或更新相册', '[{\"albumCover\":\"http://8.131.54.14:83/config/5009ed6b0923c395ede699485032c146.jpg\",\"albumDesc\":\"4K高清壁纸\",\"albumName\":\"壁纸\",\"status\":1}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:40:11', NULL);
INSERT INTO `tb_operation_log` VALUES (701, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":4,\"photoUrlList\":[\"http://8.131.54.14:83/articles/683e6d5603cad71cc046bc0b1e9e4264.jpg\",\"http://8.131.54.14:83/articles/5244aa98f61062f3c47d2ac573538843.jpg\",\"http://8.131.54.14:83/articles/f5e8a9131fc75c7036399b748d25b76a.jpg\",\"http://8.131.54.14:83/articles/5009ed6b0923c395ede699485032c146.jpg\",\"http://8.131.54.14:83/articles/75c47155cf833ef32aeb635b52884397.jpg\",\"http://8.131.54.14:83/articles/a822004a23789295bf1cc5255046b656.jpg\",\"http://8.131.54.14:83/articles/1747d47ec70c992f20a93d722c6ab779.jpg\",\"http://8.131.54.14:83/articles/6acd60b75b7b0ffea7e543f1c30c1b3b.jpg\",\"http://8.131.54.14:83/articles/25d8e355d844357acab5e9bb9fa106ec.jpg\",\"http://8.131.54.14:83/articles/67fc681c9311571e753cf450e5af8e1c.jpg\",\"http://8.131.54.14:83/articles/7dbba03b180e920870bee3924ca5579b.jpg\",\"http://8.131.54.14:83/articles/6fda6624e5d4e80edc3c851f633268da.jpg\",\"http://8.131.54.14:83/articles/2ac8957f9c7b35a26b5408760c21839c.jpg\",\"http://8.131.54.14:83/articles/ea74a2f80704b244467dfaa4d99604a4.jpg\",\"http://8.131.54.14:83/articles/ddb5646607568a9638f6cf2dc8f2d5b7.jpg\",\"http://8.131.54.14:83/articles/264b564911f49ec4bcd55c1ff6fb5f16.jpg\",\"http://8.131.54.14:83/articles/a936814cffa6f4d5177e6183a0001d05.jpg\",\"http://8.131.54.14:83/articles/94b70a9d5525573cb34747e3d5c22a19.jpg\",\"http://8.131.54.14:83/articles/a55fd7131261214530717fcf42e4c3dc.jpg\",\"http://8.131.54.14:83/articles/66cd740e3fc2db2924686aa7cfd43127.jpg\",\"http://8.131.54.14:83/articles/ddb58dee0a4ef8ce8d38e7228adc73fe.jpg\",\"http://8.131.54.14:83/articles/3f5d1cc80cbee3bcef8be14e3e2ae629.jpg\",\"http://8.131.54.14:83/articles/a1f3fef23a7f8c5fe8b7a51a5b558fa2.jpg\",\"http://8.131.54.14:83/articles/1ff0a8942e3993970341dad4446a484e.jpg\",\"http://8.131.54.14:83/articles/22f2a50e4e0d4499eb551791024623a3.jpg\",\"http://8.131.54.14:83/articles/75772f33629d416b013cc45335827f47.jpg\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:41:39', NULL);
INSERT INTO `tb_operation_log` VALUES (702, '分类模块', '新增或修改', '/admin/categories', 'com.minzheng.blog.controller.CategoryController.saveOrUpdateCategory', '添加或修改分类', '[{\"categoryName\":\"java专栏\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:47:22', NULL);
INSERT INTO `tb_operation_log` VALUES (703, '标签模块', '新增或修改', '/admin/tags', 'com.minzheng.blog.controller.TagController.saveOrUpdateTag', '添加或修改标签', '[{\"tagName\":\"bug记录\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:47:55', NULL);
INSERT INTO `tb_operation_log` VALUES (704, '友链模块', '新增或修改', '/admin/links', 'com.minzheng.blog.controller.FriendLinkController.saveOrUpdateFriendLink', '保存或修改友链', '[{\"linkAddress\":\"http://www.2800.so/pic/\",\"linkAvatar\":\"http://browser9.qhimg.com/bdm/480_296_0/t01753453b660de14e9.jpg\",\"linkIntro\":\"高清壁纸\",\"linkName\":\"高清壁纸\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:55:28', NULL);
INSERT INTO `tb_operation_log` VALUES (705, '友链模块', '新增或修改', '/admin/links', 'com.minzheng.blog.controller.FriendLinkController.saveOrUpdateFriendLink', '保存或修改友链', '[{\"id\":21,\"linkAddress\":\"http://www.2800.so/pic/\",\"linkAvatar\":\"http://browser9.qhimg.com/bdm/960_593_0/t01028e5f2ec69e423d.jpg\",\"linkIntro\":\"高清壁纸\",\"linkName\":\"高清壁纸\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-20 23:56:15', NULL);
INSERT INTO `tb_operation_log` VALUES (706, '文章模块', '新增或修改', '/admin/articles', 'com.minzheng.blog.controller.ArticleController.saveOrUpdateArticle', '添加或修改文章', '[{\"articleContent\":\"@[TOC](java反射机制)\\n## 概念\\n>Java 反射机制是 Java 语言的一个重要特性。在学习 Java 反射机制前，大家应该先了解两个概念，编译期和运行期。\\n\\n>**编译期**:是指把源码交给编译器编译成计算机可以执行的文件的过程。在 Java 中也就是把 Java 代码编成 class 文件的过程。编译期只是做了一些翻译功能，并没有把代码放在内存中运行起来，而只是把代码当成文本进行操作，比如检查错误。\\n\\n>**运行期**:是把编译后的文件交给计算机执行，直到程序运行结束。所谓运行期就把在磁盘中的代码放到内存中执行起来。\\n\\n>**百度百科:**\\nJava的反射（*reflection*）机制是指在程序的运行状态中，可以构造任意一个类的对象，可以了解任意一个对象所属的类，可以了解任意一个类的成员变量和方法，可以调用任意一个对象的属性和方法。这种动态获取程序信息以及动态调用对象的功能称为Java语言的反射机制。反射被视为==动态语言==的关键。\\n\\n>**个人理解：**\\nJava反射机制可以视为一个输入流，将一个自定义的类，视为一个普通文件，我们可以得到该文件的所有信息，但又比输入流更加的高级，我们又可以执行该类中的功能点，简单来说，反射机制指的是程序在运行时能够获取自身的信息。在 Java 中，只要给定**类的全限定名**，就可以通过反射机制来获得类的所有信息\\n\\n**什么是静态语言什么是动态语言？**\\n>静态类型语言：变量定义时有类型声明的语言。\\n> 1. 变量的类型在编译的时候确定\\n> 2. 变量的类型在运行时不能修改\\n\\n>动态类型语言：变量定义时无类型声明的语言。\\n> 1. 变量的类型在运行的时候确定\\n>  2. 变量的类型在运行可以修改\\n## 如何实现java的语言动态化？\\n\\n 1. 通过java的反射机制\\n 2. 多态，Java又可以通过接口等方式，在运行时注入相关的类的实现，所以这个又是其动态性的提现\\n \\n ## 如何使用java的反射机制\\n **熟悉获取 Java 反射使用到类**\\n \\n 3. Class类\\n 4. Constructor 类\\n 5. Method 类\\n 6. Field 类\\nConstructor，Method，Field 都是通过Class类得到的，在java.lang.reflect 包下\\n\\n ### 掌握Class类的常用方法\\n >  如何得到Class类对象？\\n \\t1.众所周知，所有 Java 类均继承了 Object 类，在 Object 类中定义了一个 getClass() 方法，该方法返回同一个类型为 Class 的对象。\\n \\t2.或者使用 Class类的静态方法forName(“全限定类名”)\\n \\t\\n|类型  | 访问方法  | 返回值类型 | 说明\\n|--|--|--|--|\\n| 包路径 | getPackage() | Package 对象 | 获取该类的存放路径\\n| 类名称| getName() | String 对象 | 获取该类的名称\\n| 继承类| getSuperclass() | Class 对象 | 获取该类继承的类\\n|实现接口\\t|getlnterfaces()|\\tClass 型数组|\\t获取该类实现的所有接口\\n|构造方法|\\tgetConstructor()\\t|Constructor 对象实例|\\t获取权限为 public 的构造方法，包括，所继承的父类的构造\\n|| getConstructors()\\t|Constructor 型数组|\\t获取所有权限为 public 的构造方法\\n|| getDeclaredContruectors()|Constructor 对象|获取当前对象的所有构造方法\\n|方法|getMethods()|Method 型数组|获取权限为 public 的方法，包括父类\\n||getDeclaredMethods()|Method 对象数组|获取当前对象的所有方法\\n||getDeclaredMethod(String name, Class<?>... parameterTypes)|Method对象|获取本类指定的方法\\n成员变量|getFields()|Field 型数组|获取所有权限为 public 的成员变量,包含父类\\n||getDeclareFileds()|Field 型数组|获取当前对象的所有成员变量\\n>Student类\\n```java\\npackage cn.entity;\\n\\npublic class Student {\\n    private String name;\\n    private Integer age;\\n    private Double score;\\n    public Student(){\\n        System.out.println(\\\"执行了Student的无参构造方法---------------->\\\");\\n    }\\n\\n    public Student(String name) {\\n        System.out.println(\\\"执行了Student的有参构造方法---------------->\\\");\\n        this.name=name;\\n    }\\n\\n    public String getName() {\\n        return name;\\n    }\\n\\n    public void setName(String name) {\\n        this.name = name;\\n    }\\n\\n    public Integer getAge() {\\n        return age;\\n    }\\n\\n    public void setAge(Integer age) {\\n        this.age = age;\\n        System.out.println(\\\"setAge-------------------->\\\");\\n    }\\n\\n    public Double getScore() {\\n        return score;\\n    }\\n\\n    public void setScore(Double score) {\\n        this.score = score;\\n    }\\n\\n    @Override\\n    public String toString() {\\n        return \\\"Student{\\\" +\\n                \\\"name=\'\\\" + name + \'\\\\\'\' +\\n                \\\", age=\\\" + age +\\n                \\\", score=\\\" + score +\\n                \'}\';\\n    }\\n}\\n```\\n\\n> 示例代码：\\n```java\\nClass<?> cls = Class.forName(\\\"cn.entity.Student\\\");\\n//得到Student类的构造器，此时并未创建实例\\nConstructor  constructor= cls.getDeclaredConstructor();\\n//得到Student类的所有方法，不包括父类\\nMethod[] methods = cls.getDeclaredMethods()\\n//得到Student类的指定方法名的方法\\nMethod getName = cls.getMethod(\\\"getName\\\");\\n//得到Student类的指定属性名的属性\\nField age = cls.getDeclaredField(\\\"age\\\");\\n```\\n### 掌握 Constructor 类的常用方法\\n|方法名称\\t| 说明 |\\n|--|--|\\n|isVarArgs()|\\t查看该构造方法是否允许带可变数量的参数，如果允许，返回 true，否则返回false|\\n|getParameterTypes() |\\t按照声明顺序以 Class 数组的形式获取该构造方法各个参数的类型|\\n|getExceptionTypes() |\\t以 Class 数组的形式获取该构造方法可能抛出的异常类型|\\n|newInstance(Object … initargs) |\\t通过该构造方法利用指定参数创建一个该类型的对象，如果未设置参数则表示采用默认无参的构造方法|\\n|setAccessiable(boolean flag) |\\t如果该构造方法的权限为 private，默认为不允许通过反射利用 \\nnewlnstance()|方法创建对象。如果先执行该方法，并将入口参数设置为 true，则允许创建对象\\n|getModifiers()\\t| 获得可以解析出该构造方法所采用修饰符的整数\\n\\n>示例代码：\\n\\n```java\\n //此方法忽略构造方法的访问修饰符，不推荐使用，会破坏类的封装性，\\nconstructor.setAccessible(true);\\n//此步是真正的创建一个实例，执行了类的无惨构造，当无惨构造为private时无法使用，需执行setAccessible(true);才能创建实例\\nObject obj = constructor.newInstance();\\n```\\n### Method类的常用方法\\n|静态方法名称\\t|说明|\\n|--|--|\\ngetName()\\t|获取该方法的名称\\ngetParameterTypes()|\\t按照声明顺序以 Class 数组的形式返回该方法各个参数的类型\\ngetReturnType()|\\t以 Class 对象的形式获得该方法的返回值类型\\ngetExceptionTypes()\\t|以 Class 数组的形式获得该方法可能抛出的异常类型\\ninvoke(Object obj,Object...args)|\\t利用 args 参数执行指定对象 obj 中的该方法，返回值为 Object 类型\\nisVarArgs()|\\t查看该方法是否允许带有可变数量的参数，如果允许返回 true，否则返回 false\\ngetModifiers()|\\t获得可以解析出该方法所采用修饰符的整数\\n\\n>示例代码：\\n```java\\nMethod[] methods = cls.getDeclaredMethods();\\n//遍历所有本类方法\\nfor (Method method : methods) {\\n\\tSystem.out.println(method);\\n}\\n//使用方法名，和指定参数类型，得到需要执行的方法，此时并未指定，只是得到该方法。参数类型，可根据对应的属性类型获取\\nMethod setName = cls.getDeclaredMethod(\\\"setName\\\", String.class);\\n/*此时才是真正的执行方法，\\\"obj\\\"为使用那一个对象，\\n来调用这个方法，“张三”为参数的值，\\n使用非反射的方式调用就是这种:((Student)obj).setName(\\\"张三\\\")*/\\nsetName.invoke(obj,\\\"张三\\\");\\n```\\n### Field类的常用方法\\n|方法名称\\t|说明|\\n|--|--|\\ngetName()\\t|获得该成员变量的名称\\ngetType()|\\t获取表示该成员变量的 Class 对象\\nget(Object obj)|\\t获得指定对象 obj 中成员变量的值，返回值为 Object 类型\\nset(Object obj, Object value)|\\t将指定对象 obj 中成员变量的值设置为 value\\ngetlnt(0bject obj)|\\t获得指定对象 obj 中成员类型为 int 的成员变量的值\\nsetlnt(0bject obj, int i)|\\t将指定对象 obj 中成员变量的值设置为 i\\nsetFloat(Object obj, float f)|\\t将指定对象 obj 中成员变量的值设置为 f\\ngetBoolean(Object obj)|\\t获得指定对象 obj 中成员类型为 boolean 的成员变量的值\\nsetBoolean(Object obj, boolean b)|\\t将指定对象 obj 中成员变量的值设置为 b\\ngetFloat(Object obj)|\\t获得指定对象 obj 中成员类型为 float 的成员变量的值\\nsetAccessible(boolean flag)|\\t此方法可以设置是否忽略权限直接访问 private 等私有权限的成员变量\\ngetModifiers()|\\t获得可以解析出该方法所采用修饰符的整数\\n>示例代码：\\n```java\\n//得到本类所有属性并遍历输出\\nMethod[] methods = cls.getDeclaredMethods();\\nfor (Method method : methods) {\\n\\tSystem.out.println(method);\\n}\\n//得到指定名称的属性\\nField age = cls.getDeclaredField(\\\"age\\\");\\n //此方法属性的访问修饰符，不推荐使用，会破坏类的封装性，\\nage.setAccessible(true);\\n//为obj的age属性赋值，常规写法，为：((Student)obj).age=18;age为私有的，无法调用\\nage.set(obj,18);\\n```\\n### 那么反射到底有什么用？\\n反射最主要还是运用在框架的设计中，了解了反射才更好的了解一些框架功能实现的原理。\\n### 我们所学的springmvc中的某些功能使用到的反射：\\n\\n 1. 页面向controller中传值，的参数自动映射\\n \\t 他的下一个博客\\n\\t [ 使用反射自定义一个简易的参数自动映射](https://blog.csdn.net/qq_44621891/article/details/108910305).\\n 2.  DispatchServlet(前端控制器)\\n    他的下一个博客\\n\\t[使用反射自定义一个简易DispatchServlet](https://blog.csdn.net/qq_44621891/article/details/108913692)\\n\\t\\n## Java 反射机制的优缺点\\n**优点**： \\n\\n - 能够运行时动态获取类的实例，大大提高系统的灵活性和扩展性。\\n - 与 Java 动态编译相结合，可以实现无比强大的功能。\\n- 对于 Java 这种先编译再运行的语言，能够让我们很方便的创建灵活的代码，这些代码可以在运行时装配，无需在组件之间进行源代码的链接，更加容易实现面向对象。\\n\\n**缺点**：\\n- 一个主要的缺点是对性能有影响，因此，如果不需要动态地创建一个对象，那么就不需要用反射；\\n- 反射调用方法时可以忽略权限检查，获取这个类的私有方法和属性，因此可能会破坏类的封装性而导致安全问题。\\n## 总结：\\n使用java的反射机制，一般需要遵循三步：\\n\\n- 获得你想操作类的Class对象，使用全限定类名，“包名+类名””\\n- 使用Class对象，得到构造，方法，属性，\\n-\\t再使用具体的构造，方法，属性，来操作对应的方法，\",\"articleCover\":\"\",\"articleTitle\":\"2021-09-20\",\"isTop\":0,\"originalUrl\":\"\",\"status\":3,\"tagNameList\":[],\"type\":1}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 00:05:56', NULL);
INSERT INTO `tb_operation_log` VALUES (707, '文章模块', '新增或修改', '/admin/articles', 'com.minzheng.blog.controller.ArticleController.saveOrUpdateArticle', '添加或修改文章', '[{\"articleContent\":\"@[TOC](java反射机制)\\n## 概念\\n>Java 反射机制是 Java 语言的一个重要特性。在学习 Java 反射机制前，大家应该先了解两个概念，编译期和运行期。\\n\\n>**编译期**:是指把源码交给编译器编译成计算机可以执行的文件的过程。在 Java 中也就是把 Java 代码编成 class 文件的过程。编译期只是做了一些翻译功能，并没有把代码放在内存中运行起来，而只是把代码当成文本进行操作，比如检查错误。\\n\\n>**运行期**:是把编译后的文件交给计算机执行，直到程序运行结束。所谓运行期就把在磁盘中的代码放到内存中执行起来。\\n\\n>**百度百科:**\\nJava的反射（*reflection*）机制是指在程序的运行状态中，可以构造任意一个类的对象，可以了解任意一个对象所属的类，可以了解任意一个类的成员变量和方法，可以调用任意一个对象的属性和方法。这种动态获取程序信息以及动态调用对象的功能称为Java语言的反射机制。反射被视为==动态语言==的关键。\\n\\n>**个人理解：**\\nJava反射机制可以视为一个输入流，将一个自定义的类，视为一个普通文件，我们可以得到该文件的所有信息，但又比输入流更加的高级，我们又可以执行该类中的功能点，简单来说，反射机制指的是程序在运行时能够获取自身的信息。在 Java 中，只要给定**类的全限定名**，就可以通过反射机制来获得类的所有信息\\n\\n**什么是静态语言什么是动态语言？**\\n>静态类型语言：变量定义时有类型声明的语言。\\n> 1. 变量的类型在编译的时候确定\\n> 2. 变量的类型在运行时不能修改\\n\\n>动态类型语言：变量定义时无类型声明的语言。\\n> 1. 变量的类型在运行的时候确定\\n>  2. 变量的类型在运行可以修改\\n## 如何实现java的语言动态化？\\n\\n 1. 通过java的反射机制\\n 2. 多态，Java又可以通过接口等方式，在运行时注入相关的类的实现，所以这个又是其动态性的提现\\n \\n ## 如何使用java的反射机制\\n **熟悉获取 Java 反射使用到类**\\n \\n 3. Class类\\n 4. Constructor 类\\n 5. Method 类\\n 6. Field 类\\nConstructor，Method，Field 都是通过Class类得到的，在java.lang.reflect 包下\\n\\n ### 掌握Class类的常用方法\\n >  如何得到Class类对象？\\n \\t1.众所周知，所有 Java 类均继承了 Object 类，在 Object 类中定义了一个 getClass() 方法，该方法返回同一个类型为 Class 的对象。\\n \\t2.或者使用 Class类的静态方法forName(“全限定类名”)\\n \\t\\n|类型  | 访问方法  | 返回值类型 | 说明\\n|--|--|--|--|\\n| 包路径 | getPackage() | Package 对象 | 获取该类的存放路径\\n| 类名称| getName() | String 对象 | 获取该类的名称\\n| 继承类| getSuperclass() | Class 对象 | 获取该类继承的类\\n|实现接口\\t|getlnterfaces()|\\tClass 型数组|\\t获取该类实现的所有接口\\n|构造方法|\\tgetConstructor()\\t|Constructor 对象实例|\\t获取权限为 public 的构造方法，包括，所继承的父类的构造\\n|| getConstructors()\\t|Constructor 型数组|\\t获取所有权限为 public 的构造方法\\n|| getDeclaredContruectors()|Constructor 对象|获取当前对象的所有构造方法\\n|方法|getMethods()|Method 型数组|获取权限为 public 的方法，包括父类\\n||getDeclaredMethods()|Method 对象数组|获取当前对象的所有方法\\n||getDeclaredMethod(String name, Class<?>... parameterTypes)|Method对象|获取本类指定的方法\\n成员变量|getFields()|Field 型数组|获取所有权限为 public 的成员变量,包含父类\\n||getDeclareFileds()|Field 型数组|获取当前对象的所有成员变量\\n>Student类\\n```java\\npackage cn.entity;\\n\\npublic class Student {\\n    private String name;\\n    private Integer age;\\n    private Double score;\\n    public Student(){\\n        System.out.println(\\\"执行了Student的无参构造方法---------------->\\\");\\n    }\\n\\n    public Student(String name) {\\n        System.out.println(\\\"执行了Student的有参构造方法---------------->\\\");\\n        this.name=name;\\n    }\\n\\n    public String getName() {\\n        return name;\\n    }\\n\\n    public void setName(String name) {\\n        this.name = name;\\n    }\\n\\n    public Integer getAge() {\\n        return age;\\n    }\\n\\n    public void setAge(Integer age) {\\n        this.age = age;\\n        System.out.println(\\\"setAge-------------------->\\\");\\n    }\\n\\n    public Double getScore() {\\n        return score;\\n    }\\n\\n    public void setScore(Double score) {\\n        this.score = score;\\n    }\\n\\n    @Override\\n    public String toString() {\\n        return \\\"Student{\\\" +\\n                \\\"name=\'\\\" + name + \'\\\\\'\' +\\n                \\\", age=\\\" + age +\\n                \\\", score=\\\" + score +\\n                \'}\';\\n    }\\n}\\n```\\n\\n> 示例代码：\\n```java\\nClass<?> cls = Class.forName(\\\"cn.entity.Student\\\");\\n//得到Student类的构造器，此时并未创建实例\\nConstructor  constructor= cls.getDeclaredConstructor();\\n//得到Student类的所有方法，不包括父类\\nMethod[] methods = cls.getDeclaredMethods()\\n//得到Student类的指定方法名的方法\\nMethod getName = cls.getMethod(\\\"getName\\\");\\n//得到Student类的指定属性名的属性\\nField age = cls.getDeclaredField(\\\"age\\\");\\n```\\n### 掌握 Constructor 类的常用方法\\n|方法名称\\t| 说明 |\\n|--|--|\\n|isVarArgs()|\\t查看该构造方法是否允许带可变数量的参数，如果允许，返回 true，否则返回false|\\n|getParameterTypes() |\\t按照声明顺序以 Class 数组的形式获取该构造方法各个参数的类型|\\n|getExceptionTypes() |\\t以 Class 数组的形式获取该构造方法可能抛出的异常类型|\\n|newInstance(Object … initargs) |\\t通过该构造方法利用指定参数创建一个该类型的对象，如果未设置参数则表示采用默认无参的构造方法|\\n|setAccessiable(boolean flag) |\\t如果该构造方法的权限为 private，默认为不允许通过反射利用 \\nnewlnstance()|方法创建对象。如果先执行该方法，并将入口参数设置为 true，则允许创建对象\\n|getModifiers()\\t| 获得可以解析出该构造方法所采用修饰符的整数\\n\\n>示例代码：\\n\\n```java\\n //此方法忽略构造方法的访问修饰符，不推荐使用，会破坏类的封装性，\\nconstructor.setAccessible(true);\\n//此步是真正的创建一个实例，执行了类的无惨构造，当无惨构造为private时无法使用，需执行setAccessible(true);才能创建实例\\nObject obj = constructor.newInstance();\\n```\\n### Method类的常用方法\\n|静态方法名称\\t|说明|\\n|--|--|\\ngetName()\\t|获取该方法的名称\\ngetParameterTypes()|\\t按照声明顺序以 Class 数组的形式返回该方法各个参数的类型\\ngetReturnType()|\\t以 Class 对象的形式获得该方法的返回值类型\\ngetExceptionTypes()\\t|以 Class 数组的形式获得该方法可能抛出的异常类型\\ninvoke(Object obj,Object...args)|\\t利用 args 参数执行指定对象 obj 中的该方法，返回值为 Object 类型\\nisVarArgs()|\\t查看该方法是否允许带有可变数量的参数，如果允许返回 true，否则返回 false\\ngetModifiers()|\\t获得可以解析出该方法所采用修饰符的整数\\n\\n>示例代码：\\n```java\\nMethod[] methods = cls.getDeclaredMethods();\\n//遍历所有本类方法\\nfor (Method method : methods) {\\n\\tSystem.out.println(method);\\n}\\n//使用方法名，和指定参数类型，得到需要执行的方法，此时并未指定，只是得到该方法。参数类型，可根据对应的属性类型获取\\nMethod setName = cls.getDeclaredMethod(\\\"setName\\\", String.class);\\n/*此时才是真正的执行方法，\\\"obj\\\"为使用那一个对象，\\n来调用这个方法，“张三”为参数的值，\\n使用非反射的方式调用就是这种:((Student)obj).setName(\\\"张三\\\")*/\\nsetName.invoke(obj,\\\"张三\\\");\\n```\\n### Field类的常用方法\\n|方法名称\\t|说明|\\n|--|--|\\ngetName()\\t|获得该成员变量的名称\\ngetType()|\\t获取表示该成员变量的 Class 对象\\nget(Object obj)|\\t获得指定对象 obj 中成员变量的值，返回值为 Object 类型\\nset(Object obj, Object value)|\\t将指定对象 obj 中成员变量的值设置为 value\\ngetlnt(0bject obj)|\\t获得指定对象 obj 中成员类型为 int 的成员变量的值\\nsetlnt(0bject obj, int i)|\\t将指定对象 obj 中成员变量的值设置为 i\\nsetFloat(Object obj, float f)|\\t将指定对象 obj 中成员变量的值设置为 f\\ngetBoolean(Object obj)|\\t获得指定对象 obj 中成员类型为 boolean 的成员变量的值\\nsetBoolean(Object obj, boolean b)|\\t将指定对象 obj 中成员变量的值设置为 b\\ngetFloat(Object obj)|\\t获得指定对象 obj 中成员类型为 float 的成员变量的值\\nsetAccessible(boolean flag)|\\t此方法可以设置是否忽略权限直接访问 private 等私有权限的成员变量\\ngetModifiers()|\\t获得可以解析出该方法所采用修饰符的整数\\n>示例代码：\\n```java\\n//得到本类所有属性并遍历输出\\nMethod[] methods = cls.getDeclaredMethods();\\nfor (Method method : methods) {\\n\\tSystem.out.println(method);\\n}\\n//得到指定名称的属性\\nField age = cls.getDeclaredField(\\\"age\\\");\\n //此方法属性的访问修饰符，不推荐使用，会破坏类的封装性，\\nage.setAccessible(true);\\n//为obj的age属性赋值，常规写法，为：((Student)obj).age=18;age为私有的，无法调用\\nage.set(obj,18);\\n```\\n### 那么反射到底有什么用？\\n反射最主要还是运用在框架的设计中，了解了反射才更好的了解一些框架功能实现的原理。\\n### 我们所学的springmvc中的某些功能使用到的反射：\\n\\n 1. 页面向controller中传值，的参数自动映射\\n \\t 他的下一个博客\\n\\t [ 使用反射自定义一个简易的参数自动映射](https://blog.csdn.net/qq_44621891/article/details/108910305).\\n 2.  DispatchServlet(前端控制器)\\n    他的下一个博客\\n\\t[使用反射自定义一个简易DispatchServlet](https://blog.csdn.net/qq_44621891/article/details/108913692)\\n\\t\\n## Java 反射机制的优缺点\\n**优点**： \\n\\n - 能够运行时动态获取类的实例，大大提高系统的灵活性和扩展性。\\n - 与 Java 动态编译相结合，可以实现无比强大的功能。\\n- 对于 Java 这种先编译再运行的语言，能够让我们很方便的创建灵活的代码，这些代码可以在运行时装配，无需在组件之间进行源代码的链接，更加容易实现面向对象。\\n\\n**缺点**：\\n- 一个主要的缺点是对性能有影响，因此，如果不需要动态地创建一个对象，那么就不需要用反射；\\n- 反射调用方法时可以忽略权限检查，获取这个类的私有方法和属性，因此可能会破坏类的封装性而导致安全问题。\\n## 总结：\\n使用java的反射机制，一般需要遵循三步：\\n\\n- 获得你想操作类的Class对象，使用全限定类名，“包名+类名””\\n- 使用Class对象，得到构造，方法，属性，\\n-\\t再使用具体的构造，方法，属性，来操作对应的方法，\",\"articleCover\":\"http://8.131.54.14:83/articles/a822004a23789295bf1cc5255046b656.jpg\",\"articleTitle\":\"2021-09-20\",\"categoryName\":\"java专栏\",\"id\":48,\"isTop\":0,\"originalUrl\":\"https://blog.csdn.net/qq_44621891/article/details/108902719\",\"status\":1,\"tagNameList\":[\"随便写写\"],\"type\":2}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 00:10:14', NULL);
INSERT INTO `tb_operation_log` VALUES (708, '文章模块', '新增或修改', '/admin/articles', 'com.minzheng.blog.controller.ArticleController.saveOrUpdateArticle', '添加或修改文章', '[{\"articleContent\":\"@[TOC](java反射机制)\\n## 概念\\n>Java 反射机制是 Java 语言的一个重要特性。在学习 Java 反射机制前，大家应该先了解两个概念，编译期和运行期。\\n\\n>**编译期**:是指把源码交给编译器编译成计算机可以执行的文件的过程。在 Java 中也就是把 Java 代码编成 class 文件的过程。编译期只是做了一些翻译功能，并没有把代码放在内存中运行起来，而只是把代码当成文本进行操作，比如检查错误。\\n\\n>**运行期**:是把编译后的文件交给计算机执行，直到程序运行结束。所谓运行期就把在磁盘中的代码放到内存中执行起来。\\n\\n>**百度百科:**\\nJava的反射（*reflection*）机制是指在程序的运行状态中，可以构造任意一个类的对象，可以了解任意一个对象所属的类，可以了解任意一个类的成员变量和方法，可以调用任意一个对象的属性和方法。这种动态获取程序信息以及动态调用对象的功能称为Java语言的反射机制。反射被视为==动态语言==的关键。\\n\\n>**个人理解：**\\nJava反射机制可以视为一个输入流，将一个自定义的类，视为一个普通文件，我们可以得到该文件的所有信息，但又比输入流更加的高级，我们又可以执行该类中的功能点，简单来说，反射机制指的是程序在运行时能够获取自身的信息。在 Java 中，只要给定**类的全限定名**，就可以通过反射机制来获得类的所有信息\\n\\n**什么是静态语言什么是动态语言？**\\n>静态类型语言：变量定义时有类型声明的语言。\\n> 1. 变量的类型在编译的时候确定\\n> 2. 变量的类型在运行时不能修改\\n\\n>动态类型语言：变量定义时无类型声明的语言。\\n> 1. 变量的类型在运行的时候确定\\n>  2. 变量的类型在运行可以修改\\n## 如何实现java的语言动态化？\\n\\n 1. 通过java的反射机制\\n 2. 多态，Java又可以通过接口等方式，在运行时注入相关的类的实现，所以这个又是其动态性的提现\\n \\n ## 如何使用java的反射机制\\n **熟悉获取 Java 反射使用到类**\\n \\n 3. Class类\\n 4. Constructor 类\\n 5. Method 类\\n 6. Field 类\\nConstructor，Method，Field 都是通过Class类得到的，在java.lang.reflect 包下\\n\\n ### 掌握Class类的常用方法\\n >  如何得到Class类对象？\\n \\t1.众所周知，所有 Java 类均继承了 Object 类，在 Object 类中定义了一个 getClass() 方法，该方法返回同一个类型为 Class 的对象。\\n \\t2.或者使用 Class类的静态方法forName(“全限定类名”)\\n \\t\\n|类型  | 访问方法  | 返回值类型 | 说明\\n|--|--|--|--|\\n| 包路径 | getPackage() | Package 对象 | 获取该类的存放路径\\n| 类名称| getName() | String 对象 | 获取该类的名称\\n| 继承类| getSuperclass() | Class 对象 | 获取该类继承的类\\n|实现接口\\t|getlnterfaces()|\\tClass 型数组|\\t获取该类实现的所有接口\\n|构造方法|\\tgetConstructor()\\t|Constructor 对象实例|\\t获取权限为 public 的构造方法，包括，所继承的父类的构造\\n|| getConstructors()\\t|Constructor 型数组|\\t获取所有权限为 public 的构造方法\\n|| getDeclaredContruectors()|Constructor 对象|获取当前对象的所有构造方法\\n|方法|getMethods()|Method 型数组|获取权限为 public 的方法，包括父类\\n||getDeclaredMethods()|Method 对象数组|获取当前对象的所有方法\\n||getDeclaredMethod(String name, Class<?>... parameterTypes)|Method对象|获取本类指定的方法\\n成员变量|getFields()|Field 型数组|获取所有权限为 public 的成员变量,包含父类\\n||getDeclareFileds()|Field 型数组|获取当前对象的所有成员变量\\n>Student类\\n```java\\npackage cn.entity;\\n\\npublic class Student {\\n    private String name;\\n    private Integer age;\\n    private Double score;\\n    public Student(){\\n        System.out.println(\\\"执行了Student的无参构造方法---------------->\\\");\\n    }\\n\\n    public Student(String name) {\\n        System.out.println(\\\"执行了Student的有参构造方法---------------->\\\");\\n        this.name=name;\\n    }\\n\\n    public String getName() {\\n        return name;\\n    }\\n\\n    public void setName(String name) {\\n        this.name = name;\\n    }\\n\\n    public Integer getAge() {\\n        return age;\\n    }\\n\\n    public void setAge(Integer age) {\\n        this.age = age;\\n        System.out.println(\\\"setAge-------------------->\\\");\\n    }\\n\\n    public Double getScore() {\\n        return score;\\n    }\\n\\n    public void setScore(Double score) {\\n        this.score = score;\\n    }\\n\\n    @Override\\n    public String toString() {\\n        return \\\"Student{\\\" +\\n                \\\"name=\'\\\" + name + \'\\\\\'\' +\\n                \\\", age=\\\" + age +\\n                \\\", score=\\\" + score +\\n                \'}\';\\n    }\\n}\\n```\\n\\n> 示例代码：\\n```java\\nClass<?> cls = Class.forName(\\\"cn.entity.Student\\\");\\n//得到Student类的构造器，此时并未创建实例\\nConstructor  constructor= cls.getDeclaredConstructor();\\n//得到Student类的所有方法，不包括父类\\nMethod[] methods = cls.getDeclaredMethods()\\n//得到Student类的指定方法名的方法\\nMethod getName = cls.getMethod(\\\"getName\\\");\\n//得到Student类的指定属性名的属性\\nField age = cls.getDeclaredField(\\\"age\\\");\\n```\\n### 掌握 Constructor 类的常用方法\\n|方法名称\\t| 说明 |\\n|--|--|\\n|isVarArgs()|\\t查看该构造方法是否允许带可变数量的参数，如果允许，返回 true，否则返回false|\\n|getParameterTypes() |\\t按照声明顺序以 Class 数组的形式获取该构造方法各个参数的类型|\\n|getExceptionTypes() |\\t以 Class 数组的形式获取该构造方法可能抛出的异常类型|\\n|newInstance(Object … initargs) |\\t通过该构造方法利用指定参数创建一个该类型的对象，如果未设置参数则表示采用默认无参的构造方法|\\n|setAccessiable(boolean flag) |\\t如果该构造方法的权限为 private，默认为不允许通过反射利用 \\nnewlnstance()|方法创建对象。如果先执行该方法，并将入口参数设置为 true，则允许创建对象\\n|getModifiers()\\t| 获得可以解析出该构造方法所采用修饰符的整数\\n\\n>示例代码：\\n\\n```java\\n //此方法忽略构造方法的访问修饰符，不推荐使用，会破坏类的封装性，\\nconstructor.setAccessible(true);\\n//此步是真正的创建一个实例，执行了类的无惨构造，当无惨构造为private时无法使用，需执行setAccessible(true);才能创建实例\\nObject obj = constructor.newInstance();\\n```\\n### Method类的常用方法\\n|静态方法名称\\t|说明|\\n|--|--|\\ngetName()\\t|获取该方法的名称\\ngetParameterTypes()|\\t按照声明顺序以 Class 数组的形式返回该方法各个参数的类型\\ngetReturnType()|\\t以 Class 对象的形式获得该方法的返回值类型\\ngetExceptionTypes()\\t|以 Class 数组的形式获得该方法可能抛出的异常类型\\ninvoke(Object obj,Object...args)|\\t利用 args 参数执行指定对象 obj 中的该方法，返回值为 Object 类型\\nisVarArgs()|\\t查看该方法是否允许带有可变数量的参数，如果允许返回 true，否则返回 false\\ngetModifiers()|\\t获得可以解析出该方法所采用修饰符的整数\\n\\n>示例代码：\\n```java\\nMethod[] methods = cls.getDeclaredMethods();\\n//遍历所有本类方法\\nfor (Method method : methods) {\\n\\tSystem.out.println(method);\\n}\\n//使用方法名，和指定参数类型，得到需要执行的方法，此时并未指定，只是得到该方法。参数类型，可根据对应的属性类型获取\\nMethod setName = cls.getDeclaredMethod(\\\"setName\\\", String.class);\\n/*此时才是真正的执行方法，\\\"obj\\\"为使用那一个对象，\\n来调用这个方法，“张三”为参数的值，\\n使用非反射的方式调用就是这种:((Student)obj).setName(\\\"张三\\\")*/\\nsetName.invoke(obj,\\\"张三\\\");\\n```\\n### Field类的常用方法\\n|方法名称\\t|说明|\\n|--|--|\\ngetName()\\t|获得该成员变量的名称\\ngetType()|\\t获取表示该成员变量的 Class 对象\\nget(Object obj)|\\t获得指定对象 obj 中成员变量的值，返回值为 Object 类型\\nset(Object obj, Object value)|\\t将指定对象 obj 中成员变量的值设置为 value\\ngetlnt(0bject obj)|\\t获得指定对象 obj 中成员类型为 int 的成员变量的值\\nsetlnt(0bject obj, int i)|\\t将指定对象 obj 中成员变量的值设置为 i\\nsetFloat(Object obj, float f)|\\t将指定对象 obj 中成员变量的值设置为 f\\ngetBoolean(Object obj)|\\t获得指定对象 obj 中成员类型为 boolean 的成员变量的值\\nsetBoolean(Object obj, boolean b)|\\t将指定对象 obj 中成员变量的值设置为 b\\ngetFloat(Object obj)|\\t获得指定对象 obj 中成员类型为 float 的成员变量的值\\nsetAccessible(boolean flag)|\\t此方法可以设置是否忽略权限直接访问 private 等私有权限的成员变量\\ngetModifiers()|\\t获得可以解析出该方法所采用修饰符的整数\\n>示例代码：\\n```java\\n//得到本类所有属性并遍历输出\\nMethod[] methods = cls.getDeclaredMethods();\\nfor (Method method : methods) {\\n\\tSystem.out.println(method);\\n}\\n//得到指定名称的属性\\nField age = cls.getDeclaredField(\\\"age\\\");\\n //此方法属性的访问修饰符，不推荐使用，会破坏类的封装性，\\nage.setAccessible(true);\\n//为obj的age属性赋值，常规写法，为：((Student)obj).age=18;age为私有的，无法调用\\nage.set(obj,18);\\n```\\n### 那么反射到底有什么用？\\n反射最主要还是运用在框架的设计中，了解了反射才更好的了解一些框架功能实现的原理。\\n### 我们所学的springmvc中的某些功能使用到的反射：\\n\\n 1. 页面向controller中传值，的参数自动映射\\n \\t 他的下一个博客\\n\\t [ 使用反射自定义一个简易的参数自动映射](https://blog.csdn.net/qq_44621891/article/details/108910305).\\n 2.  DispatchServlet(前端控制器)\\n    他的下一个博客\\n\\t[使用反射自定义一个简易DispatchServlet](https://blog.csdn.net/qq_44621891/article/details/108913692)\\n\\t\\n## Java 反射机制的优缺点\\n**优点**： \\n\\n - 能够运行时动态获取类的实例，大大提高系统的灵活性和扩展性。\\n - 与 Java 动态编译相结合，可以实现无比强大的功能。\\n- 对于 Java 这种先编译再运行的语言，能够让我们很方便的创建灵活的代码，这些代码可以在运行时装配，无需在组件之间进行源代码的链接，更加容易实现面向对象。\\n\\n**缺点**：\\n- 一个主要的缺点是对性能有影响，因此，如果不需要动态地创建一个对象，那么就不需要用反射；\\n- 反射调用方法时可以忽略权限检查，获取这个类的私有方法和属性，因此可能会破坏类的封装性而导致安全问题。\\n## 总结：\\n使用java的反射机制，一般需要遵循三步：\\n\\n- 获得你想操作类的Class对象，使用全限定类名，“包名+类名””\\n- 使用Class对象，得到构造，方法，属性，\\n-\\t再使用具体的构造，方法，属性，来操作对应的方法，\",\"articleCover\":\"http://8.131.54.14:83/articles/a822004a23789295bf1cc5255046b656.jpg\",\"articleTitle\":\"java反射机制\",\"categoryName\":\"java专栏\",\"id\":48,\"isTop\":0,\"originalUrl\":\"https://blog.csdn.net/qq_44621891/article/details/108902719\",\"status\":1,\"tagNameList\":[],\"type\":2}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 00:11:23', NULL);
INSERT INTO `tb_operation_log` VALUES (709, '文章模块', '新增或修改', '/admin/articles', 'com.minzheng.blog.controller.ArticleController.saveOrUpdateArticle', '添加或修改文章', '[{\"articleContent\":\"# java反射机制\\n## 概念\\n>Java 反射机制是 Java 语言的一个重要特性。在学习 Java 反射机制前，大家应该先了解两个概念，编译期和运行期。\\n\\n>**编译期**:是指把源码交给编译器编译成计算机可以执行的文件的过程。在 Java 中也就是把 Java 代码编成 class 文件的过程。编译期只是做了一些翻译功能，并没有把代码放在内存中运行起来，而只是把代码当成文本进行操作，比如检查错误。\\n\\n>**运行期**:是把编译后的文件交给计算机执行，直到程序运行结束。所谓运行期就把在磁盘中的代码放到内存中执行起来。\\n\\n>**百度百科:**\\nJava的反射（*reflection*）机制是指在程序的运行状态中，可以构造任意一个类的对象，可以了解任意一个对象所属的类，可以了解任意一个类的成员变量和方法，可以调用任意一个对象的属性和方法。这种动态获取程序信息以及动态调用对象的功能称为Java语言的反射机制。反射被视为==动态语言==的关键。\\n\\n>**个人理解：**\\nJava反射机制可以视为一个输入流，将一个自定义的类，视为一个普通文件，我们可以得到该文件的所有信息，但又比输入流更加的高级，我们又可以执行该类中的功能点，简单来说，反射机制指的是程序在运行时能够获取自身的信息。在 Java 中，只要给定**类的全限定名**，就可以通过反射机制来获得类的所有信息\\n\\n**什么是静态语言什么是动态语言？**\\n>静态类型语言：变量定义时有类型声明的语言。\\n> 1. 变量的类型在编译的时候确定\\n> 2. 变量的类型在运行时不能修改\\n\\n>动态类型语言：变量定义时无类型声明的语言。\\n> 1. 变量的类型在运行的时候确定\\n>  2. 变量的类型在运行可以修改\\n## 如何实现java的语言动态化？\\n\\n 1. 通过java的反射机制\\n 2. 多态，Java又可以通过接口等方式，在运行时注入相关的类的实现，所以这个又是其动态性的提现\\n \\n ## 如何使用java的反射机制\\n **熟悉获取 Java 反射使用到类**\\n \\n 3. Class类\\n 4. Constructor 类\\n 5. Method 类\\n 6. Field 类\\nConstructor，Method，Field 都是通过Class类得到的，在java.lang.reflect 包下\\n\\n ### 掌握Class类的常用方法\\n >  如何得到Class类对象？\\n \\t1.众所周知，所有 Java 类均继承了 Object 类，在 Object 类中定义了一个 getClass() 方法，该方法返回同一个类型为 Class 的对象。\\n \\t2.或者使用 Class类的静态方法forName(“全限定类名”)\\n \\t\\n|类型  | 访问方法  | 返回值类型 | 说明\\n|--|--|--|--|\\n| 包路径 | getPackage() | Package 对象 | 获取该类的存放路径\\n| 类名称| getName() | String 对象 | 获取该类的名称\\n| 继承类| getSuperclass() | Class 对象 | 获取该类继承的类\\n|实现接口\\t|getlnterfaces()|\\tClass 型数组|\\t获取该类实现的所有接口\\n|构造方法|\\tgetConstructor()\\t|Constructor 对象实例|\\t获取权限为 public 的构造方法，包括，所继承的父类的构造\\n|| getConstructors()\\t|Constructor 型数组|\\t获取所有权限为 public 的构造方法\\n|| getDeclaredContruectors()|Constructor 对象|获取当前对象的所有构造方法\\n|方法|getMethods()|Method 型数组|获取权限为 public 的方法，包括父类\\n||getDeclaredMethods()|Method 对象数组|获取当前对象的所有方法\\n||getDeclaredMethod(String name, Class<?>... parameterTypes)|Method对象|获取本类指定的方法\\n成员变量|getFields()|Field 型数组|获取所有权限为 public 的成员变量,包含父类\\n||getDeclareFileds()|Field 型数组|获取当前对象的所有成员变量\\n>Student类\\n```java\\npackage cn.entity;\\n\\npublic class Student {\\n    private String name;\\n    private Integer age;\\n    private Double score;\\n    public Student(){\\n        System.out.println(\\\"执行了Student的无参构造方法---------------->\\\");\\n    }\\n\\n    public Student(String name) {\\n        System.out.println(\\\"执行了Student的有参构造方法---------------->\\\");\\n        this.name=name;\\n    }\\n\\n    public String getName() {\\n        return name;\\n    }\\n\\n    public void setName(String name) {\\n        this.name = name;\\n    }\\n\\n    public Integer getAge() {\\n        return age;\\n    }\\n\\n    public void setAge(Integer age) {\\n        this.age = age;\\n        System.out.println(\\\"setAge-------------------->\\\");\\n    }\\n\\n    public Double getScore() {\\n        return score;\\n    }\\n\\n    public void setScore(Double score) {\\n        this.score = score;\\n    }\\n\\n    @Override\\n    public String toString() {\\n        return \\\"Student{\\\" +\\n                \\\"name=\'\\\" + name + \'\\\\\'\' +\\n                \\\", age=\\\" + age +\\n                \\\", score=\\\" + score +\\n                \'}\';\\n    }\\n}\\n```\\n\\n> 示例代码：\\n```java\\nClass<?> cls = Class.forName(\\\"cn.entity.Student\\\");\\n//得到Student类的构造器，此时并未创建实例\\nConstructor  constructor= cls.getDeclaredConstructor();\\n//得到Student类的所有方法，不包括父类\\nMethod[] methods = cls.getDeclaredMethods()\\n//得到Student类的指定方法名的方法\\nMethod getName = cls.getMethod(\\\"getName\\\");\\n//得到Student类的指定属性名的属性\\nField age = cls.getDeclaredField(\\\"age\\\");\\n```\\n### 掌握 Constructor 类的常用方法\\n|方法名称\\t| 说明 |\\n|--|--|\\n|isVarArgs()|\\t查看该构造方法是否允许带可变数量的参数，如果允许，返回 true，否则返回false|\\n|getParameterTypes() |\\t按照声明顺序以 Class 数组的形式获取该构造方法各个参数的类型|\\n|getExceptionTypes() |\\t以 Class 数组的形式获取该构造方法可能抛出的异常类型|\\n|newInstance(Object … initargs) |\\t通过该构造方法利用指定参数创建一个该类型的对象，如果未设置参数则表示采用默认无参的构造方法|\\n|setAccessiable(boolean flag) |\\t如果该构造方法的权限为 private，默认为不允许通过反射利用 \\nnewlnstance()|方法创建对象。如果先执行该方法，并将入口参数设置为 true，则允许创建对象\\n|getModifiers()\\t| 获得可以解析出该构造方法所采用修饰符的整数\\n\\n>示例代码：\\n\\n```java\\n //此方法忽略构造方法的访问修饰符，不推荐使用，会破坏类的封装性，\\nconstructor.setAccessible(true);\\n//此步是真正的创建一个实例，执行了类的无惨构造，当无惨构造为private时无法使用，需执行setAccessible(true);才能创建实例\\nObject obj = constructor.newInstance();\\n```\\n### Method类的常用方法\\n|静态方法名称\\t|说明|\\n|--|--|\\ngetName()\\t|获取该方法的名称\\ngetParameterTypes()|\\t按照声明顺序以 Class 数组的形式返回该方法各个参数的类型\\ngetReturnType()|\\t以 Class 对象的形式获得该方法的返回值类型\\ngetExceptionTypes()\\t|以 Class 数组的形式获得该方法可能抛出的异常类型\\ninvoke(Object obj,Object...args)|\\t利用 args 参数执行指定对象 obj 中的该方法，返回值为 Object 类型\\nisVarArgs()|\\t查看该方法是否允许带有可变数量的参数，如果允许返回 true，否则返回 false\\ngetModifiers()|\\t获得可以解析出该方法所采用修饰符的整数\\n\\n>示例代码：\\n```java\\nMethod[] methods = cls.getDeclaredMethods();\\n//遍历所有本类方法\\nfor (Method method : methods) {\\n\\tSystem.out.println(method);\\n}\\n//使用方法名，和指定参数类型，得到需要执行的方法，此时并未指定，只是得到该方法。参数类型，可根据对应的属性类型获取\\nMethod setName = cls.getDeclaredMethod(\\\"setName\\\", String.class);\\n/*此时才是真正的执行方法，\\\"obj\\\"为使用那一个对象，\\n来调用这个方法，“张三”为参数的值，\\n使用非反射的方式调用就是这种:((Student)obj).setName(\\\"张三\\\")*/\\nsetName.invoke(obj,\\\"张三\\\");\\n```\\n### Field类的常用方法\\n|方法名称\\t|说明|\\n|--|--|\\ngetName()\\t|获得该成员变量的名称\\ngetType()|\\t获取表示该成员变量的 Class 对象\\nget(Object obj)|\\t获得指定对象 obj 中成员变量的值，返回值为 Object 类型\\nset(Object obj, Object value)|\\t将指定对象 obj 中成员变量的值设置为 value\\ngetlnt(0bject obj)|\\t获得指定对象 obj 中成员类型为 int 的成员变量的值\\nsetlnt(0bject obj, int i)|\\t将指定对象 obj 中成员变量的值设置为 i\\nsetFloat(Object obj, float f)|\\t将指定对象 obj 中成员变量的值设置为 f\\ngetBoolean(Object obj)|\\t获得指定对象 obj 中成员类型为 boolean 的成员变量的值\\nsetBoolean(Object obj, boolean b)|\\t将指定对象 obj 中成员变量的值设置为 b\\ngetFloat(Object obj)|\\t获得指定对象 obj 中成员类型为 float 的成员变量的值\\nsetAccessible(boolean flag)|\\t此方法可以设置是否忽略权限直接访问 private 等私有权限的成员变量\\ngetModifiers()|\\t获得可以解析出该方法所采用修饰符的整数\\n>示例代码：\\n```java\\n//得到本类所有属性并遍历输出\\nMethod[] methods = cls.getDeclaredMethods();\\nfor (Method method : methods) {\\n\\tSystem.out.println(method);\\n}\\n//得到指定名称的属性\\nField age = cls.getDeclaredField(\\\"age\\\");\\n //此方法属性的访问修饰符，不推荐使用，会破坏类的封装性，\\nage.setAccessible(true);\\n//为obj的age属性赋值，常规写法，为：((Student)obj).age=18;age为私有的，无法调用\\nage.set(obj,18);\\n```\\n### 那么反射到底有什么用？\\n反射最主要还是运用在框架的设计中，了解了反射才更好的了解一些框架功能实现的原理。\\n### 我们所学的springmvc中的某些功能使用到的反射：\\n\\n 1. 页面向controller中传值，的参数自动映射\\n \\t 他的下一个博客\\n\\t [ 使用反射自定义一个简易的参数自动映射](https://blog.csdn.net/qq_44621891/article/details/108910305).\\n 2.  DispatchServlet(前端控制器)\\n    他的下一个博客\\n\\t[使用反射自定义一个简易DispatchServlet](https://blog.csdn.net/qq_44621891/article/details/108913692)\\n\\t\\n## Java 反射机制的优缺点\\n**优点**： \\n\\n - 能够运行时动态获取类的实例，大大提高系统的灵活性和扩展性。\\n - 与 Java 动态编译相结合，可以实现无比强大的功能。\\n- 对于 Java 这种先编译再运行的语言，能够让我们很方便的创建灵活的代码，这些代码可以在运行时装配，无需在组件之间进行源代码的链接，更加容易实现面向对象。\\n\\n**缺点**：\\n- 一个主要的缺点是对性能有影响，因此，如果不需要动态地创建一个对象，那么就不需要用反射；\\n- 反射调用方法时可以忽略权限检查，获取这个类的私有方法和属性，因此可能会破坏类的封装性而导致安全问题。\\n## 总结：\\n使用java的反射机制，一般需要遵循三步：\\n\\n- 获得你想操作类的Class对象，使用全限定类名，“包名+类名””\\n- 使用Class对象，得到构造，方法，属性，\\n-\\t再使用具体的构造，方法，属性，来操作对应的方法，\",\"articleCover\":\"http://8.131.54.14:83/articles/a822004a23789295bf1cc5255046b656.jpg\",\"articleTitle\":\"java反射机制\",\"categoryName\":\"java专栏\",\"id\":48,\"isTop\":0,\"originalUrl\":\"https://blog.csdn.net/qq_44621891/article/details/108902719\",\"status\":1,\"tagNameList\":[],\"type\":2}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 00:11:59', NULL);
INSERT INTO `tb_operation_log` VALUES (710, '文章模块', '修改', '/admin/articles', 'com.minzheng.blog.controller.ArticleController.updateArticleDelete', '恢复或删除文章', '[{\"idList\":[47],\"isDelete\":1}]', 'PUT', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 00:13:03', NULL);
INSERT INTO `tb_operation_log` VALUES (711, '页面模块', '新增或修改', '/admin/pages', 'com.minzheng.blog.controller.PageController.saveOrUpdatePage', '保存或更新页面', '[{\"id\":1,\"pageCover\":\"http://8.131.54.14:83/config/1747d47ec70c992f20a93d722c6ab779.jpg\",\"pageLabel\":\"home\",\"pageName\":\"首页\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 01:11:53', NULL);
INSERT INTO `tb_operation_log` VALUES (712, '页面模块', '新增或修改', '/admin/pages', 'com.minzheng.blog.controller.PageController.saveOrUpdatePage', '保存或更新页面', '[{\"id\":1,\"pageCover\":\"http://8.131.54.14:83/config/6fda6624e5d4e80edc3c851f633268da.jpg\",\"pageLabel\":\"home\",\"pageName\":\"首页\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 01:12:15', NULL);
INSERT INTO `tb_operation_log` VALUES (713, '页面模块', '新增或修改', '/admin/pages', 'com.minzheng.blog.controller.PageController.saveOrUpdatePage', '保存或更新页面', '[{\"id\":4,\"pageCover\":\"http://8.131.54.14:83/config/7dbba03b180e920870bee3924ca5579b.jpg\",\"pageLabel\":\"tag\",\"pageName\":\"标签\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 01:12:37', NULL);
INSERT INTO `tb_operation_log` VALUES (714, '页面模块', '新增或修改', '/admin/pages', 'com.minzheng.blog.controller.PageController.saveOrUpdatePage', '保存或更新页面', '[{\"id\":3,\"pageCover\":\"http://8.131.54.14:83/config/2a273104a938161f2151dbc46a20cd23.jpg\",\"pageLabel\":\"category\",\"pageName\":\"分类\"}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 01:15:25', NULL);
INSERT INTO `tb_operation_log` VALUES (715, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":4,\"photoUrlList\":[\"http://8.131.54.14:83/articles/e84207b03afc38ab7a26d8985a17ef4a.jpg\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 15:05:08', NULL);
INSERT INTO `tb_operation_log` VALUES (716, '照片模块', '修改', '/admin/photos/delete', 'com.minzheng.blog.controller.PhotoController.updatePhotoDelete', '更新照片删除状态', '[{\"idList\":[82],\"isDelete\":1}]', 'PUT', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 15:05:15', NULL);
INSERT INTO `tb_operation_log` VALUES (717, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":4,\"photoUrlList\":[\"http://8.131.54.14:83/articles/1f92064ced101aee81d47e2ba1dcd765.JPG\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 15:30:41', NULL);
INSERT INTO `tb_operation_log` VALUES (718, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":4,\"photoUrlList\":[\"http://8.131.54.14:83/articles/5fcd05feadea386a795aa3f0a2a11f4a.jpg\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 15:36:27', NULL);
INSERT INTO `tb_operation_log` VALUES (719, '照片模块', '修改', '/admin/photos/delete', 'com.minzheng.blog.controller.PhotoController.updatePhotoDelete', '更新照片删除状态', '[{\"idList\":[84,83,81,80,79,78,77,76,75,74,73,72,71,70,69,68,67,66],\"isDelete\":1}]', 'PUT', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 15:37:14', NULL);
INSERT INTO `tb_operation_log` VALUES (720, '照片模块', '修改', '/admin/photos/delete', 'com.minzheng.blog.controller.PhotoController.updatePhotoDelete', '更新照片删除状态', '[{\"idList\":[65,64,63,62,61,60,59,58,57,56],\"isDelete\":1}]', 'PUT', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 15:37:19', NULL);
INSERT INTO `tb_operation_log` VALUES (721, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":4,\"photoUrlList\":[\"http://8.131.54.14:83/articles/76de2af0867a5200f3701647386498e4.jpg\",\"http://8.131.54.14:83/articles/647540c79018ecc58b68e6e33caab609.jpg\",\"http://8.131.54.14:83/articles/aa997c149affe78e05cfa51c0449a0e2.jpg\",\"http://8.131.54.14:83/articles/e6b6468fe090dc20d49e175fd34cbfc5.jpg\",\"http://8.131.54.14:83/articles/4c572f1903a0b49d8a51a8bb5a77fc86.jpg\",\"http://8.131.54.14:83/articles/3de4c1046260e1541eade0524fcc18c9.jpg\",\"http://8.131.54.14:83/articles/8fdf3c05a02ca0390f42e5ffcdb51b6a.jpg\",\"http://8.131.54.14:83/articles/6b5222ce0b24cac4c266b956c946e8f4.jpg\",\"http://8.131.54.14:83/articles/2da0e00bcb06a3c2f7bade415e65ab38.jpg\",\"http://8.131.54.14:83/articles/d5053a222168b3193a93307def6b8ede.jpg\",\"http://8.131.54.14:83/articles/9c4be3a980aa606a0e2714047b829d90.jpg\",\"http://8.131.54.14:83/articles/5a2458fe691ff5bbb837734368dda863.jpg\",\"http://8.131.54.14:83/articles/4b3511a78a068631a8321806cf68cb2d.jpg\",\"http://8.131.54.14:83/articles/96727fec3160ad7114bd424377a90ee8.jpg\",\"http://8.131.54.14:83/articles/8b8d7f6fa1e6ddd072e19706748907d7.jpg\",\"http://8.131.54.14:83/articles/ef1b12dc91608f450057c6743db026be.jpg\",\"http://8.131.54.14:83/articles/3c26e14b664a95f4389770f309a46f27.jpg\",\"http://8.131.54.14:83/articles/2c27711e0a364e1a3545dfab4a21367a.jpg\",\"http://8.131.54.14:83/articles/cefc6164ca1fac1eb6ebc93e36d3248c.jpg\",\"http://8.131.54.14:83/articles/b7c127c1415e95aae76dfba327d984e3.jpg\",\"http://8.131.54.14:83/articles/f352e403085e4786af74549e13199ff8.jpg\",\"http://8.131.54.14:83/articles/e38c4cc62b6c77b501c52fb37d63774f.jpg\",\"http://8.131.54.14:83/articles/22a960f6bb21b95a9a81858cba45ad47.jpg\",\"http://8.131.54.14:83/articles/fc2da1db07447ff75056d89456ddc69e.jpg\",\"http://8.131.54.14:83/articles/6cc14c49c8fd6d51733f389865cd028b.jpg\",\"http://8.131.54.14:83/articles/d996aa2bf72f37122241ddf0a76987c1.jpg\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 15:39:33', NULL);
INSERT INTO `tb_operation_log` VALUES (722, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":4,\"photoUrlList\":[\"http://8.131.54.14:83/articles/2a273104a938161f2151dbc46a20cd23.jpg\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 17:04:12', NULL);
INSERT INTO `tb_operation_log` VALUES (723, '照片模块', '修改', '/admin/photos/delete', 'com.minzheng.blog.controller.PhotoController.updatePhotoDelete', '更新照片删除状态', '[{\"idList\":[111,110,109,108,107,106,105,104,103,102,101,100,99,98,97,96,95,94],\"isDelete\":1}]', 'PUT', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 18:42:44', NULL);
INSERT INTO `tb_operation_log` VALUES (724, '照片模块', '修改', '/admin/photos/delete', 'com.minzheng.blog.controller.PhotoController.updatePhotoDelete', '更新照片删除状态', '[{\"idList\":[93,92,91,90,89,88,87,86,85],\"isDelete\":1}]', 'PUT', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 18:42:48', NULL);
INSERT INTO `tb_operation_log` VALUES (725, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":4,\"photoUrlList\":[\"http://8.131.54.14:83/articles/76de2af0867a5200f3701647386498e4.jpg\",\"http://8.131.54.14:83/articles/aa997c149affe78e05cfa51c0449a0e2.jpg\",\"http://8.131.54.14:83/articles/e6b6468fe090dc20d49e175fd34cbfc5.jpg\",\"http://8.131.54.14:83/articles/4c572f1903a0b49d8a51a8bb5a77fc86.jpg\",\"http://8.131.54.14:83/articles/3de4c1046260e1541eade0524fcc18c9.jpg\",\"http://8.131.54.14:83/articles/647540c79018ecc58b68e6e33caab609.jpg\",\"http://8.131.54.14:83/articles/2da0e00bcb06a3c2f7bade415e65ab38.jpg\",\"http://8.131.54.14:83/articles/8fdf3c05a02ca0390f42e5ffcdb51b6a.jpg\",\"http://8.131.54.14:83/articles/6b5222ce0b24cac4c266b956c946e8f4.jpg\",\"http://8.131.54.14:83/articles/d5053a222168b3193a93307def6b8ede.jpg\",\"http://8.131.54.14:83/articles/9c4be3a980aa606a0e2714047b829d90.jpg\",\"http://8.131.54.14:83/articles/8b8d7f6fa1e6ddd072e19706748907d7.jpg\",\"http://8.131.54.14:83/articles/96727fec3160ad7114bd424377a90ee8.jpg\",\"http://8.131.54.14:83/articles/ef1b12dc91608f450057c6743db026be.jpg\",\"http://8.131.54.14:83/articles/3c26e14b664a95f4389770f309a46f27.jpg\",\"http://8.131.54.14:83/articles/2c27711e0a364e1a3545dfab4a21367a.jpg\",\"http://8.131.54.14:83/articles/fc2da1db07447ff75056d89456ddc69e.jpg\",\"http://8.131.54.14:83/articles/cefc6164ca1fac1eb6ebc93e36d3248c.jpg\",\"http://8.131.54.14:83/articles/b7c127c1415e95aae76dfba327d984e3.jpg\",\"http://8.131.54.14:83/articles/5a2458fe691ff5bbb837734368dda863.jpg\",\"http://8.131.54.14:83/articles/4b3511a78a068631a8321806cf68cb2d.jpg\",\"http://8.131.54.14:83/articles/e38c4cc62b6c77b501c52fb37d63774f.jpg\",\"http://8.131.54.14:83/articles/22a960f6bb21b95a9a81858cba45ad47.jpg\",\"http://8.131.54.14:83/articles/f352e403085e4786af74549e13199ff8.jpg\",\"http://8.131.54.14:83/articles/d996aa2bf72f37122241ddf0a76987c1.jpg\",\"http://8.131.54.14:83/articles/6cc14c49c8fd6d51733f389865cd028b.jpg\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 18:44:30', NULL);
INSERT INTO `tb_operation_log` VALUES (726, '相册模块', '新增或修改', '/admin/photos/albums', 'com.minzheng.blog.controller.PhotoAlbumController.saveOrUpdatePhotoAlbum', '保存或更新相册', '[{\"albumCover\":\"http://8.131.54.14:83/config/92e78b7c6586c5c5620190964908c6c7.jpg\",\"albumDesc\":\"精美高清配图\",\"albumName\":\"配图\",\"id\":3,\"status\":1}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 18:54:36', NULL);
INSERT INTO `tb_operation_log` VALUES (727, '照片模块', '修改', '/admin/photos/delete', 'com.minzheng.blog.controller.PhotoController.updatePhotoDelete', '更新照片删除状态', '[{\"idList\":[55,54,53,52,51,50,49,48,47,46,45,44,43,42,41,40,39,38],\"isDelete\":1}]', 'PUT', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 18:54:42', NULL);
INSERT INTO `tb_operation_log` VALUES (728, '照片模块', '修改', '/admin/photos/delete', 'com.minzheng.blog.controller.PhotoController.updatePhotoDelete', '更新照片删除状态', '[{\"idList\":[37,36,35,34,33,32,31,30,29,28,27,26,25,24,23,22,21],\"isDelete\":1}]', 'PUT', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 18:54:46', NULL);
INSERT INTO `tb_operation_log` VALUES (729, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":3,\"photoUrlList\":[\"http://8.131.54.14:83/articles/2a273104a938161f2151dbc46a20cd23.jpg\",\"http://8.131.54.14:83/articles/8834cf5a60c617aca30548ad7d4ae917.jpg\",\"http://8.131.54.14:83/articles/fa463458b06cc68da4b55472e44f1bc5.jpg\",\"http://8.131.54.14:83/articles/421c2a74ec9b49ee3ceaf1533b12d571.jpg\",\"http://8.131.54.14:83/articles/444c25b48039c4895b730c2363cff5fc.jpg\",\"http://8.131.54.14:83/articles/b7c127c1415e95aae76dfba327d984e3.jpg\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 18:55:22', NULL);
INSERT INTO `tb_operation_log` VALUES (730, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":3,\"photoUrlList\":[\"http://8.131.54.14:83/articles/1df4d0cc9777da7fe795b8c30c2bc144.jpg\",\"http://8.131.54.14:83/articles/3bd2b7f1021de42b612b06273034c95f.jpg\",\"http://8.131.54.14:83/articles/00bf0b48dd553dc7d4e2bdb2b2421025.jpg\",\"http://8.131.54.14:83/articles/4f7815feeeddfacc4822009852ddda62.jpg\",\"http://8.131.54.14:83/articles/ef79ad68904159854d30eda845ac13c0.jpg\",\"http://8.131.54.14:83/articles/4b2a200ef24195695502704fc0e602a7.jpg\",\"http://8.131.54.14:83/articles/9e1bfcb1f70d8b4aff96115f1021cf7a.jpg\",\"http://8.131.54.14:83/articles/ac2b635b123aca15a2addc02a798ab5b.jpg\",\"http://8.131.54.14:83/articles/bc5a4d47a5ca984974f522f1194b3dfc.jpg\",\"http://8.131.54.14:83/articles/583b82092ecf0bd302a1665fdfae6f73.jpg\",\"http://8.131.54.14:83/articles/51ac932fc5a57514528f9bf130fdea2c.jpg\",\"http://8.131.54.14:83/articles/f591c229c482392c69965f85e867b105.jpg\",\"http://8.131.54.14:83/articles/448ba6fd79c7e6377c0bfef5e1589efb.jpg\",\"http://8.131.54.14:83/articles/e7871d171de53c83126a727f6177f292.jpg\",\"http://8.131.54.14:83/articles/1768dad0c62312062fe32067174223aa.jpg\",\"http://8.131.54.14:83/articles/cad547b4c355dd8f3519c087783caecc.jpg\",\"http://8.131.54.14:83/articles/af5092f98fe95820f48dc8ca672bd328.jpg\",\"http://8.131.54.14:83/articles/b5b0ad236a16cb10a22c6a1f5b37774f.jpg\",\"http://8.131.54.14:83/articles/b6de398283e8ba3b721053d0692955ed.jpg\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_operation_log` VALUES (731, '照片模块', '新增', '/admin/photos', 'com.minzheng.blog.controller.PhotoController.savePhotos', '保存照片', '[{\"albumId\":3,\"photoUrlList\":[\"http://8.131.54.14:83/articles/dda790792df9769e05bbff841f76be3d.jpg\",\"http://8.131.54.14:83/articles/a2424c1394de0c674f967d7ba664a88f.jpg\",\"http://8.131.54.14:83/articles/d9c4356f97fdb3f45e96768dab7f6706.jpg\",\"http://8.131.54.14:83/articles/bc93ce794469a8d56f805934c5f678ac.jpg\",\"http://8.131.54.14:83/articles/07726be6506fb3a1748ae20daf71cd88.jpg\",\"http://8.131.54.14:83/articles/e9a173a941baa62897dcc8e3f627f8d1.jpg\",\"http://8.131.54.14:83/articles/17a66951b61a2e9e3a4251176c9a9e28.jpg\",\"http://8.131.54.14:83/articles/86d3c2f22b220e35fd8e19193165910f.jpg\",\"http://8.131.54.14:83/articles/1c1ea0504a9447afe153ce1e5a7c867b.jpg\",\"http://8.131.54.14:83/articles/d65df8c22aafb5267b897c37e5b69a0f.jpg\",\"http://8.131.54.14:83/articles/f537234ba6923e11f209cadb8593a311.jpg\",\"http://8.131.54.14:83/articles/cca9a80d455a6c6a0ce581caa72b5831.jpg\",\"http://8.131.54.14:83/articles/2b460693330fa99209c1a01a1000d8b0.jpg\",\"http://8.131.54.14:83/articles/fec54845713d03a272bc5ee6fcc1c79a.jpg\",\"http://8.131.54.14:83/articles/8e3c699817717d2f28d544546cbe2f81.jpg\",\"http://8.131.54.14:83/articles/39a35afeb6ccc451eb9f1706fed6b703.jpg\",\"http://8.131.54.14:83/articles/9699d267d06c1773c3d60c72a962f2d0.jpg\",\"http://8.131.54.14:83/articles/58ca41093f2ff9bd2fe5640113a35ff1.jpg\",\"http://8.131.54.14:83/articles/e204a45f532414ff42d0bf557c7e4885.jpg\",\"http://8.131.54.14:83/articles/00212d0ec9c7a3d7204bc81fd68a2ae8.jpg\",\"http://8.131.54.14:83/articles/80f3d464aafeddc32cf6f2ee2a29cc3d.jpg\",\"http://8.131.54.14:83/articles/cc988e6fcf99cb09fa318934ab1103a1.jpg\",\"http://8.131.54.14:83/articles/f971e1d10935b8d237c6ed16bead9d6d.jpg\",\"http://8.131.54.14:83/articles/892bb377355d5ba0986785c36d70373a.jpg\",\"http://8.131.54.14:83/articles/846cba93eeacd0d02421231ab25a7947.jpg\",\"http://8.131.54.14:83/articles/e0d6bf3833597b7b4df5b98c957b4136.jpg\"]}]', 'POST', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_operation_log` VALUES (732, '照片模块', '修改', '/admin/photos/delete', 'com.minzheng.blog.controller.PhotoController.updatePhotoDelete', '更新照片删除状态', '[{\"idList\":[168],\"isDelete\":1}]', 'PUT', '{\"code\":20000,\"flag\":true,\"message\":\"操作成功\"}', 1, '管理员', '182.119.22.170', '河南省郑州市 联通', '2021-09-21 19:03:45', NULL);

-- ----------------------------
-- Table structure for tb_page
-- ----------------------------
DROP TABLE IF EXISTS `tb_page`;
CREATE TABLE `tb_page`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '页面id',
  `page_name` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '页面名',
  `page_label` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '页面标签',
  `page_cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '页面封面',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 903 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '页面' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_page
-- ----------------------------
INSERT INTO `tb_page` VALUES (1, '首页', 'home', 'http://8.131.54.14:83/config/6fda6624e5d4e80edc3c851f633268da.jpg', '2021-08-07 10:32:36', '2021-09-21 01:12:15');
INSERT INTO `tb_page` VALUES (2, '归档', 'archive', 'https://www.static.talkxj.com/wallroom-1920x1080-bg-338d7bc.jpg', '2021-08-07 10:32:36', NULL);
INSERT INTO `tb_page` VALUES (3, '分类', 'category', 'http://8.131.54.14:83/config/2a273104a938161f2151dbc46a20cd23.jpg', '2021-08-07 10:32:36', '2021-09-21 01:15:24');
INSERT INTO `tb_page` VALUES (4, '标签', 'tag', 'http://8.131.54.14:83/config/7dbba03b180e920870bee3924ca5579b.jpg', '2021-08-07 10:32:36', '2021-09-21 01:12:37');
INSERT INTO `tb_page` VALUES (5, '相册', 'album', 'https://www.static.talkxj.com/album.jpg', '2021-08-07 10:32:36', NULL);
INSERT INTO `tb_page` VALUES (6, '友链', 'link', 'http://8.131.54.14:83/config/0a129ee6c2764fac6d5eef2394db9b7a.jpg', '2021-08-07 10:32:36', '2021-09-20 23:14:54');
INSERT INTO `tb_page` VALUES (7, '关于', 'about', 'http://8.131.54.14:83/config/c7bbd3c43373872a08e0e577858e2c51.jpg', '2021-08-07 10:32:36', '2021-09-20 23:13:38');
INSERT INTO `tb_page` VALUES (8, '留言', 'message', 'https://www.static.talkxj.com/d5ojdj.jpg', '2021-08-07 10:32:36', NULL);
INSERT INTO `tb_page` VALUES (9, '个人中心', 'user', 'https://www.static.talkxj.com/article/setting.jpeg', '2021-08-07 10:32:36', NULL);
INSERT INTO `tb_page` VALUES (902, '文章列表', 'articleList', 'https://www.static.talkxj.com/photos/663a6e574a6845669fb417a3f02c7afb.jpg', '2021-08-10 15:36:19', NULL);

-- ----------------------------
-- Table structure for tb_photo
-- ----------------------------
DROP TABLE IF EXISTS `tb_photo`;
CREATE TABLE `tb_photo`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `album_id` int NOT NULL COMMENT '相册id',
  `photo_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '照片名',
  `photo_desc` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '照片描述',
  `photo_compress` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '压缩后的图片地址',
  `photo_src` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '照片地址',
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 189 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '照片' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_photo
-- ----------------------------
INSERT INTO `tb_photo` VALUES (21, 3, '1439972952697028609', NULL, NULL, 'http://8.131.54.14:83/articles/92e78b7c6586c5c5620190964908c6c7.jpg', 1, '2021-09-20 23:21:20', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (22, 3, '1439972952697028610', NULL, NULL, 'http://8.131.54.14:83/articles/c7bbd3c43373872a08e0e577858e2c51.jpg', 1, '2021-09-20 23:21:20', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (23, 3, '1439972952697028611', NULL, NULL, 'http://8.131.54.14:83/articles/ab11cc8ca3d2258dae7b40a802d20e83.jpg', 1, '2021-09-20 23:21:20', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (24, 3, '1439972952697028612', NULL, NULL, 'http://8.131.54.14:83/articles/9c7fc5035adba1571d27549d2d98e123.jpg', 1, '2021-09-20 23:21:20', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (25, 3, '1439972952697028613', NULL, NULL, 'http://8.131.54.14:83/articles/4adce412894f3a9c2c1a942065859037.jpg', 1, '2021-09-20 23:21:20', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (26, 3, '1439972952697028614', NULL, NULL, 'http://8.131.54.14:83/articles/ca0bf4cf744e32d694f7965450fd9d16.jpg', 1, '2021-09-20 23:21:20', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (27, 3, '1439972952697028615', NULL, NULL, 'http://8.131.54.14:83/articles/57f840c784d6e921b9be6324f2d038f1.jpg', 1, '2021-09-20 23:21:20', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (28, 3, '1439975030953689090', NULL, NULL, 'http://8.131.54.14:83/articles/00212d0ec9c7a3d7204bc81fd68a2ae8.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (29, 3, '1439975030953689091', NULL, NULL, 'http://8.131.54.14:83/articles/2a273104a938161f2151dbc46a20cd23.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (30, 3, '1439975030953689092', NULL, NULL, 'http://8.131.54.14:83/articles/580a71e15056ca5af9d854eef89956d8.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (31, 3, '1439975030953689093', NULL, NULL, 'http://8.131.54.14:83/articles/0cb8cc1fe5b09776215d0be91c6b6f13.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (32, 3, '1439975030953689094', NULL, NULL, 'http://8.131.54.14:83/articles/479f4af71fd0addfe4f9fb9fb054c2e6.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (33, 3, '1439975030953689095', NULL, NULL, 'http://8.131.54.14:83/articles/382a1e91f66baef5be699be7b68cc7da.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (34, 3, '1439975030953689096', NULL, NULL, 'http://8.131.54.14:83/articles/ecfe698a85901b9fc504bf94e52d5da9.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (35, 3, '1439975030953689097', NULL, NULL, 'http://8.131.54.14:83/articles/26bfe22548973428ddf3532c15095af1.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (36, 3, '1439975030953689098', NULL, NULL, 'http://8.131.54.14:83/articles/3bf823d4221a27d41bda273beb1f0e14.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (37, 3, '1439975030953689099', NULL, NULL, 'http://8.131.54.14:83/articles/d0d1aa5e0f1c1b49fe4346ec6a93c0a1.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:46');
INSERT INTO `tb_photo` VALUES (38, 3, '1439975030953689100', NULL, NULL, 'http://8.131.54.14:83/articles/1a69ad8431e57ab978d7cbf2dee6662f.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (39, 3, '1439975030953689101', NULL, NULL, 'http://8.131.54.14:83/articles/3571985233db5f47ac0c60cc4a3cf2b4.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (40, 3, '1439975030953689102', NULL, NULL, 'http://8.131.54.14:83/articles/a70032ec9baa881321abff48bca35de3.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (41, 3, '1439975030953689103', NULL, NULL, 'http://8.131.54.14:83/articles/3a32e1783ad95aefcd7a4cd15d53aa48.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (42, 3, '1439975030953689104', NULL, NULL, 'http://8.131.54.14:83/articles/2662bc80c6d1bc871d8276fc7852425b.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (43, 3, '1439975030953689105', NULL, NULL, 'http://8.131.54.14:83/articles/cedc33b0045f5d4702b9b8ba3692978b.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (44, 3, '1439975030953689106', NULL, NULL, 'http://8.131.54.14:83/articles/902661f2bbbcf62268637c0c92ead031.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (45, 3, '1439975030953689107', NULL, NULL, 'http://8.131.54.14:83/articles/58ca41093f2ff9bd2fe5640113a35ff1.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (46, 3, '1439975030953689108', NULL, NULL, 'http://8.131.54.14:83/articles/0c674d8258c7f4ece3dd7921eeb437ec.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (47, 3, '1439975030953689109', NULL, NULL, 'http://8.131.54.14:83/articles/daba30642c8cefbf8f3b8bbbd3236ead.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (48, 3, '1439975030953689110', NULL, NULL, 'http://8.131.54.14:83/articles/d3447e329c1ecbf753c96dedeee09ced.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (49, 3, '1439975030953689111', NULL, NULL, 'http://8.131.54.14:83/articles/7f00a64f169e9294fbb29c8e010af73a.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (50, 3, '1439975030953689112', NULL, NULL, 'http://8.131.54.14:83/articles/4406841ec66c688e8450f84cbd557b7b.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (51, 3, '1439975030953689113', NULL, NULL, 'http://8.131.54.14:83/articles/9ad17bd824f692636d0f20f0722b005b.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (52, 3, '1439975030953689114', NULL, NULL, 'http://8.131.54.14:83/articles/a91b381285b6302184a3cadc9147b14e.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (53, 3, '1439975030953689115', NULL, NULL, 'http://8.131.54.14:83/articles/d0c32b20289b767094b752ba922dfc68.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (54, 3, '1439975030953689116', NULL, NULL, 'http://8.131.54.14:83/articles/d057ec5e5d83c0fa52de422cbb3e46f4.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (55, 3, '1439975030953689117', NULL, NULL, 'http://8.131.54.14:83/articles/25ad2ad8ef92ac45b659d6af873d93b3.jpg', 1, '2021-09-20 23:29:35', '2021-09-21 18:54:42');
INSERT INTO `tb_photo` VALUES (56, 4, '1439978065381638146', NULL, NULL, 'http://8.131.54.14:83/articles/683e6d5603cad71cc046bc0b1e9e4264.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:19');
INSERT INTO `tb_photo` VALUES (57, 4, '1439978065381638147', NULL, NULL, 'http://8.131.54.14:83/articles/5244aa98f61062f3c47d2ac573538843.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:19');
INSERT INTO `tb_photo` VALUES (58, 4, '1439978065381638148', NULL, NULL, 'http://8.131.54.14:83/articles/f5e8a9131fc75c7036399b748d25b76a.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:19');
INSERT INTO `tb_photo` VALUES (59, 4, '1439978065381638149', NULL, NULL, 'http://8.131.54.14:83/articles/5009ed6b0923c395ede699485032c146.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:19');
INSERT INTO `tb_photo` VALUES (60, 4, '1439978065381638150', NULL, NULL, 'http://8.131.54.14:83/articles/75c47155cf833ef32aeb635b52884397.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:19');
INSERT INTO `tb_photo` VALUES (61, 4, '1439978065381638151', NULL, NULL, 'http://8.131.54.14:83/articles/a822004a23789295bf1cc5255046b656.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:19');
INSERT INTO `tb_photo` VALUES (62, 4, '1439978065381638152', NULL, NULL, 'http://8.131.54.14:83/articles/1747d47ec70c992f20a93d722c6ab779.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:19');
INSERT INTO `tb_photo` VALUES (63, 4, '1439978065381638153', NULL, NULL, 'http://8.131.54.14:83/articles/6acd60b75b7b0ffea7e543f1c30c1b3b.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:19');
INSERT INTO `tb_photo` VALUES (64, 4, '1439978065381638154', NULL, NULL, 'http://8.131.54.14:83/articles/25d8e355d844357acab5e9bb9fa106ec.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:19');
INSERT INTO `tb_photo` VALUES (65, 4, '1439978065381638155', NULL, NULL, 'http://8.131.54.14:83/articles/67fc681c9311571e753cf450e5af8e1c.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:19');
INSERT INTO `tb_photo` VALUES (66, 4, '1439978065381638156', NULL, NULL, 'http://8.131.54.14:83/articles/7dbba03b180e920870bee3924ca5579b.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (67, 4, '1439978065381638157', NULL, NULL, 'http://8.131.54.14:83/articles/6fda6624e5d4e80edc3c851f633268da.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (68, 4, '1439978065381638158', NULL, NULL, 'http://8.131.54.14:83/articles/2ac8957f9c7b35a26b5408760c21839c.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (69, 4, '1439978065381638159', NULL, NULL, 'http://8.131.54.14:83/articles/ea74a2f80704b244467dfaa4d99604a4.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (70, 4, '1439978065381638160', NULL, NULL, 'http://8.131.54.14:83/articles/ddb5646607568a9638f6cf2dc8f2d5b7.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (71, 4, '1439978065381638161', NULL, NULL, 'http://8.131.54.14:83/articles/264b564911f49ec4bcd55c1ff6fb5f16.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (72, 4, '1439978065381638162', NULL, NULL, 'http://8.131.54.14:83/articles/a936814cffa6f4d5177e6183a0001d05.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (73, 4, '1439978065381638163', NULL, NULL, 'http://8.131.54.14:83/articles/94b70a9d5525573cb34747e3d5c22a19.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (74, 4, '1439978065381638164', NULL, NULL, 'http://8.131.54.14:83/articles/a55fd7131261214530717fcf42e4c3dc.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (75, 4, '1439978065381638165', NULL, NULL, 'http://8.131.54.14:83/articles/66cd740e3fc2db2924686aa7cfd43127.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (76, 4, '1439978065381638166', NULL, NULL, 'http://8.131.54.14:83/articles/ddb58dee0a4ef8ce8d38e7228adc73fe.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (77, 4, '1439978065381638167', NULL, NULL, 'http://8.131.54.14:83/articles/3f5d1cc80cbee3bcef8be14e3e2ae629.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (78, 4, '1439978065381638168', NULL, NULL, 'http://8.131.54.14:83/articles/a1f3fef23a7f8c5fe8b7a51a5b558fa2.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (79, 4, '1439978065381638169', NULL, NULL, 'http://8.131.54.14:83/articles/1ff0a8942e3993970341dad4446a484e.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (80, 4, '1439978065381638170', NULL, NULL, 'http://8.131.54.14:83/articles/22f2a50e4e0d4499eb551791024623a3.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (81, 4, '1439978065381638171', NULL, NULL, 'http://8.131.54.14:83/articles/75772f33629d416b013cc45335827f47.jpg', 1, '2021-09-20 23:41:39', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (82, 4, '1440210468133998593', NULL, NULL, 'http://8.131.54.14:83/articles/e84207b03afc38ab7a26d8985a17ef4a.jpg', 1, '2021-09-21 15:05:08', '2021-09-21 15:05:15');
INSERT INTO `tb_photo` VALUES (83, 4, '1440216895623860226', NULL, NULL, 'http://8.131.54.14:83/articles/1f92064ced101aee81d47e2ba1dcd765.JPG', 1, '2021-09-21 15:30:41', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (84, 4, '1440218350363676673', NULL, NULL, 'http://8.131.54.14:83/articles/5fcd05feadea386a795aa3f0a2a11f4a.jpg', 1, '2021-09-21 15:36:27', '2021-09-21 15:37:14');
INSERT INTO `tb_photo` VALUES (85, 4, '1440219127400435713', NULL, NULL, 'http://8.131.54.14:83/articles/76de2af0867a5200f3701647386498e4.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:48');
INSERT INTO `tb_photo` VALUES (86, 4, '1440219127400435714', NULL, NULL, 'http://8.131.54.14:83/articles/647540c79018ecc58b68e6e33caab609.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:48');
INSERT INTO `tb_photo` VALUES (87, 4, '1440219127400435715', NULL, NULL, 'http://8.131.54.14:83/articles/aa997c149affe78e05cfa51c0449a0e2.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:48');
INSERT INTO `tb_photo` VALUES (88, 4, '1440219127400435716', NULL, NULL, 'http://8.131.54.14:83/articles/e6b6468fe090dc20d49e175fd34cbfc5.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:48');
INSERT INTO `tb_photo` VALUES (89, 4, '1440219127400435717', NULL, NULL, 'http://8.131.54.14:83/articles/4c572f1903a0b49d8a51a8bb5a77fc86.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:48');
INSERT INTO `tb_photo` VALUES (90, 4, '1440219127400435718', NULL, NULL, 'http://8.131.54.14:83/articles/3de4c1046260e1541eade0524fcc18c9.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:48');
INSERT INTO `tb_photo` VALUES (91, 4, '1440219127400435719', NULL, NULL, 'http://8.131.54.14:83/articles/8fdf3c05a02ca0390f42e5ffcdb51b6a.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:48');
INSERT INTO `tb_photo` VALUES (92, 4, '1440219127400435720', NULL, NULL, 'http://8.131.54.14:83/articles/6b5222ce0b24cac4c266b956c946e8f4.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:48');
INSERT INTO `tb_photo` VALUES (93, 4, '1440219127400435721', NULL, NULL, 'http://8.131.54.14:83/articles/2da0e00bcb06a3c2f7bade415e65ab38.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:48');
INSERT INTO `tb_photo` VALUES (94, 4, '1440219127400435722', NULL, NULL, 'http://8.131.54.14:83/articles/d5053a222168b3193a93307def6b8ede.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (95, 4, '1440219127400435723', NULL, NULL, 'http://8.131.54.14:83/articles/9c4be3a980aa606a0e2714047b829d90.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (96, 4, '1440219127400435724', NULL, NULL, 'http://8.131.54.14:83/articles/5a2458fe691ff5bbb837734368dda863.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (97, 4, '1440219127400435725', NULL, NULL, 'http://8.131.54.14:83/articles/4b3511a78a068631a8321806cf68cb2d.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (98, 4, '1440219127400435726', NULL, NULL, 'http://8.131.54.14:83/articles/96727fec3160ad7114bd424377a90ee8.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (99, 4, '1440219127400435727', NULL, NULL, 'http://8.131.54.14:83/articles/8b8d7f6fa1e6ddd072e19706748907d7.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (100, 4, '1440219127400435728', NULL, NULL, 'http://8.131.54.14:83/articles/ef1b12dc91608f450057c6743db026be.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (101, 4, '1440219127400435729', NULL, NULL, 'http://8.131.54.14:83/articles/3c26e14b664a95f4389770f309a46f27.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (102, 4, '1440219127400435730', NULL, NULL, 'http://8.131.54.14:83/articles/2c27711e0a364e1a3545dfab4a21367a.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (103, 4, '1440219127400435731', NULL, NULL, 'http://8.131.54.14:83/articles/cefc6164ca1fac1eb6ebc93e36d3248c.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (104, 4, '1440219127400435732', NULL, NULL, 'http://8.131.54.14:83/articles/b7c127c1415e95aae76dfba327d984e3.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (105, 4, '1440219127400435733', NULL, NULL, 'http://8.131.54.14:83/articles/f352e403085e4786af74549e13199ff8.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (106, 4, '1440219127400435734', NULL, NULL, 'http://8.131.54.14:83/articles/e38c4cc62b6c77b501c52fb37d63774f.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (107, 4, '1440219127400435735', NULL, NULL, 'http://8.131.54.14:83/articles/22a960f6bb21b95a9a81858cba45ad47.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (108, 4, '1440219127400435736', NULL, NULL, 'http://8.131.54.14:83/articles/fc2da1db07447ff75056d89456ddc69e.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (109, 4, '1440219127400435737', NULL, NULL, 'http://8.131.54.14:83/articles/6cc14c49c8fd6d51733f389865cd028b.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (110, 4, '1440219127400435738', NULL, NULL, 'http://8.131.54.14:83/articles/d996aa2bf72f37122241ddf0a76987c1.jpg', 1, '2021-09-21 15:39:33', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (111, 4, '1440240429653889025', NULL, NULL, 'http://8.131.54.14:83/articles/2a273104a938161f2151dbc46a20cd23.jpg', 1, '2021-09-21 17:04:11', '2021-09-21 18:42:44');
INSERT INTO `tb_photo` VALUES (112, 4, '1440265599554682881', NULL, 'http://8.131.54.14:83/articles/compress_76de2af0867a5200f3701647386498e4.jpg', 'http://8.131.54.14:83/articles/76de2af0867a5200f3701647386498e4.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (113, 4, '1440265601345650690', NULL, 'http://8.131.54.14:83/articles/compress_aa997c149affe78e05cfa51c0449a0e2.jpg', 'http://8.131.54.14:83/articles/aa997c149affe78e05cfa51c0449a0e2.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (114, 4, '1440265602826240001', NULL, 'http://8.131.54.14:83/articles/compress_e6b6468fe090dc20d49e175fd34cbfc5.jpg', 'http://8.131.54.14:83/articles/e6b6468fe090dc20d49e175fd34cbfc5.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (115, 4, '1440265609033809921', NULL, 'http://8.131.54.14:83/articles/compress_4c572f1903a0b49d8a51a8bb5a77fc86.jpg', 'http://8.131.54.14:83/articles/4c572f1903a0b49d8a51a8bb5a77fc86.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (116, 4, '1440265610594091009', NULL, 'http://8.131.54.14:83/articles/compress_3de4c1046260e1541eade0524fcc18c9.jpg', 'http://8.131.54.14:83/articles/3de4c1046260e1541eade0524fcc18c9.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (117, 4, '1440265612204703746', NULL, 'http://8.131.54.14:83/articles/compress_647540c79018ecc58b68e6e33caab609.jpg', 'http://8.131.54.14:83/articles/647540c79018ecc58b68e6e33caab609.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (118, 4, '1440265613702070273', NULL, 'http://8.131.54.14:83/articles/compress_2da0e00bcb06a3c2f7bade415e65ab38.jpg', 'http://8.131.54.14:83/articles/2da0e00bcb06a3c2f7bade415e65ab38.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (119, 4, '1440265615358820353', NULL, 'http://8.131.54.14:83/articles/compress_8fdf3c05a02ca0390f42e5ffcdb51b6a.jpg', 'http://8.131.54.14:83/articles/8fdf3c05a02ca0390f42e5ffcdb51b6a.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (120, 4, '1440265616617111553', NULL, 'http://8.131.54.14:83/articles/compress_6b5222ce0b24cac4c266b956c946e8f4.jpg', 'http://8.131.54.14:83/articles/6b5222ce0b24cac4c266b956c946e8f4.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (121, 4, '1440265618814926849', NULL, 'http://8.131.54.14:83/articles/compress_d5053a222168b3193a93307def6b8ede.jpg', 'http://8.131.54.14:83/articles/d5053a222168b3193a93307def6b8ede.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (122, 4, '1440265624485625858', NULL, 'http://8.131.54.14:83/articles/compress_9c4be3a980aa606a0e2714047b829d90.jpg', 'http://8.131.54.14:83/articles/9c4be3a980aa606a0e2714047b829d90.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (123, 4, '1440265630957436929', NULL, 'http://8.131.54.14:83/articles/compress_8b8d7f6fa1e6ddd072e19706748907d7.jpg', 'http://8.131.54.14:83/articles/8b8d7f6fa1e6ddd072e19706748907d7.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (124, 4, '1440265632635158530', NULL, 'http://8.131.54.14:83/articles/compress_96727fec3160ad7114bd424377a90ee8.jpg', 'http://8.131.54.14:83/articles/96727fec3160ad7114bd424377a90ee8.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (125, 4, '1440265634166079490', NULL, 'http://8.131.54.14:83/articles/compress_ef1b12dc91608f450057c6743db026be.jpg', 'http://8.131.54.14:83/articles/ef1b12dc91608f450057c6743db026be.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (126, 4, '1440265635684417538', NULL, 'http://8.131.54.14:83/articles/compress_3c26e14b664a95f4389770f309a46f27.jpg', 'http://8.131.54.14:83/articles/3c26e14b664a95f4389770f309a46f27.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (127, 4, '1440265637253087234', NULL, 'http://8.131.54.14:83/articles/compress_2c27711e0a364e1a3545dfab4a21367a.jpg', 'http://8.131.54.14:83/articles/2c27711e0a364e1a3545dfab4a21367a.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (128, 4, '1440265643620040706', NULL, 'http://8.131.54.14:83/articles/compress_fc2da1db07447ff75056d89456ddc69e.jpg', 'http://8.131.54.14:83/articles/fc2da1db07447ff75056d89456ddc69e.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (129, 4, '1440265645343899649', NULL, 'http://8.131.54.14:83/articles/compress_cefc6164ca1fac1eb6ebc93e36d3248c.jpg', 'http://8.131.54.14:83/articles/cefc6164ca1fac1eb6ebc93e36d3248c.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (130, 4, '1440265646899986434', NULL, 'http://8.131.54.14:83/articles/compress_b7c127c1415e95aae76dfba327d984e3.jpg', 'http://8.131.54.14:83/articles/b7c127c1415e95aae76dfba327d984e3.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (131, 4, '1440265650347704321', NULL, 'http://8.131.54.14:83/articles/compress_5a2458fe691ff5bbb837734368dda863.jpg', 'http://8.131.54.14:83/articles/5a2458fe691ff5bbb837734368dda863.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (132, 4, '1440265656177786882', NULL, 'http://8.131.54.14:83/articles/compress_4b3511a78a068631a8321806cf68cb2d.jpg', 'http://8.131.54.14:83/articles/4b3511a78a068631a8321806cf68cb2d.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (133, 4, '1440265658786643970', NULL, 'http://8.131.54.14:83/articles/compress_e38c4cc62b6c77b501c52fb37d63774f.jpg', 'http://8.131.54.14:83/articles/e38c4cc62b6c77b501c52fb37d63774f.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (134, 4, '1440265660460171266', NULL, 'http://8.131.54.14:83/articles/compress_22a960f6bb21b95a9a81858cba45ad47.jpg', 'http://8.131.54.14:83/articles/22a960f6bb21b95a9a81858cba45ad47.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (135, 4, '1440265662355996674', NULL, 'http://8.131.54.14:83/articles/compress_f352e403085e4786af74549e13199ff8.jpg', 'http://8.131.54.14:83/articles/f352e403085e4786af74549e13199ff8.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (136, 4, '1440265664004358146', NULL, 'http://8.131.54.14:83/articles/compress_d996aa2bf72f37122241ddf0a76987c1.jpg', 'http://8.131.54.14:83/articles/d996aa2bf72f37122241ddf0a76987c1.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (137, 4, '1440265670723633154', NULL, 'http://8.131.54.14:83/articles/compress_6cc14c49c8fd6d51733f389865cd028b.jpg', 'http://8.131.54.14:83/articles/6cc14c49c8fd6d51733f389865cd028b.jpg', 0, '2021-09-21 18:44:29', NULL);
INSERT INTO `tb_photo` VALUES (138, 3, '1440268397792985089', NULL, 'http://8.131.54.14:83/articles/compress_2a273104a938161f2151dbc46a20cd23.jpg', 'http://8.131.54.14:83/articles/2a273104a938161f2151dbc46a20cd23.jpg', 0, '2021-09-21 18:55:21', NULL);
INSERT INTO `tb_photo` VALUES (139, 3, '1440268398120140801', NULL, 'http://8.131.54.14:83/articles/compress_8834cf5a60c617aca30548ad7d4ae917.jpg', 'http://8.131.54.14:83/articles/8834cf5a60c617aca30548ad7d4ae917.jpg', 0, '2021-09-21 18:55:21', NULL);
INSERT INTO `tb_photo` VALUES (140, 3, '1440268398497628162', NULL, 'http://8.131.54.14:83/articles/compress_fa463458b06cc68da4b55472e44f1bc5.jpg', 'http://8.131.54.14:83/articles/fa463458b06cc68da4b55472e44f1bc5.jpg', 0, '2021-09-21 18:55:21', NULL);
INSERT INTO `tb_photo` VALUES (141, 3, '1440268401223925761', NULL, 'http://8.131.54.14:83/articles/compress_421c2a74ec9b49ee3ceaf1533b12d571.jpg', 'http://8.131.54.14:83/articles/421c2a74ec9b49ee3ceaf1533b12d571.jpg', 0, '2021-09-21 18:55:21', NULL);
INSERT INTO `tb_photo` VALUES (142, 3, '1440268402905841665', NULL, 'http://8.131.54.14:83/articles/compress_444c25b48039c4895b730c2363cff5fc.jpg', 'http://8.131.54.14:83/articles/444c25b48039c4895b730c2363cff5fc.jpg', 0, '2021-09-21 18:55:21', NULL);
INSERT INTO `tb_photo` VALUES (143, 3, '1440268404541620225', NULL, 'http://8.131.54.14:83/articles/compress_b7c127c1415e95aae76dfba327d984e3.jpg', 'http://8.131.54.14:83/articles/b7c127c1415e95aae76dfba327d984e3.jpg', 0, '2021-09-21 18:55:21', NULL);
INSERT INTO `tb_photo` VALUES (144, 3, '1440268770897297409', NULL, 'http://8.131.54.14:83/articles/compress_1df4d0cc9777da7fe795b8c30c2bc144.jpg', 'http://8.131.54.14:83/articles/1df4d0cc9777da7fe795b8c30c2bc144.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (145, 3, '1440268772012982274', NULL, 'http://8.131.54.14:83/articles/compress_3bd2b7f1021de42b612b06273034c95f.jpg', 'http://8.131.54.14:83/articles/3bd2b7f1021de42b612b06273034c95f.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (146, 3, '1440268773262884866', NULL, 'http://8.131.54.14:83/articles/compress_00bf0b48dd553dc7d4e2bdb2b2421025.jpg', 'http://8.131.54.14:83/articles/00bf0b48dd553dc7d4e2bdb2b2421025.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (147, 3, '1440268774571507714', NULL, 'http://8.131.54.14:83/articles/compress_4f7815feeeddfacc4822009852ddda62.jpg', 'http://8.131.54.14:83/articles/4f7815feeeddfacc4822009852ddda62.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (148, 3, '1440268775779467266', NULL, 'http://8.131.54.14:83/articles/compress_ef79ad68904159854d30eda845ac13c0.jpg', 'http://8.131.54.14:83/articles/ef79ad68904159854d30eda845ac13c0.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (149, 3, '1440268777041952769', NULL, 'http://8.131.54.14:83/articles/compress_4b2a200ef24195695502704fc0e602a7.jpg', 'http://8.131.54.14:83/articles/4b2a200ef24195695502704fc0e602a7.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (150, 3, '1440268779151687681', NULL, 'http://8.131.54.14:83/articles/compress_9e1bfcb1f70d8b4aff96115f1021cf7a.jpg', 'http://8.131.54.14:83/articles/9e1bfcb1f70d8b4aff96115f1021cf7a.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (151, 3, '1440268780267372545', NULL, 'http://8.131.54.14:83/articles/compress_ac2b635b123aca15a2addc02a798ab5b.jpg', 'http://8.131.54.14:83/articles/ac2b635b123aca15a2addc02a798ab5b.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (152, 3, '1440268781496303618', NULL, 'http://8.131.54.14:83/articles/compress_bc5a4d47a5ca984974f522f1194b3dfc.jpg', 'http://8.131.54.14:83/articles/bc5a4d47a5ca984974f522f1194b3dfc.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (153, 3, '1440268782620377090', NULL, 'http://8.131.54.14:83/articles/compress_583b82092ecf0bd302a1665fdfae6f73.jpg', 'http://8.131.54.14:83/articles/583b82092ecf0bd302a1665fdfae6f73.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (154, 3, '1440268783933194241', NULL, 'http://8.131.54.14:83/articles/compress_51ac932fc5a57514528f9bf130fdea2c.jpg', 'http://8.131.54.14:83/articles/51ac932fc5a57514528f9bf130fdea2c.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (155, 3, '1440268785246011394', NULL, 'http://8.131.54.14:83/articles/compress_f591c229c482392c69965f85e867b105.jpg', 'http://8.131.54.14:83/articles/f591c229c482392c69965f85e867b105.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (156, 3, '1440268786726600705', NULL, 'http://8.131.54.14:83/articles/compress_448ba6fd79c7e6377c0bfef5e1589efb.jpg', 'http://8.131.54.14:83/articles/448ba6fd79c7e6377c0bfef5e1589efb.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (157, 3, '1440268787871645697', NULL, 'http://8.131.54.14:83/articles/compress_e7871d171de53c83126a727f6177f292.jpg', 'http://8.131.54.14:83/articles/e7871d171de53c83126a727f6177f292.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (158, 3, '1440268789113159681', NULL, 'http://8.131.54.14:83/articles/compress_1768dad0c62312062fe32067174223aa.jpg', 'http://8.131.54.14:83/articles/1768dad0c62312062fe32067174223aa.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (159, 3, '1440268793596870658', NULL, 'http://8.131.54.14:83/articles/compress_cad547b4c355dd8f3519c087783caecc.jpg', 'http://8.131.54.14:83/articles/cad547b4c355dd8f3519c087783caecc.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (160, 3, '1440268794901299202', NULL, 'http://8.131.54.14:83/articles/compress_af5092f98fe95820f48dc8ca672bd328.jpg', 'http://8.131.54.14:83/articles/af5092f98fe95820f48dc8ca672bd328.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (161, 3, '1440268800530055169', NULL, 'http://8.131.54.14:83/articles/compress_b5b0ad236a16cb10a22c6a1f5b37774f.jpg', 'http://8.131.54.14:83/articles/b5b0ad236a16cb10a22c6a1f5b37774f.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (162, 3, '1440268805697437697', NULL, 'http://8.131.54.14:83/articles/compress_b6de398283e8ba3b721053d0692955ed.jpg', 'http://8.131.54.14:83/articles/b6de398283e8ba3b721053d0692955ed.jpg', 0, '2021-09-21 18:56:57', NULL);
INSERT INTO `tb_photo` VALUES (163, 3, '1440269395714375682', NULL, 'http://8.131.54.14:83/articles/compress_dda790792df9769e05bbff841f76be3d.jpg', 'http://8.131.54.14:83/articles/dda790792df9769e05bbff841f76be3d.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (164, 3, '1440269398558113794', NULL, 'http://8.131.54.14:83/articles/compress_a2424c1394de0c674f967d7ba664a88f.jpg', 'http://8.131.54.14:83/articles/a2424c1394de0c674f967d7ba664a88f.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (165, 3, '1440269399396974593', NULL, 'http://8.131.54.14:83/articles/compress_d9c4356f97fdb3f45e96768dab7f6706.jpg', 'http://8.131.54.14:83/articles/d9c4356f97fdb3f45e96768dab7f6706.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (166, 3, '1440269399942234113', NULL, 'http://8.131.54.14:83/articles/compress_bc93ce794469a8d56f805934c5f678ac.jpg', 'http://8.131.54.14:83/articles/bc93ce794469a8d56f805934c5f678ac.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (167, 3, '1440269400395218946', NULL, 'http://8.131.54.14:83/articles/compress_07726be6506fb3a1748ae20daf71cd88.jpg', 'http://8.131.54.14:83/articles/07726be6506fb3a1748ae20daf71cd88.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (168, 3, '1440269401020170242', NULL, 'http://8.131.54.14:83/articles/compress_e9a173a941baa62897dcc8e3f627f8d1.jpg', 'http://8.131.54.14:83/articles/e9a173a941baa62897dcc8e3f627f8d1.jpg', 1, '2021-09-21 18:59:23', '2021-09-21 19:03:45');
INSERT INTO `tb_photo` VALUES (169, 3, '1440269402102300674', NULL, 'http://8.131.54.14:83/articles/compress_17a66951b61a2e9e3a4251176c9a9e28.jpg', 'http://8.131.54.14:83/articles/17a66951b61a2e9e3a4251176c9a9e28.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (170, 3, '1440269404623077378', NULL, 'http://8.131.54.14:83/articles/compress_86d3c2f22b220e35fd8e19193165910f.jpg', 'http://8.131.54.14:83/articles/86d3c2f22b220e35fd8e19193165910f.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (171, 3, '1440269405029924865', NULL, 'http://8.131.54.14:83/articles/compress_1c1ea0504a9447afe153ce1e5a7c867b.jpg', 'http://8.131.54.14:83/articles/1c1ea0504a9447afe153ce1e5a7c867b.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (172, 3, '1440269405629710338', NULL, 'http://8.131.54.14:83/articles/compress_d65df8c22aafb5267b897c37e5b69a0f.jpg', 'http://8.131.54.14:83/articles/d65df8c22aafb5267b897c37e5b69a0f.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (173, 3, '1440269407622004737', NULL, 'http://8.131.54.14:83/articles/compress_f537234ba6923e11f209cadb8593a311.jpg', 'http://8.131.54.14:83/articles/f537234ba6923e11f209cadb8593a311.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (174, 3, '1440269408251150337', NULL, 'http://8.131.54.14:83/articles/compress_cca9a80d455a6c6a0ce581caa72b5831.jpg', 'http://8.131.54.14:83/articles/cca9a80d455a6c6a0ce581caa72b5831.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (175, 3, '1440269408653803521', NULL, 'http://8.131.54.14:83/articles/compress_2b460693330fa99209c1a01a1000d8b0.jpg', 'http://8.131.54.14:83/articles/2b460693330fa99209c1a01a1000d8b0.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (176, 3, '1440269409069039617', NULL, 'http://8.131.54.14:83/articles/compress_fec54845713d03a272bc5ee6fcc1c79a.jpg', 'http://8.131.54.14:83/articles/fec54845713d03a272bc5ee6fcc1c79a.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (177, 3, '1440269409308114946', NULL, 'http://8.131.54.14:83/articles/compress_8e3c699817717d2f28d544546cbe2f81.jpg', 'http://8.131.54.14:83/articles/8e3c699817717d2f28d544546cbe2f81.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (178, 3, '1440269409844985858', NULL, 'http://8.131.54.14:83/articles/compress_39a35afeb6ccc451eb9f1706fed6b703.jpg', 'http://8.131.54.14:83/articles/39a35afeb6ccc451eb9f1706fed6b703.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (179, 3, '1440269411652730882', NULL, 'http://8.131.54.14:83/articles/compress_9699d267d06c1773c3d60c72a962f2d0.jpg', 'http://8.131.54.14:83/articles/9699d267d06c1773c3d60c72a962f2d0.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (180, 3, '1440269411933749250', NULL, 'http://8.131.54.14:83/articles/compress_58ca41093f2ff9bd2fe5640113a35ff1.jpg', 'http://8.131.54.14:83/articles/58ca41093f2ff9bd2fe5640113a35ff1.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (181, 3, '1440269412281876481', NULL, 'http://8.131.54.14:83/articles/compress_e204a45f532414ff42d0bf557c7e4885.jpg', 'http://8.131.54.14:83/articles/e204a45f532414ff42d0bf557c7e4885.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (182, 3, '1440269412579672065', NULL, 'http://8.131.54.14:83/articles/compress_00212d0ec9c7a3d7204bc81fd68a2ae8.jpg', 'http://8.131.54.14:83/articles/00212d0ec9c7a3d7204bc81fd68a2ae8.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (183, 3, '1440269412898439169', NULL, 'http://8.131.54.14:83/articles/compress_80f3d464aafeddc32cf6f2ee2a29cc3d.jpg', 'http://8.131.54.14:83/articles/80f3d464aafeddc32cf6f2ee2a29cc3d.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (184, 3, '1440269413355618306', NULL, 'http://8.131.54.14:83/articles/compress_cc988e6fcf99cb09fa318934ab1103a1.jpg', 'http://8.131.54.14:83/articles/cc988e6fcf99cb09fa318934ab1103a1.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (185, 3, '1440269413682774017', NULL, 'http://8.131.54.14:83/articles/compress_f971e1d10935b8d237c6ed16bead9d6d.jpg', 'http://8.131.54.14:83/articles/f971e1d10935b8d237c6ed16bead9d6d.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (186, 3, '1440269414840401922', NULL, 'http://8.131.54.14:83/articles/compress_892bb377355d5ba0986785c36d70373a.jpg', 'http://8.131.54.14:83/articles/892bb377355d5ba0986785c36d70373a.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (187, 3, '1440269416186773506', NULL, 'http://8.131.54.14:83/articles/compress_846cba93eeacd0d02421231ab25a7947.jpg', 'http://8.131.54.14:83/articles/846cba93eeacd0d02421231ab25a7947.jpg', 0, '2021-09-21 18:59:23', NULL);
INSERT INTO `tb_photo` VALUES (188, 3, '1440269417491202049', NULL, 'http://8.131.54.14:83/articles/compress_e0d6bf3833597b7b4df5b98c957b4136.jpg', 'http://8.131.54.14:83/articles/e0d6bf3833597b7b4df5b98c957b4136.jpg', 0, '2021-09-21 18:59:23', NULL);

-- ----------------------------
-- Table structure for tb_photo_album
-- ----------------------------
DROP TABLE IF EXISTS `tb_photo_album`;
CREATE TABLE `tb_photo_album`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `album_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '相册名',
  `album_desc` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '相册描述',
  `album_cover` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '相册封面',
  `is_delete` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否删除',
  `status` tinyint(1) NOT NULL DEFAULT 1 COMMENT '状态值 1公开 2私密',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 5 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci COMMENT = '相册' ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_photo_album
-- ----------------------------
INSERT INTO `tb_photo_album` VALUES (3, '配图', '精美高清配图', 'http://8.131.54.14:83/config/92e78b7c6586c5c5620190964908c6c7.jpg', 0, 1, '2021-09-20 23:17:05', '2021-09-21 18:54:35');
INSERT INTO `tb_photo_album` VALUES (4, '壁纸', '4K高清壁纸', 'http://8.131.54.14:83/config/5009ed6b0923c395ede699485032c146.jpg', 0, 1, '2021-09-20 23:40:11', NULL);

-- ----------------------------
-- Table structure for tb_resource
-- ----------------------------
DROP TABLE IF EXISTS `tb_resource`;
CREATE TABLE `tb_resource`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `resource_name` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '资源名',
  `url` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '权限路径',
  `request_method` varchar(10) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '请求方式',
  `parent_id` int NULL DEFAULT NULL COMMENT '父权限id',
  `is_anonymous` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否匿名访问 0否 1是',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '修改时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 277 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_resource
-- ----------------------------
INSERT INTO `tb_resource` VALUES (165, '分类模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (166, '博客信息模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (167, '友链模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (168, '文章模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (169, '日志模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (170, '标签模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (171, '照片模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (172, '用户信息模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (173, '用户账号模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (174, '留言模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (175, '相册模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (176, '菜单模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (177, '角色模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (178, '评论模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (179, '资源模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (180, '页面模块', NULL, NULL, NULL, 0, '2021-08-11 21:04:21', NULL);
INSERT INTO `tb_resource` VALUES (181, '查看博客信息', '/', 'GET', 166, 1, '2021-08-11 21:04:22', '2021-08-11 21:05:29');
INSERT INTO `tb_resource` VALUES (182, '查看关于我信息', '/about', 'GET', 166, 1, '2021-08-11 21:04:22', '2021-08-11 21:05:29');
INSERT INTO `tb_resource` VALUES (183, '查看后台信息', '/admin', 'GET', 166, 0, '2021-08-11 21:04:22', '2021-09-20 23:51:27');
INSERT INTO `tb_resource` VALUES (184, '修改关于我信息', '/admin/about', 'PUT', 166, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (185, '查看后台文章', '/admin/articles', 'GET', 168, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (186, '添加或修改文章', '/admin/articles', 'POST', 168, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (187, '恢复或删除文章', '/admin/articles', 'PUT', 168, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (188, '物理删除文章', '/admin/articles', 'DELETE', 168, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (189, '上传文章图片', '/admin/articles/images', 'POST', 168, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (190, '修改文章置顶', '/admin/articles/top', 'PUT', 168, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (191, '根据id查看后台文章', '/admin/articles/*', 'GET', 168, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (192, '查看后台分类列表', '/admin/categories', 'GET', 165, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (193, '添加或修改分类', '/admin/categories', 'POST', 165, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (194, '删除分类', '/admin/categories', 'DELETE', 165, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (195, '搜索文章分类', '/admin/categories/search', 'GET', 165, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (196, '查询后台评论', '/admin/comments', 'GET', 178, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (197, '删除评论', '/admin/comments', 'DELETE', 178, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (198, '审核评论', '/admin/comments/review', 'PUT', 178, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (199, '查看后台友链列表', '/admin/links', 'GET', 167, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (200, '保存或修改友链', '/admin/links', 'POST', 167, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (201, '删除友链', '/admin/links', 'DELETE', 167, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (202, '查看菜单列表', '/admin/menus', 'GET', 176, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (203, '新增或修改菜单', '/admin/menus', 'POST', 176, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (204, '删除菜单', '/admin/menus/*', 'DELETE', 176, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (205, '查看后台留言列表', '/admin/messages', 'GET', 174, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (206, '删除留言', '/admin/messages', 'DELETE', 174, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (207, '审核留言', '/admin/messages/review', 'PUT', 174, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (208, '查看操作日志', '/admin/operation/logs', 'GET', 169, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (209, '删除操作日志', '/admin/operation/logs', 'DELETE', 169, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (210, '获取页面列表', '/admin/pages', 'GET', 180, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (211, '保存或更新页面', '/admin/pages', 'POST', 180, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (212, '删除页面', '/admin/pages/*', 'DELETE', 180, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (213, '根据相册id获取照片列表', '/admin/photos', 'GET', 171, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (214, '保存照片', '/admin/photos', 'POST', 171, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (215, '更新照片信息', '/admin/photos', 'PUT', 171, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (216, '删除照片', '/admin/photos', 'DELETE', 171, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (217, '移动照片相册', '/admin/photos/album', 'PUT', 171, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (218, '查看后台相册列表', '/admin/photos/albums', 'GET', 175, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (219, '保存或更新相册', '/admin/photos/albums', 'POST', 175, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (220, '上传相册封面', '/admin/photos/albums/cover', 'POST', 175, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (221, '获取后台相册列表信息', '/admin/photos/albums/info', 'GET', 175, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (222, '根据id删除相册', '/admin/photos/albums/*', 'DELETE', 175, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (223, '根据id获取后台相册信息', '/admin/photos/albums/*/info', 'GET', 175, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (224, '更新照片删除状态', '/admin/photos/delete', 'PUT', 171, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (225, '查看资源列表', '/admin/resources', 'GET', 179, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (226, '新增或修改资源', '/admin/resources', 'POST', 179, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (227, '导入swagger接口', '/admin/resources/import/swagger', 'GET', 179, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (228, '删除资源', '/admin/resources/*', 'DELETE', 179, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (229, '保存或更新角色', '/admin/role', 'POST', 177, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (230, '查看角色菜单选项', '/admin/role/menus', 'GET', 176, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (231, '查看角色资源选项', '/admin/role/resources', 'GET', 179, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (232, '查询角色列表', '/admin/roles', 'GET', 177, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (233, '删除角色', '/admin/roles', 'DELETE', 177, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (234, '查询后台标签列表', '/admin/tags', 'GET', 170, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (235, '添加或修改标签', '/admin/tags', 'POST', 170, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (236, '删除标签', '/admin/tags', 'DELETE', 170, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (237, '搜索文章标签', '/admin/tags/search', 'GET', 170, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (238, '查看当前用户菜单', '/admin/user/menus', 'GET', 176, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (239, '查询后台用户列表', '/admin/users', 'GET', 173, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (240, '修改用户禁用状态', '/admin/users/disable', 'PUT', 172, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (241, '查看在线用户', '/admin/users/online', 'GET', 172, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (242, '修改管理员密码', '/admin/users/password', 'PUT', 173, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (243, '查询用户角色选项', '/admin/users/role', 'GET', 177, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (244, '修改用户角色', '/admin/users/role', 'PUT', 172, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (245, '下线用户', '/admin/users/*/online', 'DELETE', 172, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (246, '获取网站配置', '/admin/website/config', 'GET', 166, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (247, '更新网站配置', '/admin/website/config', 'PUT', 166, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (248, '根据相册id查看照片列表', '/albums/*/photos', 'GET', 171, 1, '2021-08-11 21:04:22', '2021-08-11 21:06:35');
INSERT INTO `tb_resource` VALUES (249, '查看首页文章', '/articles', 'GET', 168, 1, '2021-08-11 21:04:22', '2021-08-11 21:05:45');
INSERT INTO `tb_resource` VALUES (250, '查看文章归档', '/articles/archives', 'GET', 168, 1, '2021-08-11 21:04:22', '2021-08-11 21:05:47');
INSERT INTO `tb_resource` VALUES (251, '根据条件查询文章', '/articles/condition', 'GET', 168, 1, '2021-08-11 21:04:22', '2021-08-11 21:05:47');
INSERT INTO `tb_resource` VALUES (252, '搜索文章', '/articles/search', 'GET', 168, 1, '2021-08-11 21:04:22', '2021-08-11 21:05:48');
INSERT INTO `tb_resource` VALUES (253, '根据id查看文章', '/articles/*', 'GET', 168, 1, '2021-08-11 21:04:22', '2021-08-11 21:05:49');
INSERT INTO `tb_resource` VALUES (254, '点赞文章', '/articles/*/like', 'POST', 168, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (255, '查看分类列表', '/categories', 'GET', 165, 1, '2021-08-11 21:04:22', '2021-08-11 21:05:26');
INSERT INTO `tb_resource` VALUES (256, '查询评论', '/comments', 'GET', 178, 1, '2021-08-11 21:04:22', '2021-08-11 21:07:33');
INSERT INTO `tb_resource` VALUES (257, '添加评论', '/comments', 'POST', 178, 0, '2021-08-11 21:04:22', '2021-08-11 21:10:05');
INSERT INTO `tb_resource` VALUES (258, '评论点赞', '/comments/*/like', 'POST', 178, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (259, '查询评论下的回复', '/comments/*/replies', 'GET', 178, 1, '2021-08-11 21:04:22', '2021-08-11 21:07:30');
INSERT INTO `tb_resource` VALUES (260, '查看友链列表', '/links', 'GET', 167, 1, '2021-08-11 21:04:22', '2021-08-11 21:05:41');
INSERT INTO `tb_resource` VALUES (261, '查看留言列表', '/messages', 'GET', 174, 1, '2021-08-11 21:04:22', '2021-08-11 21:07:14');
INSERT INTO `tb_resource` VALUES (262, '添加留言', '/messages', 'POST', 174, 1, '2021-08-11 21:04:22', '2021-08-11 21:07:15');
INSERT INTO `tb_resource` VALUES (263, '获取相册列表', '/photos/albums', 'GET', 175, 1, '2021-08-11 21:04:22', '2021-08-11 21:07:20');
INSERT INTO `tb_resource` VALUES (264, '用户注册', '/register', 'POST', 173, 1, '2021-08-11 21:04:22', '2021-08-11 21:07:01');
INSERT INTO `tb_resource` VALUES (265, '查询标签列表', '/tags', 'GET', 170, 1, '2021-08-11 21:04:22', '2021-08-11 21:06:30');
INSERT INTO `tb_resource` VALUES (267, '更新用户头像', '/users/avatar', 'POST', 172, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (268, '发送邮箱验证码', '/users/code', 'GET', 173, 1, '2021-08-11 21:04:22', '2021-08-11 21:07:02');
INSERT INTO `tb_resource` VALUES (269, '绑定用户邮箱', '/users/email', 'POST', 172, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (270, '更新用户信息', '/users/info', 'PUT', 172, 0, '2021-08-11 21:04:22', NULL);
INSERT INTO `tb_resource` VALUES (271, 'qq登录', '/users/oauth/qq', 'POST', 173, 1, '2021-08-11 21:04:22', '2021-08-11 21:07:06');
INSERT INTO `tb_resource` VALUES (272, '微博登录', '/users/oauth/weibo', 'POST', 173, 1, '2021-08-11 21:04:22', '2021-08-11 21:07:06');
INSERT INTO `tb_resource` VALUES (273, '修改密码', '/users/password', 'PUT', 173, 1, '2021-08-11 21:04:22', '2021-08-11 21:07:09');
INSERT INTO `tb_resource` VALUES (274, '上传语音', '/voice', 'POST', 166, 1, '2021-08-11 21:04:22', '2021-08-11 21:05:33');
INSERT INTO `tb_resource` VALUES (275, '上传访客信息', '/report', 'POST', 166, 1, '2021-08-24 11:24:08', '2021-08-24 11:24:10');
INSERT INTO `tb_resource` VALUES (276, '获取用户区域分布', '/admin/user/area', 'GET', 173, 0, '2021-08-24 11:24:33', NULL);

-- ----------------------------
-- Table structure for tb_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_role`;
CREATE TABLE `tb_role`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键id',
  `role_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色名',
  `role_label` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '角色描述',
  `is_disable` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否禁用  0否 1是',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 14 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_role
-- ----------------------------
INSERT INTO `tb_role` VALUES (1, '管理员', 'admin', 0, '2021-03-22 14:10:21', '2021-08-24 11:25:30');
INSERT INTO `tb_role` VALUES (2, '用户', 'user', 0, '2021-03-22 14:25:25', '2021-08-11 21:12:21');
INSERT INTO `tb_role` VALUES (3, '测试', 'test', 0, '2021-03-22 14:42:23', '2021-08-24 11:25:39');

-- ----------------------------
-- Table structure for tb_role_menu
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_menu`;
CREATE TABLE `tb_role_menu`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '主键',
  `role_id` int NULL DEFAULT NULL COMMENT '角色id',
  `menu_id` int NULL DEFAULT NULL COMMENT '菜单id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2310 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_role_menu
-- ----------------------------
INSERT INTO `tb_role_menu` VALUES (1397, 8, 1);
INSERT INTO `tb_role_menu` VALUES (1398, 8, 2);
INSERT INTO `tb_role_menu` VALUES (1399, 8, 6);
INSERT INTO `tb_role_menu` VALUES (1400, 8, 7);
INSERT INTO `tb_role_menu` VALUES (1401, 8, 8);
INSERT INTO `tb_role_menu` VALUES (1402, 8, 9);
INSERT INTO `tb_role_menu` VALUES (1403, 8, 10);
INSERT INTO `tb_role_menu` VALUES (1404, 8, 3);
INSERT INTO `tb_role_menu` VALUES (1405, 8, 11);
INSERT INTO `tb_role_menu` VALUES (1406, 8, 12);
INSERT INTO `tb_role_menu` VALUES (1407, 8, 202);
INSERT INTO `tb_role_menu` VALUES (1408, 8, 13);
INSERT INTO `tb_role_menu` VALUES (1409, 8, 14);
INSERT INTO `tb_role_menu` VALUES (1410, 8, 201);
INSERT INTO `tb_role_menu` VALUES (1411, 8, 4);
INSERT INTO `tb_role_menu` VALUES (1412, 8, 16);
INSERT INTO `tb_role_menu` VALUES (1413, 8, 15);
INSERT INTO `tb_role_menu` VALUES (1414, 8, 17);
INSERT INTO `tb_role_menu` VALUES (1415, 8, 18);
INSERT INTO `tb_role_menu` VALUES (1416, 8, 19);
INSERT INTO `tb_role_menu` VALUES (1417, 8, 20);
INSERT INTO `tb_role_menu` VALUES (1418, 8, 5);
INSERT INTO `tb_role_menu` VALUES (1595, 9, 1);
INSERT INTO `tb_role_menu` VALUES (1596, 9, 2);
INSERT INTO `tb_role_menu` VALUES (1597, 9, 6);
INSERT INTO `tb_role_menu` VALUES (1598, 9, 7);
INSERT INTO `tb_role_menu` VALUES (1599, 9, 8);
INSERT INTO `tb_role_menu` VALUES (1600, 9, 9);
INSERT INTO `tb_role_menu` VALUES (1601, 9, 10);
INSERT INTO `tb_role_menu` VALUES (1602, 9, 3);
INSERT INTO `tb_role_menu` VALUES (1603, 9, 11);
INSERT INTO `tb_role_menu` VALUES (1604, 9, 12);
INSERT INTO `tb_role_menu` VALUES (1605, 9, 202);
INSERT INTO `tb_role_menu` VALUES (1606, 9, 13);
INSERT INTO `tb_role_menu` VALUES (1607, 9, 14);
INSERT INTO `tb_role_menu` VALUES (1608, 9, 201);
INSERT INTO `tb_role_menu` VALUES (1609, 9, 4);
INSERT INTO `tb_role_menu` VALUES (1610, 9, 16);
INSERT INTO `tb_role_menu` VALUES (1611, 9, 15);
INSERT INTO `tb_role_menu` VALUES (1612, 9, 17);
INSERT INTO `tb_role_menu` VALUES (1613, 9, 18);
INSERT INTO `tb_role_menu` VALUES (1614, 9, 19);
INSERT INTO `tb_role_menu` VALUES (1615, 9, 20);
INSERT INTO `tb_role_menu` VALUES (1616, 9, 5);
INSERT INTO `tb_role_menu` VALUES (1639, 13, 2);
INSERT INTO `tb_role_menu` VALUES (1640, 13, 6);
INSERT INTO `tb_role_menu` VALUES (1641, 13, 7);
INSERT INTO `tb_role_menu` VALUES (1642, 13, 8);
INSERT INTO `tb_role_menu` VALUES (1643, 13, 9);
INSERT INTO `tb_role_menu` VALUES (1644, 13, 10);
INSERT INTO `tb_role_menu` VALUES (1645, 13, 3);
INSERT INTO `tb_role_menu` VALUES (1646, 13, 11);
INSERT INTO `tb_role_menu` VALUES (1647, 13, 12);
INSERT INTO `tb_role_menu` VALUES (2252, 1, 1);
INSERT INTO `tb_role_menu` VALUES (2253, 1, 2);
INSERT INTO `tb_role_menu` VALUES (2254, 1, 6);
INSERT INTO `tb_role_menu` VALUES (2255, 1, 7);
INSERT INTO `tb_role_menu` VALUES (2256, 1, 8);
INSERT INTO `tb_role_menu` VALUES (2257, 1, 9);
INSERT INTO `tb_role_menu` VALUES (2258, 1, 10);
INSERT INTO `tb_role_menu` VALUES (2259, 1, 3);
INSERT INTO `tb_role_menu` VALUES (2260, 1, 11);
INSERT INTO `tb_role_menu` VALUES (2261, 1, 12);
INSERT INTO `tb_role_menu` VALUES (2262, 1, 202);
INSERT INTO `tb_role_menu` VALUES (2263, 1, 13);
INSERT INTO `tb_role_menu` VALUES (2264, 1, 201);
INSERT INTO `tb_role_menu` VALUES (2265, 1, 213);
INSERT INTO `tb_role_menu` VALUES (2266, 1, 14);
INSERT INTO `tb_role_menu` VALUES (2267, 1, 15);
INSERT INTO `tb_role_menu` VALUES (2268, 1, 16);
INSERT INTO `tb_role_menu` VALUES (2269, 1, 4);
INSERT INTO `tb_role_menu` VALUES (2270, 1, 214);
INSERT INTO `tb_role_menu` VALUES (2271, 1, 209);
INSERT INTO `tb_role_menu` VALUES (2272, 1, 17);
INSERT INTO `tb_role_menu` VALUES (2273, 1, 18);
INSERT INTO `tb_role_menu` VALUES (2274, 1, 205);
INSERT INTO `tb_role_menu` VALUES (2275, 1, 206);
INSERT INTO `tb_role_menu` VALUES (2276, 1, 208);
INSERT INTO `tb_role_menu` VALUES (2277, 1, 210);
INSERT INTO `tb_role_menu` VALUES (2278, 1, 19);
INSERT INTO `tb_role_menu` VALUES (2279, 1, 20);
INSERT INTO `tb_role_menu` VALUES (2280, 1, 5);
INSERT INTO `tb_role_menu` VALUES (2281, 3, 1);
INSERT INTO `tb_role_menu` VALUES (2282, 3, 2);
INSERT INTO `tb_role_menu` VALUES (2283, 3, 6);
INSERT INTO `tb_role_menu` VALUES (2284, 3, 7);
INSERT INTO `tb_role_menu` VALUES (2285, 3, 8);
INSERT INTO `tb_role_menu` VALUES (2286, 3, 9);
INSERT INTO `tb_role_menu` VALUES (2287, 3, 10);
INSERT INTO `tb_role_menu` VALUES (2288, 3, 3);
INSERT INTO `tb_role_menu` VALUES (2289, 3, 11);
INSERT INTO `tb_role_menu` VALUES (2290, 3, 12);
INSERT INTO `tb_role_menu` VALUES (2291, 3, 202);
INSERT INTO `tb_role_menu` VALUES (2292, 3, 13);
INSERT INTO `tb_role_menu` VALUES (2293, 3, 201);
INSERT INTO `tb_role_menu` VALUES (2294, 3, 213);
INSERT INTO `tb_role_menu` VALUES (2295, 3, 14);
INSERT INTO `tb_role_menu` VALUES (2296, 3, 15);
INSERT INTO `tb_role_menu` VALUES (2297, 3, 16);
INSERT INTO `tb_role_menu` VALUES (2298, 3, 4);
INSERT INTO `tb_role_menu` VALUES (2299, 3, 214);
INSERT INTO `tb_role_menu` VALUES (2300, 3, 209);
INSERT INTO `tb_role_menu` VALUES (2301, 3, 17);
INSERT INTO `tb_role_menu` VALUES (2302, 3, 18);
INSERT INTO `tb_role_menu` VALUES (2303, 3, 205);
INSERT INTO `tb_role_menu` VALUES (2304, 3, 206);
INSERT INTO `tb_role_menu` VALUES (2305, 3, 208);
INSERT INTO `tb_role_menu` VALUES (2306, 3, 210);
INSERT INTO `tb_role_menu` VALUES (2307, 3, 19);
INSERT INTO `tb_role_menu` VALUES (2308, 3, 20);
INSERT INTO `tb_role_menu` VALUES (2309, 3, 5);

-- ----------------------------
-- Table structure for tb_role_resource
-- ----------------------------
DROP TABLE IF EXISTS `tb_role_resource`;
CREATE TABLE `tb_role_resource`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `role_id` int NULL DEFAULT NULL COMMENT '角色id',
  `resource_id` int NULL DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 4193 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_role_resource
-- ----------------------------
INSERT INTO `tb_role_resource` VALUES (4011, 2, 254);
INSERT INTO `tb_role_resource` VALUES (4012, 2, 267);
INSERT INTO `tb_role_resource` VALUES (4013, 2, 269);
INSERT INTO `tb_role_resource` VALUES (4014, 2, 270);
INSERT INTO `tb_role_resource` VALUES (4015, 2, 257);
INSERT INTO `tb_role_resource` VALUES (4016, 2, 258);
INSERT INTO `tb_role_resource` VALUES (4076, 1, 165);
INSERT INTO `tb_role_resource` VALUES (4077, 1, 192);
INSERT INTO `tb_role_resource` VALUES (4078, 1, 193);
INSERT INTO `tb_role_resource` VALUES (4079, 1, 194);
INSERT INTO `tb_role_resource` VALUES (4080, 1, 195);
INSERT INTO `tb_role_resource` VALUES (4081, 1, 166);
INSERT INTO `tb_role_resource` VALUES (4082, 1, 183);
INSERT INTO `tb_role_resource` VALUES (4083, 1, 184);
INSERT INTO `tb_role_resource` VALUES (4084, 1, 246);
INSERT INTO `tb_role_resource` VALUES (4085, 1, 247);
INSERT INTO `tb_role_resource` VALUES (4086, 1, 167);
INSERT INTO `tb_role_resource` VALUES (4087, 1, 199);
INSERT INTO `tb_role_resource` VALUES (4088, 1, 200);
INSERT INTO `tb_role_resource` VALUES (4089, 1, 201);
INSERT INTO `tb_role_resource` VALUES (4090, 1, 168);
INSERT INTO `tb_role_resource` VALUES (4091, 1, 185);
INSERT INTO `tb_role_resource` VALUES (4092, 1, 186);
INSERT INTO `tb_role_resource` VALUES (4093, 1, 187);
INSERT INTO `tb_role_resource` VALUES (4094, 1, 188);
INSERT INTO `tb_role_resource` VALUES (4095, 1, 189);
INSERT INTO `tb_role_resource` VALUES (4096, 1, 190);
INSERT INTO `tb_role_resource` VALUES (4097, 1, 191);
INSERT INTO `tb_role_resource` VALUES (4098, 1, 254);
INSERT INTO `tb_role_resource` VALUES (4099, 1, 169);
INSERT INTO `tb_role_resource` VALUES (4100, 1, 208);
INSERT INTO `tb_role_resource` VALUES (4101, 1, 209);
INSERT INTO `tb_role_resource` VALUES (4102, 1, 170);
INSERT INTO `tb_role_resource` VALUES (4103, 1, 234);
INSERT INTO `tb_role_resource` VALUES (4104, 1, 235);
INSERT INTO `tb_role_resource` VALUES (4105, 1, 236);
INSERT INTO `tb_role_resource` VALUES (4106, 1, 237);
INSERT INTO `tb_role_resource` VALUES (4107, 1, 171);
INSERT INTO `tb_role_resource` VALUES (4108, 1, 213);
INSERT INTO `tb_role_resource` VALUES (4109, 1, 214);
INSERT INTO `tb_role_resource` VALUES (4110, 1, 215);
INSERT INTO `tb_role_resource` VALUES (4111, 1, 216);
INSERT INTO `tb_role_resource` VALUES (4112, 1, 217);
INSERT INTO `tb_role_resource` VALUES (4113, 1, 224);
INSERT INTO `tb_role_resource` VALUES (4114, 1, 172);
INSERT INTO `tb_role_resource` VALUES (4115, 1, 240);
INSERT INTO `tb_role_resource` VALUES (4116, 1, 241);
INSERT INTO `tb_role_resource` VALUES (4117, 1, 244);
INSERT INTO `tb_role_resource` VALUES (4118, 1, 245);
INSERT INTO `tb_role_resource` VALUES (4119, 1, 267);
INSERT INTO `tb_role_resource` VALUES (4120, 1, 269);
INSERT INTO `tb_role_resource` VALUES (4121, 1, 270);
INSERT INTO `tb_role_resource` VALUES (4122, 1, 173);
INSERT INTO `tb_role_resource` VALUES (4123, 1, 239);
INSERT INTO `tb_role_resource` VALUES (4124, 1, 242);
INSERT INTO `tb_role_resource` VALUES (4125, 1, 276);
INSERT INTO `tb_role_resource` VALUES (4126, 1, 174);
INSERT INTO `tb_role_resource` VALUES (4127, 1, 205);
INSERT INTO `tb_role_resource` VALUES (4128, 1, 206);
INSERT INTO `tb_role_resource` VALUES (4129, 1, 207);
INSERT INTO `tb_role_resource` VALUES (4130, 1, 175);
INSERT INTO `tb_role_resource` VALUES (4131, 1, 218);
INSERT INTO `tb_role_resource` VALUES (4132, 1, 219);
INSERT INTO `tb_role_resource` VALUES (4133, 1, 220);
INSERT INTO `tb_role_resource` VALUES (4134, 1, 221);
INSERT INTO `tb_role_resource` VALUES (4135, 1, 222);
INSERT INTO `tb_role_resource` VALUES (4136, 1, 223);
INSERT INTO `tb_role_resource` VALUES (4137, 1, 176);
INSERT INTO `tb_role_resource` VALUES (4138, 1, 202);
INSERT INTO `tb_role_resource` VALUES (4139, 1, 203);
INSERT INTO `tb_role_resource` VALUES (4140, 1, 204);
INSERT INTO `tb_role_resource` VALUES (4141, 1, 230);
INSERT INTO `tb_role_resource` VALUES (4142, 1, 238);
INSERT INTO `tb_role_resource` VALUES (4143, 1, 177);
INSERT INTO `tb_role_resource` VALUES (4144, 1, 229);
INSERT INTO `tb_role_resource` VALUES (4145, 1, 232);
INSERT INTO `tb_role_resource` VALUES (4146, 1, 233);
INSERT INTO `tb_role_resource` VALUES (4147, 1, 243);
INSERT INTO `tb_role_resource` VALUES (4148, 1, 178);
INSERT INTO `tb_role_resource` VALUES (4149, 1, 196);
INSERT INTO `tb_role_resource` VALUES (4150, 1, 197);
INSERT INTO `tb_role_resource` VALUES (4151, 1, 198);
INSERT INTO `tb_role_resource` VALUES (4152, 1, 257);
INSERT INTO `tb_role_resource` VALUES (4153, 1, 258);
INSERT INTO `tb_role_resource` VALUES (4154, 1, 179);
INSERT INTO `tb_role_resource` VALUES (4155, 1, 225);
INSERT INTO `tb_role_resource` VALUES (4156, 1, 226);
INSERT INTO `tb_role_resource` VALUES (4157, 1, 227);
INSERT INTO `tb_role_resource` VALUES (4158, 1, 228);
INSERT INTO `tb_role_resource` VALUES (4159, 1, 231);
INSERT INTO `tb_role_resource` VALUES (4160, 1, 180);
INSERT INTO `tb_role_resource` VALUES (4161, 1, 210);
INSERT INTO `tb_role_resource` VALUES (4162, 1, 211);
INSERT INTO `tb_role_resource` VALUES (4163, 1, 212);
INSERT INTO `tb_role_resource` VALUES (4164, 3, 192);
INSERT INTO `tb_role_resource` VALUES (4165, 3, 195);
INSERT INTO `tb_role_resource` VALUES (4166, 3, 183);
INSERT INTO `tb_role_resource` VALUES (4167, 3, 246);
INSERT INTO `tb_role_resource` VALUES (4168, 3, 199);
INSERT INTO `tb_role_resource` VALUES (4169, 3, 185);
INSERT INTO `tb_role_resource` VALUES (4170, 3, 191);
INSERT INTO `tb_role_resource` VALUES (4171, 3, 254);
INSERT INTO `tb_role_resource` VALUES (4172, 3, 208);
INSERT INTO `tb_role_resource` VALUES (4173, 3, 234);
INSERT INTO `tb_role_resource` VALUES (4174, 3, 237);
INSERT INTO `tb_role_resource` VALUES (4175, 3, 213);
INSERT INTO `tb_role_resource` VALUES (4176, 3, 241);
INSERT INTO `tb_role_resource` VALUES (4177, 3, 239);
INSERT INTO `tb_role_resource` VALUES (4178, 3, 276);
INSERT INTO `tb_role_resource` VALUES (4179, 3, 205);
INSERT INTO `tb_role_resource` VALUES (4180, 3, 218);
INSERT INTO `tb_role_resource` VALUES (4181, 3, 223);
INSERT INTO `tb_role_resource` VALUES (4182, 3, 202);
INSERT INTO `tb_role_resource` VALUES (4183, 3, 230);
INSERT INTO `tb_role_resource` VALUES (4184, 3, 238);
INSERT INTO `tb_role_resource` VALUES (4185, 3, 232);
INSERT INTO `tb_role_resource` VALUES (4186, 3, 243);
INSERT INTO `tb_role_resource` VALUES (4187, 3, 196);
INSERT INTO `tb_role_resource` VALUES (4188, 3, 257);
INSERT INTO `tb_role_resource` VALUES (4189, 3, 258);
INSERT INTO `tb_role_resource` VALUES (4190, 3, 225);
INSERT INTO `tb_role_resource` VALUES (4191, 3, 231);
INSERT INTO `tb_role_resource` VALUES (4192, 3, 210);

-- ----------------------------
-- Table structure for tb_tag
-- ----------------------------
DROP TABLE IF EXISTS `tb_tag`;
CREATE TABLE `tb_tag`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `tag_name` varchar(20) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '标签名',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 30 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_tag
-- ----------------------------
INSERT INTO `tb_tag` VALUES (27, '测试标签', '2021-08-12 15:50:57', NULL);
INSERT INTO `tb_tag` VALUES (28, 'bug记录', '2021-09-20 23:47:55', NULL);
INSERT INTO `tb_tag` VALUES (29, '随便写写', '2021-09-21 00:10:14', NULL);

-- ----------------------------
-- Table structure for tb_unique_view
-- ----------------------------
DROP TABLE IF EXISTS `tb_unique_view`;
CREATE TABLE `tb_unique_view`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `create_time` datetime NOT NULL COMMENT '时间',
  `views_count` int NOT NULL COMMENT '访问量',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 374 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_unique_view
-- ----------------------------
INSERT INTO `tb_unique_view` VALUES (369, '2021-09-20 00:00:00', 3);
INSERT INTO `tb_unique_view` VALUES (370, '2021-09-21 00:00:00', 3);
INSERT INTO `tb_unique_view` VALUES (371, '2021-09-22 00:00:00', 1);
INSERT INTO `tb_unique_view` VALUES (372, '2021-09-23 00:00:00', 2);
INSERT INTO `tb_unique_view` VALUES (373, '2021-09-24 00:00:00', 3);

-- ----------------------------
-- Table structure for tb_user_auth
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_auth`;
CREATE TABLE `tb_user_auth`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_info_id` int NOT NULL COMMENT '用户信息id',
  `username` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户名',
  `password` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '密码',
  `login_type` tinyint(1) NOT NULL COMMENT '登录类型',
  `ip_address` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户登录ip',
  `ip_source` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT 'ip来源',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  `last_login_time` datetime NULL DEFAULT NULL COMMENT '上次登录时间',
  PRIMARY KEY (`id`) USING BTREE,
  UNIQUE INDEX `username`(`username`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 553 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_user_auth
-- ----------------------------
INSERT INTO `tb_user_auth` VALUES (1, 1, '2416206063@qq.com', '$2a$10$aHID3l7y64dEawobVNx72uQWVYPtodHXWYo26eISyJa5RjdTL2M4C', 1, '125.46.164.45', '河南省郑州市 联通', '2021-08-12 15:43:18', '2021-09-25 00:02:52', '2021-09-25 00:02:52');

-- ----------------------------
-- Table structure for tb_user_info
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_info`;
CREATE TABLE `tb_user_info`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '用户ID',
  `email` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '邮箱号',
  `nickname` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL COMMENT '用户昵称',
  `avatar` varchar(1024) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NOT NULL DEFAULT '' COMMENT '用户头像',
  `intro` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '用户简介',
  `web_site` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '个人网站',
  `is_disable` tinyint(1) NOT NULL DEFAULT 0 COMMENT '是否禁用',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 563 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_user_info
-- ----------------------------
INSERT INTO `tb_user_info` VALUES (1, 'admin@qq.com', '管理员', 'http://8.131.54.14:83/avatar/4610eddb63a74e4610004295d321d7e8.jpeg', '2416206063@qq.com', 'http://8.131.54.14:80', 0, '2021-08-12 15:43:17', '2021-09-20 23:44:47');

-- ----------------------------
-- Table structure for tb_user_role
-- ----------------------------
DROP TABLE IF EXISTS `tb_user_role`;
CREATE TABLE `tb_user_role`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `user_id` int NULL DEFAULT NULL COMMENT '用户id',
  `role_id` int NULL DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 578 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_user_role
-- ----------------------------
INSERT INTO `tb_user_role` VALUES (577, 1, 1);

-- ----------------------------
-- Table structure for tb_website_config
-- ----------------------------
DROP TABLE IF EXISTS `tb_website_config`;
CREATE TABLE `tb_website_config`  (
  `id` int NOT NULL AUTO_INCREMENT,
  `config` varchar(1000) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci NULL DEFAULT NULL COMMENT '配置信息',
  `create_time` datetime NOT NULL COMMENT '创建时间',
  `update_time` datetime NULL DEFAULT NULL COMMENT '更新时间',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 2 CHARACTER SET = utf8mb4 COLLATE = utf8mb4_0900_ai_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of tb_website_config
-- ----------------------------
INSERT INTO `tb_website_config` VALUES (1, '{\"alipayQRCode\":\"https://www.static.talkxj.com/photos/13d83d77cc1f7e4e0437d7feaf56879f.png\",\"gitee\":\"https://gitee.com/yan-shi-kun/\",\"github\":\"https://github.com/X1192176811\",\"isChatRoom\":0,\"isCommentReview\":0,\"isEmailNotice\":1,\"isMessageReview\":0,\"isMusicPlayer\":0,\"isReward\":0,\"qq\":\"2416206063\",\"socialLoginList\":[],\"socialUrlList\":[\"qq\",\"gitee\",\"github\"],\"touristAvatar\":\"https://www.static.talkxj.com/photos/0bca52afdb2b9998132355d716390c9f.png\",\"websiteAuthor\":\"-半城柳色半声笛\",\"websiteAvatar\":\"http://8.131.54.14:83/config/4610eddb63a74e4610004295d321d7e8.jpeg\",\"websiteCreateTime\":\"2021-09-20\",\"websiteIntro\":\"热爱生活\",\"websiteName\":\"-半城柳色半声笛\",\"websiteNotice\":\"欢迎来到本站\",\"websiteRecordNo\":\"备案号\",\"websocketUrl\":\"ws://127.0.0.1:8080/websocket\",\"weiXinQRCode\":\"https://www.static.talkxj.com/photos/4f767ef84e55ab9ad42b2d20e51deca1.png\"}', '2021-08-09 19:37:30', '2021-09-21 00:15:46');

SET FOREIGN_KEY_CHECKS = 1;
